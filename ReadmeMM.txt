Para botar o projeto pra rodar:

Instalações
- NPM (https://nodejs.org/en/ instalar a LTS 16)
- Yarn (via NPM: npm install --global yarn)
- Python 3 (https://www.python.org/downloads/)

Executar na pasta do projeto
- yarn install
- npm install -g expo-cli

Para escolhar o ambiente para rodar local
- python3 publicar.py hom local
- python3 publicar.py prod local

Executar app
- expo start
- abrir URL no browser http://localhost:19002

Para publicar nova versão (PRD ou HOM)
- python3 publicar.py hom
- python3 publicar.py prod

Contas vinculadas
- BitBucket
- Expo.io