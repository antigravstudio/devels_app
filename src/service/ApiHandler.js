import axios from 'axios';
import auth from './auth';
import Formater from '../utils/Formater';

const slug = require('../../app.json').expo.slug;

var baseURL = 'https://fenixapi.azurewebsites.net/App';
if (slug == 'devels_app_homologa') baseURL = 'https://fenixapi-hom.azurewebsites.net/App';
var api = axios.create({ baseURL, timeout: 240000 });

export default {
    primeiroLogin: async(data) => {
        const token = await auth();
        const query = Formater.serialize(data);
        console.log(baseURL);
        try {
            const response = await api.get(`/PrimeiroLogin?${query}`, {
                headers: {
                    Authorization: `Bearer ${token}`
                }
            });
            console.log(response.status);
            if (response.status == 403) return 'nao_autorizado';
            return response.data;
        } catch (e) {
            //console.log(`https://fenixapi.azurewebsites.net/App/PrimeiroLogin?${query}`);
            console.log('Error --> ' + e.response.status)
            if (e.response.status == 403) return 'nao_autorizado';
            return false;
        }
    },
    validaLogin: async(data) => {
        const token = await auth();
        const query = Formater.serialize(data);
        try {
            const response = await api.get(`/Login?${query}`, {
                headers: {
                    Authorization: `Bearer ${token}`
                }
            });
            console.log(response.status);
            if (response.status == 403) return 'nao_autorizado';
            return response.status == 200 ? true : false;
        } catch (e) {
            console.log(e.response.status);
            if (e.response.status == 403) return 'nao_autorizado';
            return false;
        }
    },
    leituraQRCode: async(data) => {
        const token = await auth();
        const query = Formater.serialize(data);
        try {
            const response = await api.post(`/LeituraQRCode?${query}`, {}, {
                headers: {
                    Authorization: `Bearer ${token}`
                }
            });
            return {
                status: response.status,
                data: response.data
            };
        } catch (e) {
            var mensagem_erro = '';
            switch (Number(e.response.status)) {
                case 500:
                    mensagem_erro = 'Erro do Servidor Interno';
                    break;
                case 403:
                    mensagem_erro = 'Acesso Negado';
                    break;
                case 301:
                    mensagem_erro = 'Movido Permanentemente';
                    break;
            }
            return {
                status: e.response.status,
                data: e.response.data ? e.response.data : mensagem_erro
            };
        }
    },
    consulta: async(data) => {
        const token = await auth();
        const query = Formater.serialize(data);
        try {
            const response = await api.get(`/Consulta?${query}`, {
                headers: {
                    Authorization: `Bearer ${token}`
                }
            });
            return response.data;
        } catch (e) {
            return false;
        }
    },
    tiposBilhetes: async(data) => {
        const token = await auth();
        const query = Formater.serialize(data);
        try {
            const response = await api.get(`/ObtemTiposBilhetes?${query}`, {
                headers: {
                    Authorization: `Bearer ${token}`
                }
            });
            return response.data;
        } catch (e) {
            return false;
        }
    },
    lotesDisponiveis: async(data) => {
        const token = await auth();
        const query = Formater.serialize(data);
        try {
            const response = await api.get(`/ObtemLotesDisponiveis?${query}`, {
                headers: {
                    Authorization: `Bearer ${token}`
                }
            });
            return response.data;
        } catch (e) {
            return false;
        }
    },
    inserirSubLote: async(data) => {
        const token = await auth();
        const query = Formater.serialize(data);
        try {
            const response = await api.post(`/InserirSubLote?${query}`, {}, {
                headers: {
                    Authorization: `Bearer ${token}`
                }
            });
            return response.status == 200 ? true : false;
        } catch (e) {
            return false;
        }
    },
    inserirSubLoteLista: async(data) => {
        const token = await auth();
        const query = Formater.serialize(data);
        try {
            const response = await api.post(`/InserirSubLotePorLista?${query}`, {}, {
                headers: {
                    Authorization: `Bearer ${token}`
                }
            });
            return response.status == 200 ? true : false;
        } catch (e) {
            return false;
        }
    },
    oterMotivos: async(data) => {
        const token = await auth();
        const query = Formater.serialize(data);
        try {
            const response = await api.get(`/ObtemMotivos?${query}`, {
                headers: {
                    Authorization: `Bearer ${token}`
                }
            });
            return response.data;
        } catch (e) {
            return false;
        }
    },
    bilheteFaltante: async(data) => {
        const token = await auth();
        const query = Formater.serialize(data);
        try {
            const response = await api.post(`/SetBilheteFaltante?${query}`, {}, {
                headers: {
                    Authorization: `Bearer ${token}`
                }
            });
            return response.status == 200 ? true : false;
        } catch (e) {
            return false;
        }
    },
    bilheteSemLeitura: async(data) => {
        const token = await auth();
        const query = Formater.serialize(data);
        try {
            const response = await api.post(`/SetBilheteSemLeitura?${query}`, {}, {
                headers: {
                    Authorization: `Bearer ${token}`
                }
            });
            return response.status == 200 ? true : false;
        } catch (e) {
            return false;
        }
    },
    bilheteOcorrenciaErrada: async(data) => {
        const token = await auth();
        const query = Formater.serialize(data);
        try {
            const response = await api.post(`/SetBilheteOcorrenciaErrada?${query}`, {}, {
                headers: {
                    Authorization: `Bearer ${token}`
                }
            });
            return response.status == 200 ? true : false;
        } catch (e) {
            return false;
        }
    },
    liberarGTVparaAPF: async(data) => {
        const token = await auth();
        const query = Formater.serialize(data);
        try {
            const response = await api.post(`/LiberaGTVParaAPF?${query}`, {}, {
                headers: {
                    Authorization: `Bearer ${token}`
                }
            });
            return response.status == 200 ? true : false;
        } catch (e) {
            return false;
        }
    },
    adicionaBilheteCancelamento: async(data) => {
        const token = await auth();
        const query = Formater.serialize(data);
        try {
            const response = await api.post(`/AdicionaBilheteNoCancelamento?${query}`, {}, {
                headers: {
                    Authorization: `Bearer ${token}`
                }
            });
            return response.status == 200 ? true : false;
        } catch (e) {
            return false;
        }
    },
    vincularOperador: async(data) => {
        const token = await auth();
        const query = Formater.serialize(data);
        try {
            const response = await api.post(`/SetOperadorSubLote?${query}`, {}, {
                headers: {
                    Authorization: `Bearer ${token}`
                }
            });
            console.log(response.status, response.data);
            return response.status == 200 ? true : false;
        } catch (e) {
            console.log(e.response.status);
            return false;
        }
    },
    vincularCracha: async(data) => {
        const token = await auth();
        const query = Formater.serialize(data);
        try {
            const response = await api.post(`/SetCrachaOperador?${query}`, {}, {
                headers: {
                    Authorization: `Bearer ${token}`
                }
            });
            console.log(response.status, response.data);
            return response.status == 200 ? true : false;
        } catch (e) {
            console.log(e);
            return false;
        }
    },
    obtemOperadores: async(data) => {
        const token = await auth();
        const query = Formater.serialize(data);
        try {
            const response = await api.get(`/ObtemOperadores?${query}`, {
                headers: {
                    Authorization: `Bearer ${token}`
                }
            });
            return response.data;
        } catch (e) {
            return false;
        }
    },
    listarDocumentos: async(query, retorno) => {
        const token = await auth();
        var config = {
            method: 'post',
            url: `${baseURL}/ListaDocumentos?${query}`,
            headers: { 
            'Authorization': `Bearer ${token}`
            }
        };
        
        axios(config)
            .then(function (response) {
                retorno(response.data);
            })
            .catch(function (error) {
                retorno(false);
            });
    },
    downloadDocumento: async(query, retorno) => {
        const token = await auth();
        const url = `${baseURL}/DownloadDocumento?${query}`;
        
        retorno({
            url, token, query
        });
    },
};