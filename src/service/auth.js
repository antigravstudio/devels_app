import axios from 'axios';
var qs = require('qs');

const slug = require('../../app.json').expo.slug;

const auth = async () => {
    var url = 'https://id.lyli.com.br/auth/realms/Lyli/protocol/openid-connect/token';
    try {
        var params = qs.stringify({
            'grant_type': 'client_credentials',
            'scope': 'audience',
            'client_id': 'Athens',
            'client_secret': 'QRNDottBNZkpUdT8l9WuG92sN5TKTBxm' 
          });

        const response = await axios({
            method: 'post',
            maxBodyLength: Infinity,
            url,
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            },
            data: params
        });
        return response.data.access_token ? response.data.access_token : false;
    } catch(e){
        console.log(e);
        return false;
    }
};

export default auth;