import React, { useEffect, createRef } from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import * as Linking from 'expo-linking';

import Store from './utils/Store';

import { SplashScreen } from './components/splash.component';
import { LoginScreen } from './components/login.component';
import { PerfilScreen } from './components/perfil.component';
import { MenuScreen } from './components/menu.component';
import { QRCodeScreen } from './components/qrcode.component';
import { HistoricoLeiturasScreen } from './components/historico_leituras.component';
import { EstoqueBilhetesScreen } from './components/estoque_bilhetes.component';
import { EstatisticasAbastacimentoScreen } from './components/estatisticas_abastacimento.component';
import { EstatisticasFragmentacaoScreen } from './components/estatisticas_fragmentacao.component';
import { EstatisticasPostoScreen } from './components/estatisticas_posto.component';
import { TabelaScreen } from './components/tabela.component';
import { MovimentacaoDiariaScreen } from './components/movimentacao_diaria.component';
import { ConsultarGTVScreen } from './components/consultar_gtv.component';
import { ConsultarAPFScreen } from './components/consultar_apf.component';
import { ConsultarLoteScreen } from './components/consultar_lote.component';
import { ConsultarBilheteScreen } from './components/consultar_bilhete.component';
import { FracionarLoteScreen } from './components/fracionar_lote.component';
import { BarCodeScreen } from './components/barcode.component';
import { NFCScreen } from './components/nfc.component';
import { LotesScreen } from './components/lotes.component';
import { VincularOperadorScreen } from './components/vincular_operador.component';
import { ValidarCancelamentoScreen } from './components/validar_cancelamento.component';
import { VincularCrachaScreen } from './components/vincular_cracha.component';
import { DocumentosScreen } from './components/documentos.component';
import * as Updates from 'expo-updates';

const Stack = createStackNavigator();

export const AppNavigator = () => {
	const navigationRef = createRef();

	useEffect(() => {
		Linking.addEventListener('url', async ({ url }) => {
			var _lote = await Store.get('numeroLote');
			if (url.includes('cartoes') && _lote) {
				setTimeout(async () => {
					var cartoes = url.split('/cartoes,')[1].split(',');
					var lote = cartoes[0];
					var index = cartoes.indexOf(lote);
					if (index > -1) cartoes.splice(index, 1);
					try {
						navigationRef.current.reset({ index: 0, routes: [{ name: 'NFC', params: {
							cartoes, lote: lote.replace(/[^0-9]/g, '')
						}}]});
					} catch(e){
						await Updates.reloadAsync();
					}
				}, 2000);
			} else if (url.includes('operador')){
				try {
					var operador = url.split('/operador,')[1].split('?')[0];
					var boleta = url.split('/operador,')[1].split('?')[1];
					await Store.set('operador-leitura', String(operador));
					try {
						navigationRef.current.reset({ index: 0, routes: [{ name: 'VincularOperador', params: {
							operador, boleta
						}}]});
					} catch(e){
						await Updates.reloadAsync();
					}
				} catch(e){}
			} else if (url.includes('validacao-cancelamento')){
				try {
					var cartao = url.split('/validacao-cancelamento,')[1].split('?')[0];
					var gtv = url.split('/validacao-cancelamento,')[1].split('?')[1];
					try {
						navigationRef.current.reset({ index: 0, routes: [{ name: 'ValidarCancelamento', params: {
							cartao, gtv
						}}]});
					} catch(e){
						await Updates.reloadAsync();
					}
				} catch(e){}
			} else if (url.includes('cracha')){
				try {
					var cartao = url.split('/cracha,')[1].split('?')[0];
					var operador = url.split('/cracha,')[1].split('?')[1];
					await Store.set('operador-leitura', String(operador));
					try {
						navigationRef.current.reset({ index: 0, routes: [{ name: 'VincularCracha', params: {
							cartao, operador
						}}]});
					} catch(e){
						await Updates.reloadAsync();
					}
				} catch(e){}
			}
		});
	}, []);

	return (
		<>
			<NavigationContainer ref={navigationRef}>
				<Stack.Navigator>
					<Stack.Screen
						name='Splash'
						component={SplashScreen}
						options={{
							animationEnabled: false,
							headerShown: false
						}}
					/>
					<Stack.Screen
						name='Login'
						component={LoginScreen}
						options={{
							animationEnabled: false,
							headerShown: false
						}}
					/>
					<Stack.Screen
						name='Perfil'
						component={PerfilScreen}
						options={{
							animationEnabled: false,
							headerShown: false
						}}
					/>
					<Stack.Screen
						name='Menu'
						component={MenuScreen}
						options={{
							animationEnabled: false,
							headerShown: false
						}}
					/>
					<Stack.Screen
						name='QRCode'
						component={QRCodeScreen}
						options={{
							animationEnabled: true,
							headerShown: false
						}}
					/>
					<Stack.Screen
						name='HistoricoLeituras'
						component={HistoricoLeiturasScreen}
						options={{
							animationEnabled: true,
							headerShown: false
						}}
					/>
					<Stack.Screen
						name='EstoqueBilhetes'
						component={EstoqueBilhetesScreen}
						options={{
							animationEnabled: true,
							headerShown: false
						}}
					/>
					<Stack.Screen
						name='EstatisticasAbastacimento'
						component={EstatisticasAbastacimentoScreen}
						options={{
							animationEnabled: true,
							headerShown: false
						}}
					/>
					<Stack.Screen
						name='EstatisticasFragmentacao'
						component={EstatisticasFragmentacaoScreen}
						options={{
							animationEnabled: true,
							headerShown: false
						}}
					/>
					<Stack.Screen
						name='Tabela'
						component={TabelaScreen}
						options={{
							animationEnabled: true,
							headerShown: false
						}}
					/>
					<Stack.Screen
						name='MovimentacaoDiaria'
						component={MovimentacaoDiariaScreen}
						options={{
							animationEnabled: true,
							headerShown: false
						}}
					/>
					<Stack.Screen
						name='ConsultarGTV'
						component={ConsultarGTVScreen}
						options={{
							animationEnabled: true,
							headerShown: false
						}}
					/>
					<Stack.Screen
						name='ConsultarAPF'
						component={ConsultarAPFScreen}
						options={{
							animationEnabled: true,
							headerShown: false
						}}
					/>
					<Stack.Screen
						name='ConsultarLote'
						component={ConsultarLoteScreen}
						options={{
							animationEnabled: true,
							headerShown: false
						}}
					/>
					<Stack.Screen
						name='ConsultarBilhete'
						component={ConsultarBilheteScreen}
						options={{
							animationEnabled: true,
							headerShown: false
						}}
					/>
					<Stack.Screen
						name='EstatisticasPosto'
						component={EstatisticasPostoScreen}
						options={{
							animationEnabled: true,
							headerShown: false
						}}
					/>
					<Stack.Screen
						name='FracionarLote'
						component={FracionarLoteScreen}
						options={{
							animationEnabled: false,
							headerShown: false
						}}
					/>
					<Stack.Screen
						name='BarCode'
						component={BarCodeScreen}
						options={{
							animationEnabled: true,
							headerShown: false
						}}
					/>
					<Stack.Screen
						name='NFC'
						component={NFCScreen}
						options={{
							animationEnabled: false,
							headerShown: false
						}}
					/>
					<Stack.Screen
						name='Lotes'
						component={LotesScreen}
						options={{
							animationEnabled: false,
							headerShown: false
						}}
					/>
					<Stack.Screen
						name='VincularOperador'
						component={VincularOperadorScreen}
						options={{
							animationEnabled: true,
							headerShown: false
						}}
					/>
					<Stack.Screen
						name='VincularCracha'
						component={VincularCrachaScreen}
						options={{
							animationEnabled: true,
							headerShown: false
						}}
					/>
					<Stack.Screen
						name='ValidarCancelamento'
						component={ValidarCancelamentoScreen}
						options={{
							animationEnabled: true,
							headerShown: false
						}}
					/>
					<Stack.Screen
						name='Documentos'
						component={DocumentosScreen}
						options={{
							animationEnabled: true,
							headerShown: false
						}}
					/>
				</Stack.Navigator>
			</NavigationContainer>
		</>
	);
};