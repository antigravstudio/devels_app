import React, { useEffect, useState } from 'react';
import { TouchableOpacity, Keyboard } from 'react-native';
import { Text, Button } from '@ui-kitten/components';
import Spinner from 'react-native-loading-spinner-overlay';
import * as Device from 'expo-device';
import * as Linking from 'expo-linking';
import * as FeatherIcon from "react-native-feather";
import * as Network from 'expo-network';
import * as Location from 'expo-location';

import api from '../service/ApiHandler';

import Store from '../utils/Store';
import Alert from '../utils/Alert';
import styles from '../utils/CustomStyles';

import { Form } from './subcomponents/Form';

export const LoginScreen = (props) => {    
    const [celular, setCelular] = useState('');
    const [login, setLogin] = useState('');
    const [loginInput, setLoginInput] = useState(null);
    const [senhaInput, setSenhaInput] = useState(null);
    const [senha, setSenha] = useState('');
    const [spinner, setSpinner] = useState(true);
    const [spinnerText, setSpinnerText] = useState('Aguarde...');
    const [perfil, setPerfil] = useState('');
    const [pedirLogin, setPedirLogin] = useState(true);
    const [tipoPerfil, setTipoPerfil] = useState('');
    const [urls, setUrls] = useState({});
    const [ambiente, setAmbiente] = useState('');

    const [inputs, setInputs] = useState([]);

    const entrarLoginSenha = async () => {
        if(!login && pedirLogin) Alert.simple({ subtitle: 'Informe seu e-mail' });
        else if(!senha && pedirLogin) Alert.simple({ subtitle: 'Informe uma senha' });
        else {
            setSpinnerText('Obtendo localização...');
            setSpinner(true);
            var location = '';
            try {
                let { status } = await Location.requestBackgroundPermissionsAsync();
                if (status !== 'granted') { 
                    location = {
                        coords: {
                            latitude: '',
                            longitude: ''
                        }
                    };
                } else {
                    location = await Location.getCurrentPositionAsync({});
                }
            } catch(e){
                location = {
                    coords: {
                        latitude: '',
                        longitude: ''
                    }
                };
            }
            setSpinnerText('Validando acesso...');
            setSpinner(true);
            const ip = await Network.getIpAddressAsync();
            const responseLogin = await api.primeiroLogin({
                nomeDispositivo: Device.deviceName,
                fabricante: Device.manufacturer,
                plataforma: Device.osName,
                versao: Device.osVersion,
                modelo: Device.modelName,
                celular,
                login,
                senha,
                perfil,
                ip,
                lat: location.coords.latitude,
                lon: location.coords.longitude
            });
            console.log(login, senha, responseLogin);
            if(responseLogin == 'nao_autorizado'){
                Alert.simple({ 
                    subtitle: 'Dados de Acesso invalidos',
                    accept: () => {
                        setSpinner(false);
                    }
                });
            } else if(responseLogin){
                setSpinnerText('Autenticando...');
                const valida = await api.validaLogin({ idDispositivo: responseLogin.dispositivoIdCriptografado });
                console.log('valida', valida);
                if(valida == 'nao_autorizado'){
                    Alert.simple({ 
                        subtitle: 'Dados de Acesso invalidos',
                        accept: () => {
                            setSpinner(false);
                        }
                    });
                } else if(valida){
                    await Store.set('idDispositivo', responseLogin.dispositivoIdCriptografado);
                    await Store.set('unidadeId', String(responseLogin.dispositivoIdCriptografado));
                    await Store.set('perfil', String(responseLogin.perfil));
                    console.log(String(responseLogin.perfil));
                    props.navigation.reset({ index: 0, routes: [{ name: 'Splash' }] });
                    setSpinner(false);
                } else {
                    Alert.simple({ 
                        subtitle: 'Erro de conexão',
                        accept: () => {
                            setSpinner(false);
                        }
                    });
                }
            } else {
                Alert.simple({ 
                    subtitle: 'Erro de conexão',
                    accept: () => {
                        setSpinner(false);
                    }
                });
            }
        }
    };

    const changeLogin = (text) => {
        setLogin(text);
    };

    const changeSenha = (text) => {
        setSenha(text);
    };

    useEffect(() => {
        //forceFocusLogin();
        (async () => {
            try {
                var pacote = require('../../package.json').expo.android.package;
                setUrls({
                    id: 'https://id.lyli.com.br/auth/realms/Lyli',
                    api: 'https://fenixapi.azurewebsites.net'
                });
                if(pacote == 'com.develshomologa.app'){
                    setUrls({
                        id: 'https://id.lyli.com.br/auth/realms/Lyli',
                        api: 'https://fenixapi-hom.azurewebsites.net'
                    });
                    var id = await Store.get('id-url');
                    var api = await Store.get('id-url');
                    if(id && api && api) setUrls({ id, api });
                    setAmbiente(' - Beta');
                } else {
                    setAmbiente(' - Produção');
                }
            } catch (e){}
            setSpinnerText('Aguarde...');
            setSpinner(true);
            const data = props.route.params.perfil;
            setPerfil(data);
            var _pedirLogin = pedirLogin;
            switch(data){
                case 'Transportadora':
                case 'SPTrans':
                    _pedirLogin = false;
                    break;
            }
            setPedirLogin(_pedirLogin);
            var _inputs = [];
            if(_pedirLogin){
                _inputs.push({
                    ref: (ref) => setLoginInput(ref),
                    defautValue: login,
                    style: {...styles.inputLogin, marginBottom: 5},
                    height: 40,
                    fontSize: 16,
                    textAlign: 'left',
                    label: <Text>Login</Text>,
                    caption: '',
                    status: '',
                    keyboardType: 'email-address',
                    onChangeText: changeLogin,
                    autoCorrect: false,
                    blurOnSubmit: false,
                    returnKeyType: 'done',
                    autoCapitalize: 'none',
                    onSubmitEditing: () => {
                        try {
                            senhaInput.focus()
                        } catch(e){
                            Keyboard.dismiss();
                        }
                    }
                });
            }
            _inputs.push({
                ref: (ref) => setSenhaInput(ref),
                defautValue: senha,
                style: styles.inputLogin,
                height: 40,
                fontSize: 16,
                textAlign: 'left',
                label: <Text>Senha</Text>,
                caption: '',
                status: '',
                secureTextEntry: true,
                onChangeText: changeSenha,
                autoCorrect: false,
                blurOnSubmit: true,
                autoCapitalize: 'none',
                returnKeyType: 'done',
                textContentType: 'password',
                maxLength: 12
            });
            setInputs(_inputs);
            console.log('Perfis', data);
            if(data.includes('Encarregado')) setTipoPerfil('Encarregado');
            if(data.includes('Zeladoria')) setTipoPerfil('Zeladoria'); 
            if(data.includes('Custódia')) setTipoPerfil('Custódia'); 
            if(data.includes('Transportadora')) setTipoPerfil('Transportadora'); 
            if(data.includes('Administrador')) setTipoPerfil('Administração'); 
            if(data.includes('SPTrans')) setTipoPerfil('SP Trans'); 
            if(data.includes('Outros')) setTipoPerfil('Outros'); 
            setTimeout(() => setSpinner(false), 1500);
        })();
    }, []);

    useEffect(() => () => {}, []);

    return (
        <>
            <Spinner
                visible={spinner}
                textContent={spinnerText}
                textStyle={{ color: '#fff'}}
                color="#fff"
                overlayColor="rgba(0, 0, 0, 0.85)"
                hidden={false}
                animation="fade"
            />
            <Form 
                {...props}
                simple={true}
                headerButton={() => (
                    <TouchableOpacity 
                        onPress={() => props.navigation.reset({ index: 0, routes: [{ name: 'Perfil' }], params: {urls} })}
                        style={styles.link}
                    >
                        <FeatherIcon.Edit 
                            color="#283757" 
                            height={22} width={22} 
                            style={styles.linkIcon}
                        />
                        <Text style={styles.linkText}>
                            {tipoPerfil}
                        </Text>
                    </TouchableOpacity>
                )}
                inputs={inputs}
                content={() => (<>
                    <Button 
                        status="success"
                        style={{
                            borderRadius: 8,
                            borderWidth: 0
                        }}
                        onPress={entrarLoginSenha}
                    >
                        ENTRAR
                    </Button>
                    <Text style={{
                        marginTop: 20,
                        fontSize: 14
                    }}>
                        Problemas para acessar?
                    </Text>
                    <TouchableOpacity 
                        onPress={async () => {
                            try {
                                await Linking.openURL('mailto:suporte@lyl.com.br');
                            } catch(e){}
                        }}
                        style={{
                            marginTop: 5
                        }}
                    >
                        <Text style={{
                            fontSize: 14,
                            fontWeight:'bold',
                            color: '#4996E9'
                        }}>
                            suporte@lyl.com.br
                        </Text>
                    </TouchableOpacity>
                    <Text style={{
                        marginTop: 20, 
                        textAlign: 'center',
                        fontStyle: 'italic'
                    }}>
                        Versão {require('../../app.json').expo.version} {ambiente}
                    </Text>
                </>)}
            />
        </>
    );
};