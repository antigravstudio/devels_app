import React, { useEffect, useState } from 'react';
import { 
    ScrollView, View, Text, TouchableOpacity
} from 'react-native';
import Spinner from 'react-native-loading-spinner-overlay';
import * as FeatherIcon from "react-native-feather";
import { Table, Row } from 'react-native-table-component';

import api from '../service/ApiHandler';

import styles from '../utils/CustomStyles';
import Store from '../utils/Store';
import Formater from '../utils/Formater';

import { Form } from './subcomponents/Form';

export const EstatisticasPostoScreen = (props) => {    
    const [spinner, setSpinner] = useState(true);
    const [spinnerText, setSpinnerText] = useState('Obtendo dados...');
    const [cards, setCards] = useState([]);
    const [contagem, setContagem] = useState('...');
    const [saldos, setSaldos] = useState(null);
    const [atualizacao, setAtualizacao] = useState('...');
    
    const widthArray = [120, 120, 120, 120];

    const [header, setHeader] = useState([]);

    const linhaTabela = (row) => {
        var row = Object.values(row);
        return [
            row[0], 
            row[1],
            row[2], 
            row[3],
            row[4]
        ];
    };

    const expandTable = (w, h, r) => {
        setSpinner(true);
        setSpinnerText('Aguarde...');
        setTimeout(() => {
            props.navigation.navigate('Tabela', {
                config: { 
                    widthArray: w,
                    header: h,
                    rows: r,
                    formater: linhaTabela
                }
            });
            setTimeout(() => {
                setSpinner(false);
            }, 1000);
        }, 1500);
    };

    useEffect(() => {
        async function obterDados(){
            setSpinnerText('Obtendo dados...');
            setSpinner(true);
            const idDispositivo = await Store.get('idDispositivo');
            const tipo = await Store.get('perfil');
            var chave = 'EncStats_posto';
            switch(tipo){
                case 'Custódia':
                case 'CUSTODIA_CANCELAMENTO':
                case 'CUSTODIA_SUPORTE':
                case 'CUSTODIA_ABASTECIMENTO':
                    chave = 'CtdStats_Abast';
                    break;
            }
            const response1 = await api.consulta({
                chave,
                idDispositivo
            });
            if(response1) {
                var _cards = [];
                var i_card = 0;
                var i = 0;
                for(var row of response1){
                    if(_cards[i_card] === undefined) _cards[i_card] = [];
                    _cards[i_card].push(row);
                    i++;
                    if(i == 2){
                        i = 0;
                        i_card++;
                    }
                }
                setCards(_cards);
            } else setCards([]);
            const response2 = await api.consulta({
                chave: 'EncStats_posto3',
                idDispositivo
            });
            if(response2) {
                try {
                    setContagem(response2[0].canceladoS_ONTEM);
                } catch(e){}
            } else setContagem('...');
            const response3 = await api.consulta({
                chave: 'EncStats_posto2',
                idDispositivo
            });
            if(response3) {
                var first = response3[0];
                first = Object.keys(first);
                setHeader([
                    first[0]?first[0]:'...', 
                    first[1]?first[1]:'...',
                    first[2]?first[2]:'...', 
                    first[3]?first[3]:'...',
                ])
                setSaldos(response3);
            } else setSaldos(null);
            setSpinner(false);
            setAtualizacao(Formater.formatarDataHora(new Date()));
        }
        obterDados();
    }, []);

    const fundoCard = (i1, i2) => {
        if(i1 % 2 == 0 && i2 == 0) return '#FF9500';
        if(i1 % 2 == 0 && i2 == 1) return '#4CE4B1';
        if(Math.abs(i1 % 2) == 1 && i2 == 0) return '#FF2D55';
        if(Math.abs(i1 % 2) == 1 && i2 == 1) return '#219AFF';
        return '#FF9500';
    };

    useEffect(() => () => {}, []);

    return (
        <>
            <Spinner
                visible={spinner}
                textContent={spinnerText}
                textStyle={{ color: '#fff'}}
                color="#fff"
                overlayColor="rgba(0, 0, 0, 0.85)"
                hidden={false}
                animation="fade"
            />
            <Form 
                {...props}
                title="Estoque de Bilhetes"
                menu={true}
                content={() => (<>
                    {
                        cards.map((row, index) => (
                            <View 
                                key={index}
                                style={{ 
                                    width: '100%',
                                    flexDirection: 'row',
                                    marginBottom: 10,
                                    alignItems: 'center',
                                    justifyContent: 'center'
                                }}
                            >
                                {
                                    row.map((_row, _index) => (
                                        <View 
                                            key={_index}
                                            style={{
                                                ...styles.dashCard,
                                                backgroundColor: fundoCard(index, _index),
                                                marginHorizontal: '2%'
                                            }}
                                        >
                                            <FeatherIcon.Monitor style={styles.dashIcon} />
                                            <Text style={styles.dashTitle}>
                                                {
                                                    (_row.posto?_row.posto:'')
                                                        .toUpperCase()
                                                        .replace('POSTO', 'P.')
                                                        .replace('TERMINAL', 'TERM.')
                                                }
                                                {
                                                    (_row.tipo?_row.tipo:'')
                                                        .toUpperCase()
                                                }
                                            </Text>
                                            <Text style={styles.dashSubtitle}>
                                                {_row.saldo}
                                            </Text>
                                        </View>
                                    ))
                                }
                            </View>
                        ))
                    }
                    {
                        saldos ?
                        <>
                            <Text style={{ ...styles.viewTitle, marginTop: 24 }}>
                                Saldos de Bilhetes por Operador
                            </Text>
                            <ScrollView 
                                bounces={false} 
                                showsVerticalScrollIndicator={false} 
                                showsHorizontalScrollIndicator={false} 
                                horizontal={true}
                                vertical={true}
                                style={{
                                    width: '100%',
                                    marginBottom: 10
                                }}
                            >
                                <Table borderStyle={styles.tableBorderStyle} style={{
                                    width: '100%'
                                }}>
                                    <Row 
                                        data={header} 
                                        textStyle={styles.tableHeaderText}
                                        style={styles.tableHeader}
                                        widthArr={widthArray}
                                    />
                                    {
                                        saldos.slice(0, 5).map((row, index) => (
                                            <TouchableOpacity key={index}>
                                                <Row
                                                    data={linhaTabela(row)}
                                                    style={[styles.row, index%2 && {backgroundColor: '#EEF5FE'}]}
                                                    textStyle={{padding: 10, textAlign: 'center'}}
                                                    widthArr={widthArray}
                                                />
                                            </TouchableOpacity>
                                        ))
                                    }
                                </Table>
                            </ScrollView>
                            {
                                saldos.length > 5 ?
                                <TouchableOpacity 
                                    onPress={() => expandTable(widthArray, header, saldos)}
                                    style={styles.link}
                                >
                                    <Text style={styles.linkText}>
                                        Ver Mais
                                    </Text>
                                </TouchableOpacity> : <></>
                            }
                        </> : <></>
                    }
                    <View style={{ 
                        width: '100%',
                        flexDirection: 'column',
                        alignItems: 'center',
                        justifyContent: 'center',
                        marginBottom: 10,
                    }}>
                        <View style={{
                            ...styles.simpleCard, 
                            width: '96%',
                            marginBottom: 10
                        }}>
                            <Text style={{
                                ...styles.simpleCardTitle,
                                textAlign: 'left'
                            }}>
                                Cancelados Dia Anterior
                            </Text>
                            <Text style={{
                                ...styles.simpleCardSubtitle,
                                textAlign: 'left'
                            }}>
                                {contagem}
                            </Text>
                        </View>
                        <View style={styles.simpleCard}>
                            <Text style={styles.simpleCardTitle}>
                                Última Atualização
                            </Text>
                            <Text style={styles.simpleCardSubtitle}>
                                {atualizacao}
                            </Text>
                        </View>
                    </View>
                </>)}
            />
        </>
    );
};