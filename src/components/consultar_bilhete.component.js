
import React, { useEffect, useState } from 'react';
import { 
    TouchableWithoutFeedback, ScrollView, TouchableOpacity, Text, View, Keyboard
} from 'react-native';
import { Button } from '@ui-kitten/components';
import Spinner from 'react-native-loading-spinner-overlay';
import * as FeatherIcon from "react-native-feather";
import { Table, Row } from 'react-native-table-component';

import styles from '../utils/CustomStyles';
import Formater from '../utils/Formater';
import Store from '../utils/Store';

import api from '../service/ApiHandler';

import { Form } from './subcomponents/Form';
import { Modal as ModalBilhete } from './subcomponents/Modal';

export const ConsultarBilheteScreen = (props) => {    
    const [spinner, setSpinner] = useState(false);
    const [spinnerText, setSpinnerText] = useState('Aguarde...');
    const [modalBilhete, setModalBilhete] = useState(false);
    const [dados, setDados] = useState(null);
    const [pagina, setPagina] = useState(0);
    const [filtro, setFiltro] = useState('');
    const [dadosModal, setDadosModal] = useState({});

    const searchIcon = () => (<TouchableWithoutFeedback><FeatherIcon.Search width={24} height={24} color="#4E86FB" /></TouchableWithoutFeedback>);

    const widthArray = [100, 120, 300, 150, 120, 100];

    const pesquisar = async () => {
        setSpinner(true);
        setSpinnerText('Consultando...');
        const idDispositivo = await Store.get('idDispositivo');
        const response = await api.consulta({
            chave: filtro ? 'AdmConsult_BilheteBusca' : 'AdmConsult_BilheteInicial',
            filtro: filtro ? filtro.toUpperCase() : null,
            idDispositivo
        });
        if(response) {
            setPagina(0);
            try {
                var paginas = [];
                var i_pagina = 0;
                var i = 0;
                for(var row of response){
                    row.dataCriacao = Formater.formatarData(row.dataCriacao);
                    if(paginas[i_pagina] === undefined) paginas[i_pagina] = [];
                    paginas[i_pagina].push(row);
                    i++;
                    if(i == 5){
                        i = 0;
                        i_pagina++;
                    }
                }
                setDados(paginas);
            } catch(e){
                setDados([]);
            }
            setSpinner(false);
        } else {
            setDados([]);
            setSpinner(false);
        }
    };

    const abrirBilhete = async (id) => {
        setSpinner(true);
        setSpinnerText('Aguarde...');
        const idDispositivo = await Store.get('idDispositivo');
        const response = await api.consulta({
            chave: 'AdmConsult_BilheteSelecao',
            filtro: id,
            idDispositivo
        });
        var credito = '?';
        const response2 = await api.consulta({
            chave: 'AdmConsult_BilheteSelecaoCredito',
            filtro: id,
            idDispositivo
        });
        if(response2){
            if(response2.length){
                console.log(response2);
                if(response2[0].credito){
                    credito = response2[0].credito
                } else {
                    credito = 'R$ 0,00'; 
                }
            } else {
                credito = 'R$ 0,00';
            }
        } else {
            credito = 'R$ 0,00';
        }
        const response3 = await api.consulta({
            chave: 'Consulta_Recargas',
            filtro: id,
            idDispositivo
        });
        const response4 = await api.consulta({
            chave: 'Consulta_OutrosBilhetes',
            filtro: id,
            idDispositivo
        });
        //console.log(id);
        //.log('Consulta_Recargas');
        //console.log(response3);
        //console.log('Consulta_OutrosBilhetes');
        //console.log(response4);
        if(!response || !response.length) {
            setDadosModal({ 
                bilheteId: id, 
                table: [], 
                credito, 
                recargas: response3,
                bilhetes: response4
            });
            setModalBilhete(true);
            setSpinner(false);
        } else {
            setDadosModal({ 
                bilheteId: id, 
                table: response, 
                credito, 
                recargas: response3,
                bilhetes: response4
            });
            setModalBilhete(true);
            setSpinner(false);
        }
    };

    const changeFiltro = (text) => {
        setFiltro(text);
    };

    useEffect(() => {
        pesquisar();
    }, []);

    useEffect(() => () => {}, []);

    return (
        <>
            <ModalBilhete
                show={modalBilhete} 
                close={setModalBilhete} 
                content={() => (
                    <>
                        <Text style={{
                            color: '#fff',
                            marginBottom: 5,
                            fontSize: 22,
                            fontWeight: '400'
                        }}>Rastreamento do Bilhete Único</Text>
                        <Text style={{
                            color: '#fff',
                            marginBottom: 20,
                            fontSize: 22,
                            fontWeight: '400'
                        }}>[Athens]</Text>
                        {
                            (dadosModal.table?dadosModal.table:[]).length ?
                            <>
                                {
                                    dadosModal.table.map((row, index) => (
                                        <React.Fragment key={index}>
                                            <View style={{...styles.table, flexDirection: 'column', marginBottom: 15, width: '100%', flex: 1 }}>
                                                <View style={{ flexDirection: 'row', width: '100%' }}>
                                                    <View style={{ width: '50%' }}>
                                                        <Text style={{ fontWeight: 'bold', fontSize: 16 }}>Bilhete Único:</Text>
                                                        <Text style={{ fontSize: 16 }}>{row.numerO_BILHETE?row.numerO_BILHETE:'...'}</Text>
                                                    </View>
                                                    <View style={{ width: '50%' }}>
                                                        <Text style={{ fontWeight: 'bold', fontSize: 16, color: 'black' }}>Lote:</Text>
                                                        <Text style={{ fontSize: 16 }}>{row.numerO_SUBLOTE?row.numerO_SUBLOTE.split('-')[0]:'...'}</Text>
                                                    </View>
                                                </View>
                                                <View style={{ flexDirection: 'row', width: '100%', marginTop: 10 }}>
                                                    <View style={{ width: '100%' }}>
                                                        <Text style={{ fontWeight: 'bold', fontSize: 16 }}>Impresso:</Text>
                                                        <Text style={{ fontSize: 16 }}>{row.DATA_DISPONIBILIZADO_SPTRANS?'Dia '+row.DATA_DISPONIBILIZADO_SPTRANS:'...'}</Text>
                                                    </View>
                                                </View>
                                            </View>
                                            <View style={{...styles.table, flexDirection: 'column', marginBottom: 15, width: '100%', flex: 1 }}>
                                                <Text style={{ fontSize: 16, marginBottom: 5 }}>
                                                    <Text style={{ fontWeight: 'bold' }}>Transporte</Text>{' '}- Transportadora
                                                </Text>
                                                <View style={{ flexDirection: 'row', width: '100%', paddingLeft: 10 }}>
                                                    <View style={{ width: '100%' }}>
                                                        <Text style={{ fontSize: 16 }}>
                                                            <FeatherIcon.ArrowRightCircle 
                                                                color="#000"
                                                                width={14}
                                                                height={14}
                                                                style={{ 
                                                                    alignSelf: 'center', 
                                                                    marginRight: 5, 
                                                                    flexDirection: 'row'
                                                                }}
                                                            /> {row.datA_SAIDA_SPTRANS?`${row.datA_SAIDA_SPTRANS} - ${row.horA_SAIDA_SPTRANS.substring(0, 5)}`:'...'}
                                                            {' '}- <Text style={{fontStyle:'italic'}}>Saída SP Trans</Text>
                                                        </Text>
                                                        <Text style={{ fontSize: 16, marginTop: 5 }}>
                                                            <FeatherIcon.ArrowRightCircle 
                                                                color="#000"
                                                                width={14}
                                                                height={14}
                                                                style={{ 
                                                                    alignSelf: 'center', 
                                                                    marginRight: 5, 
                                                                    flexDirection: 'row'
                                                                }}
                                                            /> {row.datA_CHEGADA_CUSTODIA?`${row.datA_CHEGADA_CUSTODIA} - ${row.horA_CHEGADA_CUSTODIA.substring(0, 5)}`:'...'}
                                                            {' '}- <Text style={{fontStyle:'italic'}}>Chegada Custódia</Text>
                                                        </Text>
                                                        <Text style={{ fontSize: 16, marginTop: 5 }}>
                                                            <FeatherIcon.ArrowRightCircle 
                                                                color="#000"
                                                                width={14}
                                                                height={14}
                                                                style={{ 
                                                                    alignSelf: 'center', 
                                                                    marginRight: 5, 
                                                                    flexDirection: 'row'
                                                                }}
                                                            /> {row.datA_SAIDA_CUSTODIA?`${row.datA_SAIDA_CUSTODIA} - ${row.horA_SAIDA_CUSTODIA.substring(0, 5)}`:'...'}
                                                            {' '}- <Text style={{fontStyle:'italic'}}>Saída Custódia</Text>
                                                        </Text>
                                                        <Text style={{ fontSize: 16, marginTop: 5 }}>
                                                            <FeatherIcon.ArrowRightCircle 
                                                                color="#000"
                                                                width={14}
                                                                height={14}
                                                                style={{ 
                                                                    alignSelf: 'center', 
                                                                    marginRight: 5, 
                                                                    flexDirection: 'row'
                                                                }}
                                                            /> {row.datA_CHEGADA_POSTO?`${row.datA_CHEGADA_POSTO} - ${row.horA_CHEGADA_POSTO.substring(0, 5)}`:'...'}
                                                            {' '}- <Text style={{fontStyle:'italic'}}>Chegada Posto</Text>
                                                        </Text>
                                                    </View>
                                                </View>
                                            </View>
                                            <View style={{...styles.table, flexDirection: 'column', marginBottom: 15, width: '100%', flex: 1 }}>
                                                <Text style={{ fontSize: 16, marginBottom: 5 }}>
                                                    <Text style={{ fontWeight: 'bold' }}>Entrega ao Público</Text>
                                                </Text>
                                                <View style={{ flexDirection: 'row', width: '100%', paddingLeft: 10 }}>
                                                    <View style={{ width: '100%' }}>
                                                        <Text style={{ fontSize: 16 }}>
                                                            <FeatherIcon.ArrowRightCircle 
                                                                color="#000"
                                                                width={14}
                                                                height={14}
                                                                style={{ 
                                                                    alignSelf: 'center', 
                                                                    marginRight: 5 
                                                                }}
                                                            />
                                                           Atendente - {Formater.capitalizer(row.operador)}
                                                        </Text>
                                                        <Text style={{ fontSize: 16 }}>
                                                            <FeatherIcon.ArrowRightCircle 
                                                                color="#000"
                                                                width={14}
                                                                height={14}
                                                                style={{ 
                                                                    alignSelf: 'center', 
                                                                    marginRight: 5 
                                                                }}
                                                            />
                                                           {row.datA_ENTREGA_AO_USUARIO} <Text style={{fontStyle:'italic'}}>entrega ao usuário</Text>
                                                        </Text>
                                                        <Text style={{ fontSize: 16 }}>
                                                            <FeatherIcon.ArrowRightCircle 
                                                                color="#000"
                                                                width={14}
                                                                height={14}
                                                                style={{ 
                                                                    alignSelf: 'center', 
                                                                    marginRight: 5 
                                                                }}
                                                            />
                                                           Nome - {Formater.capitalizer(row.nomE_USUARIO)}
                                                        </Text>
                                                        <Text style={{ fontSize: 16 }}>
                                                            <FeatherIcon.ArrowRightCircle 
                                                                color="#000"
                                                                width={14}
                                                                height={14}
                                                                style={{ 
                                                                    alignSelf: 'center', 
                                                                    marginRight: 5 
                                                                }}
                                                            />
                                                           CPF - {Formater.formatarDocumento(row.cpF_USUARIO)}
                                                        </Text>
                                                        <Text style={{ fontSize: 16 }}>
                                                            <FeatherIcon.ArrowRightCircle 
                                                                color="#000"
                                                                width={14}
                                                                height={14}
                                                                style={{ 
                                                                    alignSelf: 'center', 
                                                                    marginRight: 5 
                                                                }}
                                                            />
                                                           Crédito - {dadosModal.credito}
                                                        </Text>
                                                    </View>
                                                </View>
                                            </View>
                                        </React.Fragment>
                                    ))
                                }
                            </> :  
                            <View style={{...styles.table, flexDirection: 'column', marginBottom: 15, width: '100%' }}>
                                <View style={{ flexDirection: 'row', width: '100%' }}>
                                    <View style={{ width: '100%' }}>
                                        <Text style={{ fontWeight: 'bold', fontSize: 16 }}>Bilhete Único:</Text>
                                        <Text style={{ fontSize: 16 }}>{dadosModal.bilheteId}</Text>
                                    </View>
                                </View>
                            </View>
                        }
                        {
                            (dadosModal.recargas?dadosModal.recargas:[]).length ?
                            <>
                                <View style={{...styles.table, flexDirection: 'column', marginBottom: 15, width: '100%', paddingHorizontal: 0, paddingBottom: 0 }}>
                                    <Text style={{ fontSize: 16, marginBottom: 15, paddingHorizontal: 10 }}>
                                        <Text style={{ fontWeight: 'bold' }}>Recargas do Bilhete</Text>
                                    </Text>
                                    <ScrollView 
                                        bounces={false} 
                                        showsVerticalScrollIndicator={false} 
                                        showsHorizontalScrollIndicator={false} 
                                        horizontal={true}
                                        vertical={true}
                                        style={{
                                            width: '100%',
                                            borderBottomLeftRadius: 6,
                                            borderBottomRightRadius: 6
                                        }}
                                    >
                                        <Table borderStyle={styles.tableBorderStyle} style={{ 
                                            width: '100%', backgroundColor: 'white' 
                                        }}>
                                            <Row 
                                                data={[
                                                    'Unidade',
                                                    'Operador',
                                                    'Recarga',
                                                    'Confirmação',
                                                    'NSU',
                                                    'HMNSU',
                                                    'Tipo',
                                                    'Valor',
                                                    'Status',
                                                ]} 
                                                textStyle={styles.tableHeaderText}
                                                style={styles.tableHeader}
                                                widthArr={[150, 150, 150, 150, 150, 150, 100, 80, 100]}
                                            />
                                            {
                                                dadosModal.recargas.map((row, index) => (
                                                    <Row
                                                        key={index}
                                                        data={[
                                                            row.unidade?row.unidade:'...',
                                                            row.operador?Formater.capitalizer(row.operador):'...',
                                                            row.recarga?Formater.formatarDataHora(row.recarga):'...',
                                                            row.confirmacao?Formater.formatarDataHora(row.confirmacao):'...',
                                                            row.nsu?row.nsu:'...',
                                                            row.hmnsu?row.hmnsu:'...',
                                                            row.tipo?row.tipo:'...',
                                                            row.valor?row.valor:'...',
                                                            row.status?row.status:'...',
                                                        ]}
                                                        style={[styles.row, index%2 && {backgroundColor: '#EEF5FE'}]}
                                                        textStyle={{padding: 10, textAlign: 'center'}}
                                                        widthArr={[150, 150, 150, 150, 150, 150, 100, 80, 100]}
                                                    />
                                                ))
                                            }
                                        </Table>
                                    </ScrollView>
                                </View>
                            </> : <></>
                        }
                        {
                            (dadosModal.bilhetes?dadosModal.bilhetes:[]).length ?
                            <>
                                <View style={{...styles.table, flexDirection: 'column', marginBottom: 15, width: '100%', flex: 1, paddingHorizontal: 0, paddingBottom: 0 }}>
                                    <Text style={{ fontSize: 16, marginBottom: 15, paddingHorizontal: 10 }}>
                                        <Text style={{ fontWeight: 'bold' }}>Todos os Bilhetes do Usuário</Text>
                                    </Text>
                                    <ScrollView 
                                        bounces={false} 
                                        showsVerticalScrollIndicator={false} 
                                        showsHorizontalScrollIndicator={false} 
                                        horizontal={true}
                                        vertical={true}
                                        style={{
                                            width: '100%',
                                            borderBottomLeftRadius: 6,
                                            borderBottomRightRadius: 6
                                        }}
                                    >
                                        <Table borderStyle={styles.tableBorderStyle} style={{ 
                                            width: '100%', backgroundColor: 'white' 
                                        }}>
                                            <Row 
                                                data={[
                                                    'Unidade',
                                                    'Operador',
                                                    'Personalização',
                                                    'Lote',
                                                    'Bilhete',
                                                    'Tipo',
                                                    'Nascimento',
                                                    'Usuário',
                                                    'CPF',
                                                ]} 
                                                textStyle={styles.tableHeaderText}
                                                style={styles.tableHeader}
                                                widthArr={[150, 150, 150, 150, 150, 150, 150, 150, 150]}
                                            />
                                            {
                                                dadosModal.bilhetes.map((row, index) => (
                                                    <Row
                                                        key={index}
                                                        data={[
                                                            row.unidade?row.unidade:'...',
                                                            row.operador?Formater.capitalizer(row.operador):'...',
                                                            row.personalizacao?Formater.formatarData(row.personalizacao):'...',
                                                            row.lote?row.lote:'...',
                                                            row.bilhete?row.bilhete:'...',
                                                            row.tipo?row.tipo:'...',
                                                            row.nascimento?row.nascimento:'...',
                                                            row.usuario?row.usuario:'...',
                                                            row.cpf?Formater.formatarDocumento(row.cpf):'...',
                                                        ]}
                                                        style={[styles.row, index%2 && {backgroundColor: '#EEF5FE'}]}
                                                        textStyle={{padding: 10, textAlign: 'center'}}
                                                        widthArr={[150, 150, 150, 150, 150, 150, 150, 150, 150]}
                                                    />
                                                ))
                                            }
                                        </Table>
                                    </ScrollView>
                                </View>
                            </> : <></>
                        }
                    </>
                )}
            />
            <Spinner
                visible={spinner}
                textContent={spinnerText}
                textStyle={{ color: '#fff'}}
                color="#fff"
                overlayColor="rgba(0, 0, 0, 0.85)"
                hidden={false}
                animation="fade"
            />
            <Form 
                {...props}
                title="Consultar Bilhete"
                menu={true}
                inputs={[{
                    style: { marginBottom: 10 },
                    accessoryLeft: searchIcon,
                    placeholder: "BUSCA POR Nº DO BILHETE",
                    textStyle: { textAlign: 'center' },
                    autoCorrect: false,
                    defautValue: filtro,
                    autoCapitalize: "none",
                    autoCorrect: false,
                    returnKeyType: 'done',
                    keyboardType: 'number-pad',
                    autoCapitalize: 'none',
                    onChangeText: changeFiltro,
                    onSubmitEditing: pesquisar
                }]}
                content={() => (<>
                    <Button
                        size="small" 
                        style={{ 
                            marginBottom: 10, width: 100, alignSelf: 'flex-end',
                        }}
                        onPress={() => { pesquisar(); Keyboard.dismiss(); }}
                    >Pesquisar</Button>
                    {
                        dados !== null ?
                        <>
                            {
                                dados.length ?
                                <>
                                    <ScrollView 
                                        bounces={false} 
                                        showsVerticalScrollIndicator={false} 
                                        showsHorizontalScrollIndicator={false} 
                                        horizontal={true}
                                        vertical={true}
                                        style={{
                                            width: '100%',
                                            marginBottom: 5
                                        }}
                                    >
                                        <Table borderStyle={styles.tableBorderStyle} style={{
                                            width: '100%'
                                        }}>
                                            <Row 
                                                data={[
                                                    'Data',
                                                    'Nº Bilhete',
                                                    'Usuário',
                                                    'CPF',
                                                    'Nasc.',
                                                    'Visualizar'
                                                ]}
                                                textStyle={styles.tableHeaderText}
                                                style={styles.tableHeader}
                                                widthArr={widthArray}
                                            />
                                            {
                                                dados[pagina].map((row, index) => (
                                                    <TouchableOpacity key={index} onPress={() => abrirBilhete(row.numerO_BILHETE)}>
                                                        <Row
                                                            data={[
                                                                row.data?Formater.formatarData(row.data):'...',
                                                                row.numerO_BILHETE?row.numerO_BILHETE:'...',
                                                                row.nomE_USUARIO?row.nomE_USUARIO:'...',
                                                                row.cpF_USUARIO?Formater.formatarDocumento(row.cpF_USUARIO):'...',
                                                                row.datA_NASCIMENTO_USUARIO?Formater.formatarData(row.datA_NASCIMENTO_USUARIO):'...',
                                                                <FeatherIcon.Search 
                                                                    color="#4E86FB"
                                                                    width={18}
                                                                    height={18}
                                                                    style={{ alignSelf: 'center' }}
                                                                />
                                                            ]}
                                                            style={[styles.row, index%2 && {backgroundColor: '#EEF5FE'}]}
                                                            textStyle={{padding: 10, textAlign: 'center'}}
                                                            widthArr={widthArray}
                                                        />
                                                    </TouchableOpacity>
                                                ))
                                            }
                                        </Table>
                                    </ScrollView>
                                    <View style={styles.pagination}>
                                        {
                                            dados.map((row, index) => (
                                                <TouchableOpacity 
                                                    key={index}
                                                    style={{...styles.paginationDot, backgroundColor: pagina == index ? '#E7EAFF' : 'white'}}
                                                    onPress={() => setPagina(index)}
                                                >
                                                    <Text style={{fontSize: 18}}>{index + 1}</Text>
                                                </TouchableOpacity>
                                            ))
                                        }
                                    </View>
                                </> : <>
                                <View style={{...styles.warningAlert, marginBottom: 30 }}>
                                        <Text style={styles.alertText}>
                                            NENHUM DADO FOI LOCALIZADO
                                        </Text>
                                    </View>
                                </>
                            }
                        </> : <></>
                    }
                </>)}
            />
        </>
    );
};