import React, { useEffect, useState } from 'react';
import { 
    Text, TouchableOpacity
} from 'react-native';
import * as Linking from 'expo-linking';

import styles from '../utils/CustomStyles';
import Store from '../utils/Store';
import Alert from '../utils/Alert';

import { Form } from './subcomponents/Form';

export const LotesScreen = (props) => {   
    const [telas, setTelas] = useState({});

    const acessarTela = (tela) => {
        if(tela == 'qrcode') props.navigation.navigate('QRCode');
        else if(tela == 'historico-leituras') props.navigation.navigate('HistoricoLeituras');
        else if(tela == 'estoque-bilhetes') props.navigation.navigate('EstoqueBilhetes');
        else if(tela == 'estatisticas-abastecimento') props.navigation.navigate('EstatisticasAbastacimento');
        else if(tela == 'estatisticas-fragmentacao') props.navigation.navigate('EstatisticasFragmentacao');
        else if(tela == 'movimentacao-diaria') props.navigation.navigate('MovimentacaoDiaria');
        else if(tela == 'consultar-gtv') props.navigation.navigate('ConsultarGTV');
        else if(tela == 'consultar-apf') props.navigation.navigate('ConsultarAPF');
        else if(tela == 'estatisticas-posto') props.navigation.navigate('EstatisticasPosto');
        else if(tela == 'fracionar-lotes') props.navigation.reset({ 
            index: 0, routes: [{ name: 'FracionarLote' 
        }]});
        else if(tela == 'consultar-lote') props.navigation.navigate('ConsultarLote');
        else if(tela == 'consultar-bilhete') props.navigation.navigate('ConsultarBilhete');
        else Alert.simple({ subtitle: 'Rota em Desenvolvimento' });
    };

    useEffect(() => {
        async function carregarTelas(){
            const perfil = await Store.get('perfil');
            var _telas = {};
            switch (perfil){
                case 'Encarregado':
                case 'ENCARREGADO':
                case 'ZELADORIA':
                case 'AGENTE':
                    _telas.qrcode = true;
                    _telas.historico_leituras = true;
                    //_telas.estoque_bilhetes = true;
                    _telas.fracionar_lotes = true;
                    _telas.movimentacao_diaria = true;
                    _telas.consultar_gtv = true;
                    _telas.estatisticas_posto = true;
                    break;
                case 'Custódia':
                case 'CUSTODIA_CANCELAMENTO':
                case 'CUSTODIA_SUPORTE':
                case 'CUSTODIA_ABASTECIMENTO':
                    _telas.qrcode = true;
                    _telas.historico_leituras = true;
                    _telas.estoque_bilhetes = true;
                    _telas.movimentacao_diaria = true;
                    _telas.consultar_gtv = true;
                    _telas.consultar_apf = true;
                    _telas.estatisticas_abastecimento = true;
                    _telas.estatisticas_fragmentacao = true;
                    break;
                case 'Transportadora':
                    _telas.qrcode = true;
                    _telas.historico_leituras = true;
                    _telas.consultar_gtv = true;
                    break;
                case 'Administrador':
                case 'DIRETORIA':
                case 'ADM_CCO':
                case 'LYLI':
                    _telas.consultar_lote = true;
                    _telas.consultar_bilhete = true;
                    break;
                case 'SPTrans':
                case 'SPTRANS':
                    _telas.qrcode = true;
                    _telas.historico_leituras = true;
                    _telas.consultar_lote = true;
                    _telas.consultar_bilhete = true;
                    break;
            }
            setTelas(_telas);
        }
        carregarTelas();
    }, []);

    useEffect(() => () => {}, []);

    return (
        <>
            <Form 
                {...props}
                title="O que deseja fazer?"
                logout={true}
                content={() => (<>
                    {
                        telas.qrcode ?
                        <TouchableOpacity 
                            onPress={() => acessarTela('qrcode')}
                            style={styles.link}
                        >
                            <Text style={styles.linkText}>
                                Ler QR Code
                            </Text>
                        </TouchableOpacity> : <></>
                    }
                    {
                        telas.historico_leituras ?
                        <TouchableOpacity 
                            onPress={() => acessarTela('historico-leituras')}
                            style={styles.link}
                        >
                            <Text style={styles.linkText}>
                                Histórico de Leituras
                            </Text>
                        </TouchableOpacity> : <></>
                    }
                    {
                        telas.estoque_bilhetes ?
                        <TouchableOpacity 
                            onPress={() => acessarTela('estoque-bilhetes')}
                            style={styles.link}
                        >
                            <Text style={styles.linkText}>
                                Estoque de Bilhetes
                            </Text>
                        </TouchableOpacity> : <></>
                    }
                    {
                        telas.fracionar_lotes ?
                        <TouchableOpacity 
                            onPress={() => acessarTela('fracionar-lotes')}
                            style={styles.link}
                        >
                            <Text style={styles.linkText}>
                                Fracionar Lotes
                            </Text>
                        </TouchableOpacity> : <></>
                    }
                    {
                        telas.movimentacao_diaria ?
                        <TouchableOpacity 
                            onPress={() => acessarTela('movimentacao-diaria')}
                            style={styles.link}
                        >
                            <Text style={styles.linkText}>
                                Movimentação Diária
                            </Text>
                        </TouchableOpacity> : <></>
                    }
                    {
                        telas.consultar_gtv ?
                        <TouchableOpacity 
                            onPress={() => acessarTela('consultar-gtv')}
                            style={styles.link}
                        >
                            <Text style={styles.linkText}>
                                Consultar Cancelamento
                            </Text>
                        </TouchableOpacity> : <></>
                    }
                    {
                        telas.consultar_apf ?
                        <TouchableOpacity 
                            onPress={() => acessarTela('consultar-apf')}
                            style={styles.link}
                        >
                            <Text style={styles.linkText}>
                                Consultar APF
                            </Text>
                        </TouchableOpacity> : <></>
                    }
                    {
                        telas.estatisticas_abastecimento ?
                        <TouchableOpacity 
                            onPress={() => acessarTela('estatisticas-abastecimento')}
                            style={styles.link}
                        >
                            <Text style={styles.linkText}>
                                Estatísticas do Abastecimento
                            </Text>
                        </TouchableOpacity> : <></>
                    }
                    {
                        telas.estatisticas_fragmentacao ?
                        <TouchableOpacity 
                            onPress={() => acessarTela('estatisticas-fragmentacao')}
                            style={styles.link}
                        >
                            <Text style={styles.linkText}>
                                Estatísticas da Fragmentação
                            </Text>
                        </TouchableOpacity> : <></>
                    }
                    {
                        telas.estatisticas_posto ?
                        <TouchableOpacity 
                            onPress={() => acessarTela('estatisticas-posto')}
                            style={styles.link}
                        >
                            <Text style={styles.linkText}>
                                Estatísticas do Posto
                            </Text>
                        </TouchableOpacity> : <></>
                    }
                    {
                        telas.consultar_lote ?
                        <TouchableOpacity 
                            onPress={() => acessarTela('consultar-lote')}
                            style={styles.link}
                        >
                            <Text style={styles.linkText}>
                                Consultar Lote
                            </Text>
                        </TouchableOpacity> : <></>
                    }
                    {
                        telas.consultar_bilhete ?
                        <TouchableOpacity 
                            onPress={() => acessarTela('consultar-bilhete')}
                            style={styles.link}
                        >
                            <Text style={styles.linkText}>
                                Consultar Bilhete
                            </Text>
                        </TouchableOpacity> : <></>
                    }
                    <TouchableOpacity 
                        onPress={async () => {
                            try {
                                await Linking.openURL('https://lyli.com.br/contato/');
                            } catch(e){}
                        }}
                        style={styles.link}
                    >
                        <Text style={{...styles.linkText, textAlign: 'center' }}>
                            Dúvidas em como usar o App?{'\n'}
                            <Text style={{ fontWeight: 'bold' }}>Clique Aqui</Text>
                        </Text>
                    </TouchableOpacity>
                </>)}
            />
        </>
    );
};