import React, { useEffect } from 'react';
import { Image, View } from  'react-native';
import { StatusBar } from 'expo-status-bar';
import * as Linking from 'expo-linking';
import * as Updates from 'expo-updates';
import * as ExpoSplashScreen from 'expo-splash-screen';
import axios from 'axios';

import Store from '../utils/Store';

export const SplashScreen = ({ navigation }) => {
    useEffect(() => {
        const load = async () => {
            try {
                await ExpoSplashScreen.hideAsync();
            } catch(e){}
            try {
                const update = await Updates.checkForUpdateAsync();
                if (update.isAvailable) {
                    await Updates.fetchUpdateAsync();
                    await Updates.reloadAsync();
                }
            } catch(e){}
            setTimeout(async () => {
                var urls = {};
                try {
                    await Store.remove('api-url');
                    await Store.remove('id-url');
                    var response = await axios.get('https://fenix.lyli.com.br/api.json');
                    if(response.data){
                        if(response.data.api_URL) await Store.set('api-url', response.data.api_URL);
                        if(response.data.ids_URL) await Store.set('id-url', response.data.ids_URL);
                        urls = { api: response.data.api_URL, id: response.data.ids_URL };
                    }
                } catch(e){}
                const idDispositivo = await Store.get('idDispositivo');
                if(idDispositivo) navigation.replace('Menu');
                else navigation.replace('Perfil', {urls});
            }, 3000);
        };
        Linking.getInitialURL()
        .then(async (url) => {
            var _lote = await Store.get('numeroLote');
            if (url.includes('cartoes') && _lote) {
                setTimeout(() => {
                    var cartoes = url.split('/cartoes,')[1].split(',');
                    var lote = cartoes[0];
                    var index = cartoes.indexOf(lote);
                    if (index > -1) cartoes.splice(index, 1);
                    navigation.reset({ index: 0, routes: [{ name: 'NFC', params: {
                        cartoes, lote: lote.replace(/[^0-9]/g, '')
                    }}]});
                }, 1000);
            } else if (url.includes('validacao-cancelamento')){
                try {
                    var cartao = url.split('/validacao-cancelamento,')[1].split('?')[0];
                    var gtv = url.split('/validacao-cancelamento,')[1].split('?')[1];
                    try {
                        navigation.current.reset({ index: 0, routes: [{ name: 'ValidarCancelamento', params: {
                            cartao, gtv
                        }}]});
                    } catch(e){
                        await Updates.reloadAsync();
                    }
                } catch(e){}
            } else if (url.includes('operador')){
                try {
					var operador = url.split('/operador,')[1].split('?')[0];
					var boleta = url.split('/operador,')[1].split('?')[1];
					await Store.set('operador-leitura', String(operador));
					try {
						navigationRef.current.reset({ index: 0, routes: [{ name: 'VincularOperador', params: {
							operador, boleta
						}}]});
					} catch(e){
						await Updates.reloadAsync();
					}
				} catch(e){}
            } else if (url.includes('cracha')){
				try {
					var cartao = url.split('/cracha,')[1].split('?')[0];
					var operador = url.split('/cracha,')[1].split('?')[1];
					await Store.set('operador-leitura', String(operador));
					try {
						navigationRef.current.reset({ index: 0, routes: [{ name: 'VincularCracha', params: {
							cartao, operador
						}}]});
					} catch(e){
						await Updates.reloadAsync();
					}
				} catch(e){}
			}
            else load();
        }).catch(load);
    }, []);
    useEffect(() => () => {}, []);

    return (
        <>
            <StatusBar style="light" backgroundColor="#293756" />
            <View style={{
                backgroundColor: '#293756',
                flex: 1,
                alignContent: 'center',
                alignItems: 'center',
                height: '100%'
            }}>
                <Image 
                    source={require("../../assets/images/athens_white.png")} 
                    resizeMode="contain"
                    style={{
                        width: 200,
                        height: '100%',
                        alignSelf: 'center',
                        resizeMode: 'contain'
                    }}
                />
            </View>
        </>
    );
};