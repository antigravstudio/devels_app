import React, { useEffect, useState } from 'react';
import { 
    TouchableWithoutFeedback, ScrollView, TouchableOpacity, Text, View, Keyboard
} from 'react-native';
import { Button } from '@ui-kitten/components';
import Spinner from 'react-native-loading-spinner-overlay';
import * as FeatherIcon from "react-native-feather";
import { Table, Row } from 'react-native-table-component';

import styles from '../utils/CustomStyles';
import Formater from '../utils/Formater';
import Store from '../utils/Store';

import api from '../service/ApiHandler';

import { Form } from './subcomponents/Form';
import { Modal as ModalCancelamento } from './subcomponents/Modal';

export const ConsultarGTVScreen = (props) => {    
    const [spinner, setSpinner] = useState(false);
    const [spinnerText, setSpinnerText] = useState('Aguarde...');
    const [modalCancelamento, setModalCancelamento] = useState(false);
    const [dados, setDados] = useState(null);
    const [pagina, setPagina] = useState(0);
    const [filtro, setFiltro] = useState('');
    const [dadosModal, setDadosModal] = useState({});

    const searchIcon = () => (<TouchableWithoutFeedback><FeatherIcon.Search width={24} height={24} color="#4E86FB" /></TouchableWithoutFeedback>);

    const widthArray = [150, 150, 100, 100];

    const pesquisar = async () => {
        setSpinner(true);
        setSpinnerText('Consultando...');
        const idDispositivo = await Store.get('idDispositivo');
        const tipo = await Store.get('perfil');
        var chave_busca = 'EncConsultar_cancelamentosBusca';
        switch(tipo){
            case 'Custódia':
            case 'CUSTODIA_CANCELAMENTO':
            case 'CUSTODIA_SUPORTE':
            case 'CUSTODIA_ABASTECIMENTO':
                chave_busca = 'CtdConsultar_cancelamentosBusca';
                break;
        }
        var filtro_inicial = 'EncConsultar_cancelamentosInicial';
        switch(tipo){
            case 'Custódia':
            case 'CUSTODIA_CANCELAMENTO':
            case 'CUSTODIA_SUPORTE':
            case 'CUSTODIA_ABASTECIMENTO':
                filtro_inicial = 'CtdConsultar_cancelamentosInicial';
                break;
        }
        console.log(idDispositivo)
        const response = await api.consulta({
            chave: filtro ? chave_busca : filtro_inicial,
            filtro: filtro ? filtro.toUpperCase() : null,
            idDispositivo
        });
        if(response) {
            setPagina(0);
            try {
                var paginas = [];
                var i_pagina = 0;
                var i = 0;
                for(var row of response){
                    row.dataCriacao = Formater.formatarData(row.dataCriacao);
                    if(paginas[i_pagina] === undefined) paginas[i_pagina] = [];
                    paginas[i_pagina].push(row);
                    i++;
                    if(i == 5){
                        i = 0;
                        i_pagina++;
                    }
                }
                setDados(paginas);
            } catch(e){
                setDados([]);
            }
            setSpinner(false);
        } else {
            setDados([]);
            setSpinner(false);
        }
    };

    const abrirCancelamento = async (id) => {
        setSpinner(true);
        setSpinnerText('Aguarde...');
        const idDispositivo = await Store.get('idDispositivo');
        const tipo = await Store.get('perfil');
        var chave = tipo == 'Custódia' ? 'CtdConsultar_cancelamentosSelecao' : 'EncConsultar_cancelamentosSelecao';
        const response = await api.consulta({
            chave,
            filtro: id,
            idDispositivo
        });
        if(!response || !response.length) {
            setDadosModal({ cancelamentoId: id, table: [] });
            setModalCancelamento(true);
            setSpinner(false);
        } else {
            setDadosModal({ cancelamentoId: id, table: response });
            setModalCancelamento(true);
            setSpinner(false);
        }
    };

    const changeFiltro = (text) => {
        setFiltro(text);
    };

    useEffect(() => {
        pesquisar();
    }, []);

    useEffect(() => () => {}, []);

    return (
        <>
            <ModalCancelamento
                show={modalCancelamento} 
                close={setModalCancelamento} 
                content={() => (
                    <>
                        <View style={{...styles.table, flexDirection: 'column', marginBottom: 15}}>
                            <Text style={{ fontWeight: 'bold', fontSize: 16 }}>ID do Cancelamento</Text>
                            <Text style={{ fontSize: 16 }}>{dadosModal.cancelamentoId}</Text>
                        </View>
                        {
                            (dadosModal.table?dadosModal.table:[]).length ?
                            <ScrollView 
                                bounces={false} 
                                showsVerticalScrollIndicator={false} 
                                showsHorizontalScrollIndicator={false} 
                                horizontal={true}
                                vertical={true}
                                style={{
                                    width: '100%',
                                    marginBottom: 5,
                                    borderRadius: 8
                                }}
                            >
                                <Table borderStyle={styles.tableBorderStyle} style={{ 
                                    width: '100%', backgroundColor: 'white' 
                                }}>
                                    <Row 
                                        data={[
                                            'CPF',
                                            'Data',
                                            'Nº Bilhete',
                                            'Tipo Bilhete',
                                            'Usuário'
                                        ]} 
                                        textStyle={styles.tableHeaderText}
                                        style={styles.tableHeader}
                                        widthArr={[120, 100, 120, 120, 150]}
                                    />
                                    {
                                        dadosModal.table.map((row, index) => (
                                            <Row
                                                key={index}
                                                data={[
                                                    row.cpf?row.cpf:'...',
                                                    row.datA_CANCELAMENTO?Formater.formatarData(row.datA_CANCELAMENTO):'...',
                                                    row.tipO_BILHETE?row.n_BILHETE:'...',
                                                    row.tipO_BILHETE?row.tipO_BILHETE:'...',
                                                    row.usuario?row.usuario:'...'
                                                ]}
                                                style={[styles.row, index%2 && {backgroundColor: '#EEF5FE'}]}
                                                textStyle={{padding: 10, textAlign: 'center'}}
                                                widthArr={[120, 100, 120, 120, 150]}
                                            />
                                        ))
                                    }
                                </Table>
                            </ScrollView> 
                            : 
                            <View style={{...styles.warningAlert, marginBottom: 30 }}>
                                <Text style={styles.alertText}>
                                    NENHUM DADO FOI LOCALIZADO
                                </Text>
                            </View>
                        }
                    </>
                )}
            />
            <Spinner
                visible={spinner}
                textContent={spinnerText}
                textStyle={{ color: '#fff'}}
                color="#fff"
                overlayColor="rgba(0, 0, 0, 0.85)"
                hidden={false}
                animation="fade"
            />
            <Form 
                {...props}
                title="Consultar Cancelamento"
                menu={true}
                inputs={[{
                    style: { marginBottom: 10 },
                    accessoryLeft: searchIcon,
                    placeholder: "BUSCA POR ID DO CANCELAMENTO",
                    textStyle: { textAlign: 'center' },
                    autoCorrect: false,
                    defautValue: filtro,
                    autoCapitalize: "none",
                    autoCorrect: false,
                    returnKeyType: 'done',
                    keyboardType: 'number-pad',
                    autoCapitalize: 'none',
                    onChangeText: changeFiltro,
                    onSubmitEditing: pesquisar
                }]}
                content={() => (<>
                    <Button
                        size="small" 
                        style={{ 
                            marginBottom: 10, width: 100, alignSelf: 'flex-end',
                        }}
                        onPress={() => { pesquisar(); Keyboard.dismiss(); }}
                    >Pesquisar</Button>
                    {
                        dados !== null ?
                        <>
                            {
                                dados.length ?
                                <>
                                    <ScrollView 
                                        bounces={false} 
                                        showsVerticalScrollIndicator={false} 
                                        showsHorizontalScrollIndicator={false} 
                                        horizontal={true}
                                        vertical={true}
                                        style={{
                                            width: '100%',
                                            marginBottom: 5
                                        }}
                                    >
                                        <Table borderStyle={styles.tableBorderStyle} style={{
                                            width: '100%'
                                        }}>
                                            <Row 
                                                data={[
                                                    'ID do Cancelamento',
                                                    'Unidade',
                                                    'Data',
                                                    'Visualizar'
                                                ]} 
                                                textStyle={styles.tableHeaderText}
                                                style={styles.tableHeader}
                                                widthArr={widthArray}
                                            />
                                            {
                                                dados[pagina].map((row, index) => (
                                                    <TouchableOpacity key={index} onPress={() => abrirCancelamento(row.cancelamentoId)}>
                                                        <Row
                                                            data={[
                                                                row.cancelamentoId,
                                                                row.posto,
                                                                row.data,
                                                                <FeatherIcon.Search 
                                                                    color="#4E86FB"
                                                                    width={18}
                                                                    height={18}
                                                                    style={{ alignSelf: 'center' }}
                                                                />
                                                            ]}
                                                            style={[styles.row, index%2 && {backgroundColor: '#EEF5FE'}]}
                                                            textStyle={{padding: 10, textAlign: 'center'}}
                                                            widthArr={widthArray}
                                                        />
                                                    </TouchableOpacity>
                                                ))
                                            }
                                        </Table>
                                    </ScrollView>
                                    <View style={styles.pagination}>
                                        {
                                            dados.map((row, index) => (
                                                <TouchableOpacity 
                                                    key={index}
                                                    style={{...styles.paginationDot, backgroundColor: pagina == index ? '#E7EAFF' : 'white'}}
                                                    onPress={() => setPagina(index)}
                                                >
                                                    <Text style={{fontSize: 18}}>{index + 1}</Text>
                                                </TouchableOpacity>
                                            ))
                                        }
                                    </View>
                                </> : <>
                                <View style={{...styles.warningAlert, marginBottom: 30 }}>
                                        <Text style={styles.alertText}>
                                            NENHUM DADO FOI LOCALIZADO
                                        </Text>
                                    </View>
                                </>
                            }
                        </> : <></>
                    }
                </>)}
            />
        </>
    );
};