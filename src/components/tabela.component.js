import React, { useEffect } from 'react';
import { 
    TouchableOpacity, ScrollView
} from 'react-native';
import { Table, Row } from 'react-native-table-component';

import styles from '../utils/CustomStyles';

import { Form } from './subcomponents/Form';

export const TabelaScreen = (props) => {   
    useEffect(() => () => {}, []);

    return (
        <>
            <Form 
                {...props}
                title={false}
                menu={true}
                content={() => (<>
                    <ScrollView 
                        bounces={false} 
                        showsVerticalScrollIndicator={false} 
                        showsHorizontalScrollIndicator={false} 
                        horizontal={true}
                        vertical={true}
                        style={{
                            width: '100%',
                            marginBottom: 10
                        }}
                    >
                        <Table borderStyle={styles.tableBorderStyle} style={{
                            width: '100%'
                        }}>
                            <Row 
                                data={props.route.params.config.header} 
                                textStyle={styles.tableHeaderText}
                                style={styles.tableHeader}
                                widthArr={props.route.params.config.widthArray}
                            />
                            {
                                props.route.params.config.rows.map((row, index) => (
                                    <TouchableOpacity key={index}>
                                        <Row
                                            data={props.route.params.config.formater(row)}
                                            style={[styles.row, index%2 && {backgroundColor: '#EEF5FE'}]}
                                            textStyle={{padding: 10, textAlign: 'center'}}
                                            widthArr={props.route.params.config.widthArray}
                                        />
                                    </TouchableOpacity>
                                ))
                            }
                        </Table>
                    </ScrollView>
                </>)}
            />
        </>
    );
};