import React, { useEffect, useState } from 'react';
import { View, Text } from 'react-native';
import Spinner from 'react-native-loading-spinner-overlay';
import * as FeatherIcon from "react-native-feather";

import api from '../service/ApiHandler';

import styles from '../utils/CustomStyles';
import Store from '../utils/Store';
import Formater from '../utils/Formater';

import { Form } from './subcomponents/Form';

export const EstatisticasAbastacimentoScreen = (props) => {    
    const [spinner, setSpinner] = useState(true);
    const [spinnerText, setSpinnerText] = useState('Obtendo dados...');
    const [cards, setCards] = useState([]);
    const [atualizacao, setAtualizacao] = useState('...');

    useEffect(() => {
        async function obterDados(){
            setSpinnerText('Obtendo dados...');
            setSpinner(true);
            const idDispositivo = await Store.get('idDispositivo');
            const tipo = await Store.get('perfil');
            var chave = 'EncStats_posto';
            switch(tipo){
                case 'Custódia':
                case 'CUSTODIA_CANCELAMENTO':
                case 'CUSTODIA_SUPORTE':
                case 'CUSTODIA_ABASTECIMENTO':
                    chave = 'CtdStats_Abast';
                    break;
            }
            const response1 = await api.consulta({
                chave,
                idDispositivo
            });
            if(response1) {
                var _cards = [];
                var i_card = 0;
                var i = 0;
                for(var row of response1){
                    if(_cards[i_card] === undefined) _cards[i_card] = [];
                    _cards[i_card].push(row);
                    i++;
                    if(i == 2){
                        i = 0;
                        i_card++;
                    }
                }
                setCards(_cards);
                setSpinner(false);
            } else setCards([]);

            setSpinner(false);
            setAtualizacao(Formater.formatarDataHora(new Date()));
        }
        obterDados();
    }, []);

    const fundoCard = (i1, i2) => {
        if(i1 % 2 == 0 && i2 == 0) return '#FF9500';
        if(i1 % 2 == 0 && i2 == 1) return '#4CE4B1';
        if(Math.abs(i1 % 2) == 1 && i2 == 0) return '#FF2D55';
        if(Math.abs(i1 % 2) == 1 && i2 == 1) return '#219AFF';
        return '#FF9500';
    };

    useEffect(() => () => {}, []);

    return (
        <>
            <Spinner
                visible={spinner}
                textContent={spinnerText}
                textStyle={{ color: '#fff'}}
                color="#fff"
                overlayColor="rgba(0, 0, 0, 0.85)"
                hidden={false}
                animation="fade"
            />
            <Form 
                {...props}
                title="Estatísticas do Abastecimento"
                menu={true}
                content={() => (<>
                    {
                        cards.map((row, index) => (
                            <View 
                                key={index}
                                style={{ 
                                    width: '100%',
                                    flexDirection: 'row',
                                    marginBottom: 10,
                                    alignItems: 'center',
                                    justifyContent: 'center'
                                }}
                            >
                                {
                                    row.map((_row, _index) => (
                                        <View 
                                            key={_index}
                                            style={{
                                                ...styles.dashCard,
                                                backgroundColor: fundoCard(index, _index),
                                                marginHorizontal: '2%'
                                            }}
                                        >
                                            <FeatherIcon.Monitor style={styles.dashIcon} />
                                            <Text style={styles.dashTitle}>
                                                {
                                                    (_row.tipo?_row.tipo:'')
                                                        .toUpperCase()
                                                        .replace('POSTO', 'P.')
                                                        .replace('TERMINAL', 'TERM.')
                                                }
                                                 {
                                                    (_row.posto?_row.posto:'')
                                                        .toUpperCase()
                                                        .replace('POSTO', 'P.')
                                                        .replace('TERMINAL', 'TERM.')
                                                }
                                            </Text>
                                            <Text style={styles.dashSubtitle}>
                                                {_row.saldo}
                                            </Text>
                                        </View>
                                    ))
                                }
                            </View>
                        ))
                    }
                    <View style={{ 
                        width: '100%',
                        flexDirection: 'column',
                        alignItems: 'center',
                        justifyContent: 'center',
                        marginBottom: 10,
                    }}>
                        <View style={styles.simpleCard}>
                            <Text style={styles.simpleCardTitle}>
                                Última Atualização
                            </Text>
                            <Text style={styles.simpleCardSubtitle}>
                                {atualizacao}
                            </Text>
                        </View>
                    </View>
                </>)}
            />
        </>
    );
};