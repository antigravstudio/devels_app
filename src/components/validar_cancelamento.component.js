import React, { useEffect, useState } from 'react';
import { 
    View, Text, Dimensions, Image, TouchableOpacity, TouchableWithoutFeedback, ScrollView, Platform
} from 'react-native';
import Spinner from 'react-native-loading-spinner-overlay';
import { BarCodeScanner } from 'expo-barcode-scanner';
import { Camera } from 'expo-camera';
import { Table, Row } from 'react-native-table-component';
import * as FeatherIcon from "react-native-feather";
import { Button } from '@ui-kitten/components';
import * as Linking from 'expo-linking';

import { Form } from './subcomponents/Form';
import Store from '../utils/Store';
import Alert from '../utils/Alert';
import api from '../service/ApiHandler';
import { Modal as ModalBilhete } from './subcomponents/Modal';

import styles from '../utils/CustomStyles';

export const ValidarCancelamentoScreen = (props) => {    
    const [spinner, setSpinner] = useState(true);
    const [spinnerText, setSpinnerText] = useState('Aguarde...');
    const [hasPermission, setHasPermission] = useState(null);
    const [scanned, setScanned] = useState(false);
    const [zoom, setZoom] = useState(0);
    const [flash, setFlash] = useState(null);
    const [numeroGTV, setNumeroGTV] = useState('');
    const [dataGTV, setDataGTV] = useState('');
    const [unidade, setUnidade] = useState('');
    const [bilhetes, setBilhetes] = useState([]);
    const [modalBilhete, setModalBilhete] = useState(false);
    const [bilhete, setBilhete] = useState({});
    const [leituras, setLeituras] = useState([]);
    const [faltantes, setFaltantes] = useState([]);
    const [ocorrencias, setOcorrencias] = useState([]);
    const [expandMotivo, setExpandMotivo] = useState(false);
    const [motivoLabel, setMotivoLabel] = useState('...');
    const [motivos, setMotivos] = useState([]);

    const changeFlash = () => {
        if(flash == Camera.Constants.FlashMode.off) 
            setFlash(Camera.Constants.FlashMode.on);
        else
            setFlash(Camera.Constants.FlashMode.off);
    };

    const changeZoom = (mode) => {
        var _zoom = zoom * 100;
        if(mode == 'out'){
            if(_zoom === 0) return _zoom = 0;
            else _zoom = (_zoom - 10) / 100;
        }
        if(mode == 'in'){
            if(_zoom === 100) _zoom = 1;
            else _zoom = (_zoom + 10) / 100;
        }
        setZoom(_zoom);
    };

    const handleQRCode = async (resultado) => {
        if(scanned) return false;
        await changeNumeroGTV(resultado.data);
    };

    const changeNumeroGTV = async (text) => {
        setSpinnerText('Validando dados...');
        setSpinner(true);
        const idDispositivo = await Store.get('idDispositivo');
        const tipo = await Store.get('perfil');
        var chave = 'EncConsultar_cancelamentosBusca';
        switch(tipo){
            case 'Custódia':
            case 'CUSTODIA_CANCELAMENTO':
            case 'CUSTODIA_SUPORTE':
            case 'CUSTODIA_ABASTECIMENTO':
                chave = 'CtdConsultar_cancelamentosBusca';
                break;
        }
        const response = await api.consulta({
            chave,
            filtro: text,
            idDispositivo
        });
        if(response){
            if(response.length){
                var chave2 = 'EncConsultar_cancelamentosSelecao';
                switch(tipo){
                    case 'Custódia':
                    case 'CUSTODIA_CANCELAMENTO':
                    case 'CUSTODIA_SUPORTE':
                    case 'CUSTODIA_ABASTECIMENTO':
                        chave = 'CtdConsultar_cancelamentosSelecao';
                        break;
                }
                const response2 = await api.consulta({
                    chave: chave2,
                    filtro: text,
                    idDispositivo
                });
                if(response2){
                    if(response2 !== false){
                        setScanned(true);
                        setNumeroGTV(String(response[0].cancelamentoId));
                        setDataGTV(String(response[0].data));
                        setUnidade(String(response[0].posto));
                        setBilhetes(response2);
                        setSpinner(false);
                    } else {
                        alert('Ok');
                        Alert.simple({ 
                            subtitle: 'Ocorreu um erro',
                            accept: () => {
                                setSpinner(false);
                                setScanned(false);
                                props.navigation.goBack();
                            }
                        });
                    }
                } else {
                    Alert.simple({ 
                        subtitle: 'Nenhum bilhete foi localizado nessa GTV',
                        accept: () => {
                            setSpinner(false);
                            setScanned(false);
                            props.navigation.goBack();
                        }
                    });
                }
            } else {
                Alert.simple({ 
                    subtitle: 'GTV não localizada',
                    accept: () => {
                        setSpinner(false);
                        setScanned(false);
                        props.navigation.goBack();
                    }
                });
            }
        } else {
            Alert.simple({ 
                subtitle: 'GTV não localizada',
                accept: () => {
                    setSpinner(false);
                    setScanned(false);
                    props.navigation.goBack();
                }
            });
        }
        setSpinner(false);
    };

    const abrirBilhete = async (row) => {
        setBilhete(row);
        var a = await Store.get('faltante-' + String(row.n_BILHETE));
        if(a){
            a = JSON.parse(a);
            if(a){
                setMotivoLabel(a.descricao);
            }
        }
        setModalBilhete(true);
    };

    const changeLeitura = (text, action, loader = true) => {
        var _leituras = leituras;
        var a = _leituras.indexOf(Number(text));
        if(action == 'add') {
            if(a === -1) _leituras.push(Number(text));
        } else {
            if(a > -1) _leituras.splice(a, 1);
        }
        setLeituras(_leituras);
        if(loader) {
            setModalBilhete(false);
            setSpinnerText('');
            setSpinner(true);
            setTimeout(() => {
                setSpinner(false);
                setModalBilhete(true);
            }, 500);
        }
    };

    const changeFaltantes = async (text, action, loader = true, _motivo = '') => {
        var _faltantes = faltantes;
        var a = _faltantes.indexOf(Number(text));
        if(action == 'add') {
            if(a === -1){
                _faltantes.push(Number(text));
                await Store.set('faltante-' + String(text), JSON.stringify(_motivo));
            }
        } else {
            if(a > -1) _faltantes.splice(a, 1);
            await Store.remove('faltante-' + String(text));
        }
        setFaltantes(_faltantes);
    };

    const changeOcorrencia = (text, action, loader = true) => {
        var _ocorrencias = ocorrencias;
        var a = _ocorrencias.indexOf(Number(text));
        if(action == 'add') {
            if(a === -1) _ocorrencias.push(Number(text));
        } else {
            if(a > -1) _ocorrencias.splice(a, 1);
        }
        setOcorrencias(_ocorrencias);
        if(loader) {
            setModalBilhete(false);
            setSpinnerText('');
            setSpinner(true);
            setTimeout(() => {
                setSpinner(false);
                setModalBilhete(true);
            }, 500);
        }
    };

    const salvarBilhete = async () => {
        Alert.confirm({
            title: 'Atenção', 
            subtitle: 'Você tem certeza?',
            accept: async () => {
                const limit = 1000;
                const dados = {
                    numeroGTV,
                    dataGTV,
                    unidade,
                    leituras,
                    faltantes,
                    ocorrencias,
                    bilhetes
                };
                var _dados = JSON.stringify(dados);
                _dados = _dados.replace(/(\r\n|\n|\r)/gm, "");
                const byte = Math.ceil(_dados.length / limit);
                var start = 0, end = 0;
				for (var i = 0; i < byte; i++) {
					end += limit;
					var bloco = _dados.substring(start, end);
					await Store.set(`gtv-${numeroGTV}-${i}`, bloco);
					start += limit;
				}
                await Store.set('validar-gtv-' + String(numeroGTV), String(byte));
                setModalBilhete(false);
                setExpandMotivo(false);
                setMotivoLabel('...');
            }
        });
    };

    const liberarFragmentacao = () => {
        Alert.confirm({
            title: 'Atenção', 
            subtitle: 'Você tem certeza?',
            accept: () => {
                props.navigation.navigate('BarCode');
                let interval = setInterval(async () => {
                    var leitura_fracionamento = await Store.get('leitura-boleta');
                    if(leitura_fracionamento){
                        clearInterval(interval);
                        await Store.remove('leitura-boleta');
                        finalizarLiberacao(leitura_fracionamento);
                    }
                }, 500);
            }
        });
    };

    const finalizarLiberacao = async (idFragmentacao) => {
        var salvos = 0;
        setSpinner(true);
        setSpinnerText('Liberando...');
        const idDispositivo = await Store.get('idDispositivo');
        for(const idCancelamentoItem of leituras){
            var updated = await api.bilheteSemLeitura({ idDispositivo, idCancelamentoItem  })
            if(updated) salvos += 1;
        }
        for(const idCancelamentoItem of faltantes){
            var idMotivo = await Store.get('faltante-' + String(idCancelamentoItem));
            if(idMotivo){
                idMotivo = JSON.parse(idMotivo);
                idMotivo = idMotivo.motivoId;
                var updated = await api.bilheteFaltante({ idDispositivo, idCancelamentoItem, idMotivo })
                if(updated){
                    salvos += 1;
                    await Store.remove('faltante-' + String(idCancelamentoItem));
                }
            }
        }
        for(const idCancelamentoItem of ocorrencias){
            var updated = await api.bilheteOcorrenciaErrada({ idDispositivo, idCancelamentoItem  })
            if(updated) salvos += 1;
        }
        for(const bilhete of bilhetes){
            if(!bilhete.datA_CANCELAMENTO){
                var updated = await api.adicionaBilheteCancelamento({ 
                    idDispositivo, codigoBilhete: bilhete.n_BILHETE, idCancelamento: numeroGTV  
                });
                if(updated){
                    salvos += 1; 
                    await Store.remove(`validar-gtv-${String(g)}`);
                }
            } else {
                var bytes = await Store.get(`validar-gtv-${bilhete.n_BILHETE}`);
                if (bytes) {
                    bytes = Number(bytes);
                    for (var r = 0; r < bytes; r++) await Store.remove(`gtv-${g}-${r}`);
                }
                await Store.remove(`validar-gtv-${bilhete.n_BILHETE}`);
            }
        }
        var liberado = await api.liberarGTVparaAPF({ idDispositivo, idCancelamento, idFragmentacao });
        if(liberado) salvos += 1; 
        if(salvos){
            Alert.simple({ 
                subtitle: 'Processo encerrado com sucesso',
                accept: async () => {
                    await Store.remove(`validar-gtv-${String(numeroGTV)}`);
                    setSpinner(false);
                    props.navigation.reset({ index: 0, routes: [{ name: 'Menu' }]});
                }
            });
        } else {
            Alert.simple({ 
                title: 'Ocorreu um erro',
                subtitle: 'Nenhum item foi atualizado',
                accept: () => {
                    setSpinner(false);
                }
            });
        }
    };

    const adicionarBilhete = async () => {
        setSpinnerText('Iniciando leitura');
        setSpinner(true);
        try {
            const limit = 1000;
            const dados = {
                numeroGTV,
                dataGTV,
                unidade,
                leituras,
                faltantes,
                ocorrencias,
                bilhetes
            };
            var _dados = JSON.stringify(dados);
            _dados = _dados.replace(/(\r\n|\n|\r)/gm, "");
            const byte = Math.ceil(_dados.length / limit);
            var start = 0, end = 0;
            for (var i = 0; i < byte; i++) {
                end += limit;
                var bloco = _dados.substring(start, end);
                await Store.set(`gtv-${numeroGTV}-${i}`, bloco);
                start += limit;
            }
            await Store.set(`validar-gtv-${String(numeroGTV)}`, String(byte));
            await Linking.openURL(`devels://nfc/leitura_bilhete/?${numeroGTV}`);
            setSpinner(false); 
        } catch(e){
            setSpinner(false);
            Alert.confirm({
                title: 'A utilidade de leitura de cartões ainda não foi instalada', 
                subtitle: 'Deseja instalar o complemento agora?',
                accept: async () => {
                    await Linking.openURL(`http://projects.samuelcarrara.com/devels/devels-nfc-1.0.1.apk`);
                    setSpinner(false);
                    props.navigation.reset({ index: 0, routes: [{ name: 'Menu' }]});
                }, 
                cancel: async () => {
                    setSpinner(false);
                    props.navigation.reset({ index: 0, routes: [{ name: 'Menu' }]});
                }
            });
            return false;
        }
    };

    useEffect(() => {
        (async () => {
            setSpinner(true);
            var validar_nfc = true;
            try {
                if(props.route.params.cartao && props.route.params.gtv) validar_nfc = false;
                else validar_nfc = true;
            } catch (e){ validar_nfc = true; }
            if(Platform.OS !== 'ios' && validar_nfc){
                try {
                    await Linking.openURL(`devels://nfc/verificar_versao/1.0.1?http://projects.samuelcarrara.com/devels/devels-nfc-1.0.1.apk`);
                } catch(e){
                    Alert.confirm({
                        title: 'A utilidade de leitura de cartões ainda não foi instalada', 
                        subtitle: 'Deseja instalar o complemento agora?',
                        accept: async () => {
                            await Linking.openURL(`http://projects.samuelcarrara.com/devels/devels-nfc-1.0.1.apk`);
                            setSpinner(false);
                            props.navigation.reset({ index: 0, routes: [{ name: 'Menu' }]});
                        }, 
                        cancel: async () => {
                            setSpinner(false);
                            props.navigation.reset({ index: 0, routes: [{ name: 'Menu' }]});
                        }
                    });
                    return false;
                }
            }
            changeFlash(Camera.Constants.FlashMode.off)
            setZoom(0);
            const { status } = await BarCodeScanner.requestPermissionsAsync();
            try {
                const idDispositivo = await Store.get('idDispositivo');
                const response = await api.oterMotivos({ idDispositivo });
                if(response){
                    if(response.length) setMotivos(response);
                    else setMotivos([]);
                } else setMotivos([]);
            } catch(e){ setMotivos([]); }
            setHasPermission(status === 'granted');
            setScanned(false);
            if(!validar_nfc){
                var c = props.route.params.cartao;
                var g = props.route.params.gtv;
                var bytes = await Store.get(`validar-gtv-${g}`);
                if (bytes) {
                    bytes = Number(bytes);
                    var blocos = '';
                    for (var r = 0; r < bytes; r++) {
                        var bloco = await Store.get(`gtv-${g}-${r}`);
                        blocos += bloco;
                    }
                    blocos = JSON.parse(blocos);
                    if (blocos) {
                        setNumeroGTV(String(g));
                        setDataGTV(blocos.dataGTV);
                        setUnidade(blocos.unidade);
                        setLeituras(blocos.leituras);
                        setFaltantes(blocos.faltantes);
                        setOcorrencias(blocos.ocorrencias);
                        var _bilhetes = blocos.bilhetes;
                        var row = {
                            cancelamentoId: Number(g),
                            cpf: '---',
                            datA_CANCELAMENTO: '---',
                            liberado: '---',
                            motivo: '---',
                            n_BILHETE: Number(c),
                            nomE_OPERADOR: '---',
                            tipO_BILHETE: '---',
                            tipO_OCORRENCIA: '---',
                            usuario: '---',
                        };
                        setBilhetes([..._bilhetes, row]);
                        const limit = 1000;
                        const dados = {
                            numeroGTV: String(g),
                            dataGTV: blocos.dataGTV,
                            unidade: blocos.leituras,
                            leituras: blocos.leituras,
                            faltantes: blocos.faltantes,
                            ocorrencias: blocos.ocorrencias,
                            bilhetes: [..._bilhetes, row]
                        };
                        var _dados = JSON.stringify(dados);
                        _dados = _dados.replace(/(\r\n|\n|\r)/gm, "");
                        const byte = Math.ceil(_dados.length / limit);
                        var start = 0, end = 0;
                        for (var i = 0; i < byte; i++) {
                            end += limit;
                            var bloco = _dados.substring(start, end);
                            await Store.set(`gtv-${g}-${i}`, bloco);
                            start += limit;
                        }
                        await Store.set(`validar-gtv-${String(g)}`, String(byte));
                        abrirBilhete(row);
                        setScanned(true);
                    }
                }
            }
            //setTimeout(async () => await changeNumeroGTV(5299), 0);
            setSpinner(false);
        })();
    }, []);

    useEffect(() => () => {}, []);

    return (
        <>
            <Spinner
                visible={spinner}
                textContent={spinnerText}
                textStyle={{ color: '#fff'}}
                color="#fff"
                overlayColor="rgba(0, 0, 0, 0.85)"
                hidden={false}
                animation="fade"
            />
            <ModalBilhete
                show={modalBilhete} 
                close={setModalBilhete} 
                closable={false}
                content={() => (
                    <>
                        {
                            bilhete.tipO_BILHETE != '---' ?
                            <View style={{...styles.table, flexDirection: 'column', marginBottom: 10}}>
                                <Text style={{ fontWeight: 'bold', fontSize: 16 }}>Tipo Cartão</Text>
                                <Text style={{ fontSize: 16 }}>{bilhete.tipO_BILHETE}</Text>
                            </View> : <></>
                        }
                        {
                            bilhete.n_BILHETE != '---' ?
                            <View style={{...styles.table, flexDirection: 'column', marginBottom: 10}}>
                                <Text style={{ fontWeight: 'bold', fontSize: 16 }}>N° do Cartão</Text>
                                <Text style={{ fontSize: 16 }}>{bilhete.n_BILHETE}</Text>
                            </View>: <></>
                        }
                        {
                            bilhete.usuario != '---' ?
                            <View style={{...styles.table, flexDirection: 'column', marginBottom: 10}}>
                                <Text style={{ fontWeight: 'bold', fontSize: 16 }}>Cliente</Text>
                                <Text style={{ fontSize: 16 }}>{bilhete.usuario}</Text>
                            </View>: <></>
                        }
                        {/*
                            {
                                bilhete.nomE_OPERADOR != '---' ?
                                <View style={{...styles.table, flexDirection: 'column', marginBottom: 10}}>
                                    <Text style={{ fontWeight: 'bold', fontSize: 16 }}>Operador</Text>
                                    <Text style={{ fontSize: 16 }}>{bilhete.nomE_OPERADOR}</Text>
                                </View>: <></>
                            }
                            {
                                bilhete.tipO_OCORRENCIA != '---' ?
                                <View style={{...styles.table, flexDirection: 'column', marginBottom: 10}}>
                                    <Text style={{ fontWeight: 'bold', fontSize: 16 }}>Tipo Ocorrência</Text>
                                    <Text style={{ fontSize: 16 }}>{bilhete.tipO_OCORRENCIA}</Text>
                                </View>: <></>
                            }
                            {
                                bilhete.motivo != '---' ?
                                <View style={{...styles.table, flexDirection: 'column', marginBottom: 10}}>
                                    <Text style={{ fontWeight: 'bold', fontSize: 16 }}>Motivo</Text>
                                    <Text style={{ fontSize: 16 }}>{bilhete.motivo}</Text>
                                </View>: <></>
                            }
                        */
                        }
                        <View style={{...styles.table, flexDirection: 'row', marginBottom: 10, alignItems: 'center'}}>
                            <Text style={{ fontWeight: 'bold', fontSize: 16 }}>Leitura?</Text>
                            <View style={{
                                flexDirection: 'row',
                                position: 'absolute',
                                right: 10
                            }}>
                                <Button 
                                    status={leituras.indexOf(bilhete.n_BILHETE) > -1?'info':'basic'}
                                    size="small" 
                                    style={{
                                        width: 80, marginRight: 10
                                    }}
                                    appearance={leituras.indexOf(bilhete.n_BILHETE) > -1?'filled':'outline'}
                                    onPress={() => changeLeitura(bilhete.n_BILHETE, 'add')}
                                >Sim</Button>
                                <Button 
                                    status={leituras.indexOf(bilhete.n_BILHETE) > -1?'basic':'info'}
                                    size="small" 
                                    style={{
                                        width: 80
                                    }}
                                    appearance={leituras.indexOf(bilhete.n_BILHETE) > -1?'outline':'filled'}
                                    onPress={() => changeLeitura(bilhete.n_BILHETE, 'remove')}
                                >Não</Button>
                            </View>
                        </View>
                        <View style={{...styles.table, flexDirection: 'row', marginBottom: 10, alignItems: 'center'}}>
                            <Text style={{ fontWeight: 'bold', fontSize: 16 }}>Ocor Correta?</Text>
                            <View style={{
                                flexDirection: 'row',
                                position: 'absolute',
                                right: 10
                            }}>
                                <Button 
                                    status={ocorrencias.indexOf(bilhete.n_BILHETE) > -1?'info':'basic'}
                                    size="small" 
                                    style={{
                                        width: 80, marginRight: 10
                                    }}
                                    appearance={ocorrencias.indexOf(bilhete.n_BILHETE) > -1?'filled':'outline'}
                                    onPress={() => changeOcorrencia(bilhete.n_BILHETE, 'add')}
                                >Sim</Button>
                                <Button 
                                    status={ocorrencias.indexOf(bilhete.n_BILHETE) > -1?'basic':'info'}
                                    size="small" 
                                    style={{
                                        width: 80
                                    }}
                                    appearance={ocorrencias.indexOf(bilhete.n_BILHETE) > -1?'outline':'filled'}
                                    onPress={() => changeOcorrencia(bilhete.n_BILHETE, 'remove')}
                                >Não</Button>
                            </View>
                        </View>
                        <View style={{...styles.table, flexDirection: 'column', marginBottom: 10, alignItems: 'flex-start'}}>
                            <Text style={{ fontWeight: 'bold', fontSize: 16, marginBottom: 5 }}>Faltante</Text>
                            <TouchableOpacity 
                                style={{
                                    ...styles.customCombo,
                                    borderBottomLeftRadius: expandMotivo ? 0 : 6,
                                    borderBottomRightRadius: expandMotivo ? 0 : 6,
                                    marginBottom: 0,
                                }}
                                onPress={() => setExpandMotivo(!expandMotivo)}
                            >
                                <View style={{ height: 16, width: 16 }}></View>
                                <Text
                                    style={{
                                        position: 'absolute',
                                        left: 10,
                                        top: 10
                                    }}
                                >{motivoLabel}</Text>
                                {
                                    expandMotivo ?
                                    <FeatherIcon.ChevronUp 
                                        width={20} 
                                        height={20} 
                                        color={'#000'} 
                                        style={{ position: 'absolute', right: 5, top: 8 }}
                                    />
                                    :
                                    <FeatherIcon.ChevronDown 
                                        width={20} 
                                        height={20} 
                                        color={'#000'} 
                                        style={{ position: 'absolute', right: 5, top: 8 }}
                                    />
                                }
                            </TouchableOpacity>
                            {
                                expandMotivo ?
                                <ScrollView 
                                    bounces={false}
                                    style={{ 
                                        marginBottom: 15, 
                                        width: '100%',
                                    }}
                                >
                                    <TouchableOpacity 
                                        style={styles.customComboOption}
                                        onPress={() => {
                                            setMotivoLabel('...');
                                            setExpandMotivo(false);
                                            changeFaltantes(bilhete.n_BILHETE, 'remove');
                                        }}
                                    >
                                        <Text>...</Text>
                                    </TouchableOpacity>
                                    {
                                        motivos.map((row, index) => (
                                            <TouchableOpacity 
                                                key={index} 
                                                style={styles.customComboOption}
                                                onPress={() => {
                                                    setMotivoLabel(row.descricao);
                                                    setExpandMotivo(false);
                                                    changeFaltantes(bilhete.n_BILHETE, 'add', true, row);
                                                }}
                                            >
                                                <Text>{row.descricao}</Text>
                                            </TouchableOpacity>
                                        ))
                                    }
                                </ScrollView> : <></>
                            }
                        </View>
                        <View style={{ width: '100%', alignItems: 'center', flexDirection: 'row', marginTop: 20}}>
                            <Button
                                status="success"
                                size="small"
                                style={{
                                    alignItems: 'center',
                                    width: 100,
                                    position: 'absolute',
                                    right: 0
                                }}
                                onPress={() => salvarBilhete(bilhete.n_BILHETE)}
                            >Salvar</Button>
                        </View>
                    </>
                )}
            />
            <Form 
                {...props}
                title="Validar Cancelamento"
                menu={false}
                bigContent={hasPermission && !numeroGTV}
                inputs={numeroGTV ? [
                {
                    accessoryLeft: () => 
                    (<TouchableWithoutFeedback>
                            <View style={{ minWidth: 110, backgroundColor: '#293756', paddingHorizontal: 10, paddingTop: 6, paddingBottom: 7, marginLeft: -9, borderTopLeftRadius: 4, borderBottomLeftRadius: 4, marginTop: -6, marginBottom: -6 }}>
                                <Text style={{ fontWeight: 'bold', color: '#fff', textAlign: 'center' }}>Número GTV</Text>
                            </View>
                        </TouchableWithoutFeedback>),
                    style: { marginBottom: 10, alignSelf: 'center' },
                    textStyle: { textAlign: 'center', color: '#000', fontSize: 16 },
                    value: numeroGTV,
                    disabled: true,
                },
                {
                    accessoryLeft: () => 
                    (<TouchableWithoutFeedback>
                            <View style={{ minWidth: 110, backgroundColor: '#293756', paddingHorizontal: 10, paddingTop: 6, paddingBottom: 7, marginLeft: -9, borderTopLeftRadius: 4, borderBottomLeftRadius: 4, marginTop: -6, marginBottom: -6 }}>
                                <Text style={{ fontWeight: 'bold', color: '#fff', textAlign: 'center'}}>Data</Text>
                            </View>
                        </TouchableWithoutFeedback>),
                    style: { marginBottom: 10, alignSelf: 'center' },
                    textStyle: { textAlign: 'center', color: '#000', fontSize: 16 },
                    value: dataGTV,
                    disabled: true,
                },
                {
                    accessoryLeft: () => 
                    (<TouchableWithoutFeedback>
                            <View style={{ minWidth: 110, backgroundColor: '#293756', paddingHorizontal: 10, paddingVertical: 8, marginLeft: -9, borderTopLeftRadius: 4, borderBottomLeftRadius: 4, marginTop: -6, marginBottom: -6 }}>
                                <Text style={{ fontWeight: 'bold', color: '#fff', textAlign: 'center'}}>Unidade</Text>
                            </View>
                        </TouchableWithoutFeedback>),
                    style: { marginBottom: 10, alignSelf: 'center' },
                    textStyle: { textAlign: 'center', color: '#000', fontSize: 16 },
                    value: unidade,
                    disabled: true,
                },
                ] : []}
                content={() => (<>
                    {
                        hasPermission ?
                        <>
                            {
                                numeroGTV ?
                                <>
                                    {
                                        bilhetes.length ?
                                        <ScrollView 
                                            bounces={false} 
                                            showsVerticalScrollIndicator={false} 
                                            showsHorizontalScrollIndicator={false} 
                                            horizontal={true}
                                            vertical={true}
                                            style={{
                                                width: '100%',
                                                marginBottom: 5,
                                                borderRadius: 6
                                            }}
                                        >
                                            <Table 
                                                borderStyle={styles.tableBorderStyle} 
                                                style={{ 
                                                    width: '100%', 
                                                    backgroundColor: 'white',
                                                    marginBottom: 15
                                                }}
                                            >
                                                <Row 
                                                    data={[
                                                        'Tipo Cartão',
                                                        'N° Cartão',
                                                        'Cliente',
                                                        'Operador',
                                                        'Tipo Ocorrência',
                                                        'Motivo',
                                                        'Leitura?',
                                                        'Ocor Correta?',
                                                        'Faltante?',
                                                    ]} 
                                                    textStyle={styles.tableHeaderText}
                                                    style={styles.tableHeader}
                                                    widthArr={[180, 100, 180, 120, 150, 100, 90, 90, 100]}
                                                />
                                                {
                                                    bilhetes.map((row, index) => (
                                                        <TouchableOpacity
                                                            key={index}
                                                            onPress={() => abrirBilhete(row)}
                                                        >
                                                            <Row
                                                                data={[
                                                                    row.tipO_BILHETE?row.tipO_BILHETE:'...',
                                                                    row.tipO_BILHETE?row.n_BILHETE:'...',
                                                                    row.usuario?row.usuario:'...',
                                                                    row.nomE_OPERADOR?row.nomE_OPERADOR:'...',
                                                                    row.tipO_OCORRENCIA?row.tipO_OCORRENCIA:'...',
                                                                    row.motivo?row.motivo:'...',
                                                                    <View style={{
                                                                        alignSelf: 'center',
                                                                        alignItems: 'center',
                                                                        alignContent: 'center'
                                                                    }}>
                                                                        {
                                                                            leituras.indexOf(row.n_BILHETE) > -1 ?
                                                                            <FeatherIcon.AlertCircle 
                                                                                color="#e67e22"
                                                                                width={18}
                                                                                height={18}
                                                                            />
                                                                            :
                                                                            <FeatherIcon.X 
                                                                                color="#7f8c8d"
                                                                                width={18}
                                                                                height={18}
                                                                            />
                                                                        }
                                                                    </View>,
                                                                    <View style={{
                                                                        alignSelf: 'center',
                                                                        alignItems: 'center',
                                                                        alignContent: 'center'
                                                                    }}>
                                                                        {
                                                                        ocorrencias.indexOf(row.n_BILHETE) > -1 ?
                                                                            <FeatherIcon.AlertCircle 
                                                                                color="#e67e22"
                                                                                width={18}
                                                                                height={18}
                                                                            />
                                                                            :
                                                                            <FeatherIcon.X 
                                                                                color="#7f8c8d"
                                                                                width={18}
                                                                                height={18}
                                                                            />
                                                                        }
                                                                    </View>,
                                                                    <View style={{
                                                                        alignSelf: 'center',
                                                                        alignItems: 'center',
                                                                        alignContent: 'center'
                                                                    }}>
                                                                        {
                                                                            faltantes.indexOf(row.n_BILHETE) > -1 ?
                                                                            <FeatherIcon.AlertCircle 
                                                                                color="#e67e22"
                                                                                width={18}
                                                                                height={18}
                                                                            />
                                                                            :
                                                                            <Text>---</Text>
                                                                        }
                                                                    </View>,
                                                                ]}
                                                                style={[styles.row, index%2 && {backgroundColor: '#EEF5FE'}]}
                                                                textStyle={{padding: 10, textAlign: 'center'}}
                                                                widthArr={[180, 100, 180, 120, 150, 100, 90, 90, 100]}
                                                            />
                                                        </TouchableOpacity>
                                                    ))
                                                }
                                            </Table>
                                        </ScrollView>
                                        :
                                        <View style={{...styles.warningAlert, marginBottom: 30 }}>
                                            <Text style={styles.alertText}>
                                                NENHUM BILHETE FOI ADICIONADO
                                            </Text>
                                        </View>
                                    }
                                    {
                                        Platform.OS !== 'ios' ?
                                        <Button 
                                            onPress={adicionarBilhete} 
                                            style={{ 
                                                marginBottom: 15
                                            }}
                                        >
                                            <Text style={{ textAlign: 'center' }}>
                                                Adicionar Bilhete
                                            </Text>
                                        </Button> : <></>
                                    }
                                    {
                                        bilhetes.length ?
                                        <Button 
                                            onPress={liberarFragmentacao} 
                                            style={{ 
                                                marginBottom: 15
                                            }}
                                            status="success"
                                        >
                                            <Text style={{ textAlign: 'center' }}>
                                                Liberar para Fragmentação
                                            </Text>
                                        </Button> : <></>
                                    }
                                </>
                                :
                                <Camera
                                    zoom={zoom}
                                    flashMode={flash}
                                    onBarCodeScanned={handleQRCode}
                                    barCodeScannerSettings={{
                                        barCodeTypes: [BarCodeScanner.Constants.BarCodeType.qr]
                                    }}
                                    style={{
                                        ...styles.formScrollContainer, 
                                        ...styles.formScroll,
                                        backgroundColor: 'black',
                                        borderTopLeftRadius: 20,
                                        padding: 0,
                                        height: Dimensions.get("window").height - 80
                                    }}
                                >
                                    <Image
                                        source={require("../../assets/images/qrcode.png")}
                                        style={{
                                            width: '100%',
                                            height: Dimensions.get('window').height - 190
                                        }}
                                        resizeMode="contain"
                                    />
                                    {
                                        zoom ?
                                        <Text style={{
                                            position: 'absolute',
                                            bottom: 140,
                                            alignSelf: 'center',
                                            color: '#fff',
                                            fontSize: 16
                                        }}>{Number(String((zoom)).substr(-1)) + 1} x Zoom</Text> : <></>
                                    }
                                    <TouchableOpacity
                                        onPress={changeFlash}
                                        style={{
                                            alignSelf: 'center',
                                            position: 'absolute',
                                            zIndex: 10,
                                            bottom: 80
                                        }}
                                    >
                                        {
                                            flash == Camera.Constants.FlashMode.off ?
                                            <Image
                                                source={require('../../assets/images/flash-off.png')}
                                                resizeMode="contain"
                                                style={{ 
                                                    width: 64, 
                                                    height: 30,
                                                }}
                                            />
                                            :
                                            <Image
                                                source={require('../../assets/images/flash-on.png')}
                                                resizeMode="contain"
                                                style={{ 
                                                    width: 64, 
                                                    height: 30,
                                                }}
                                            />
                                        }
                                    </TouchableOpacity>
                                    <TouchableOpacity
                                        onPress={() => {changeZoom('out')}}
                                        style={{
                                            alignSelf: 'flex-start',
                                            position: 'absolute',
                                            bottom: 80,
                                            left: 20,
                                            zIndex: 50,
                                        }}
                                    >
                                        <Image
                                            source={require('../../assets/images/minus-button.png')}
                                            resizeMode="contain"
                                            style={{ 
                                                width: 64, 
                                                height: 30,
                                            }}
                                        />
                                    </TouchableOpacity>
                                    <TouchableOpacity
                                        onPress={() => {changeZoom('in')}}
                                        style={{
                                            alignSelf: 'flex-end',
                                            position: 'absolute',
                                            bottom: 80,
                                            right: 20,
                                            zIndex: 50,
                                        }}
                                    >
                                        <Image
                                            source={require('../../assets/images/plus-button.png')}
                                            resizeMode="contain"
                                            style={{ 
                                                width: 64, 
                                                height: 30,
                                            }}
                                        />
                                    </TouchableOpacity>
                                    <View style={{
                                        position: 'absolute',
                                        width: Dimensions.get("window").width,
                                        zIndex: 10,
                                        alignItems: 'center',
                                        alignContent: 'center',
                                        justifyContent: 'center',
                                        top: 30,
                                    }}>
                                        <Text style={{
                                            fontSize: 18,
                                            color: '#fff',
                                            textShadowColor: 'rgba(0, 0, 0, 0.75)',
                                            textShadowOffset: {width: -1, height: 1},
                                            textShadowRadius: 0.1
                                        }}>Validar Cancelamento</Text>
                                    </View>
                                </Camera>
                            }
                        </>
                        :
                        <View style={{
                            backgroundColor: '#e74c3c',
                            padding: 15,
                            borderRadius: 10,
                            width: '100%',
                            shadowColor: "#000",
                            shadowOffset: {
                                width: 0,
                                height: 2,
                            },
                            shadowOpacity: 0.23,
                            shadowRadius: 2.62,
                            elevation: 4,
                        }}>
                            <Text style={{
                                textAlign: 'center',
                                color: 'white',
                                fontSize: 20,
                                textTransform: 'uppercase'
                            }}>
                                Sem Permissão para {'\n'} utilizar a câmera
                            </Text>
                        </View>
                    }
                </>)}
            />
        </>
    );
};