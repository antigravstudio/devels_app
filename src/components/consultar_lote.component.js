
import React, { useEffect, useState } from 'react';
import { 
    TouchableWithoutFeedback, ScrollView, TouchableOpacity, Text, View, Keyboard
} from 'react-native';
import { Button } from '@ui-kitten/components';
import Spinner from 'react-native-loading-spinner-overlay';
import * as FeatherIcon from "react-native-feather";
import { Table, Row } from 'react-native-table-component';

import styles from '../utils/CustomStyles';
import Formater from '../utils/Formater';
import Store from '../utils/Store';

import api from '../service/ApiHandler';

import { Form } from './subcomponents/Form';
import { Modal as ModalLote } from './subcomponents/Modal';

export const ConsultarLoteScreen = (props) => {    
    const [spinner, setSpinner] = useState(false);
    const [spinnerText, setSpinnerText] = useState('Aguarde...');
    const [modalLote, setModalLote] = useState(false);
    const [dados, setDados] = useState(null);
    const [pagina, setPagina] = useState(0);
    const [filtro, setFiltro] = useState('');
    const [dadosModal, setDadosModal] = useState({});

    const searchIcon = () => (<TouchableWithoutFeedback><FeatherIcon.Search width={24} height={24} color="#4E86FB" /></TouchableWithoutFeedback>);

    const widthArray = [80, 50, 100, 100, 100];

    const pesquisar = async () => {
        setSpinner(true);
        setSpinnerText('Consultando...');
        const idDispositivo = await Store.get('idDispositivo');
        const response = await api.consulta({
            chave: filtro ? 'AdmConsult_LoteBusca' : 'AdmConsult_LoteInicial',
            filtro: filtro.toUpperCase(),
            idDispositivo
        });
        if(response) {
            setPagina(0);
            try {
                var paginas = [];
                var i_pagina = 0;
                var i = 0;
                for(var row of response){
                    row.dataCriacao = Formater.formatarData(row.dataCriacao);
                    if(paginas[i_pagina] === undefined) paginas[i_pagina] = [];
                    paginas[i_pagina].push(row);
                    i++;
                    if(i == 5){
                        i = 0;
                        i_pagina++;
                    }
                }
                setDados(paginas);
            } catch(e){
                setDados([]);
            }
            setSpinner(false);
        } else {
            setDados([]);
            setSpinner(false);
        }
    };

    const abrirLote = async (id) => {
        setSpinner(true);
        setSpinnerText('Aguarde...');
        const idDispositivo = await Store.get('idDispositivo');
        const response = await api.consulta({
            chave: 'AdmConsult_LoteSelecao',
            filtro: id,
            idDispositivo
        });
        if(!response || !response.length) {
            setDadosModal({ lote: id, table: {} });
            setModalLote(true);
            setSpinner(false);
        } else {
            setDadosModal({ lote: id, table: response });
            setModalLote(true);
            setSpinner(false);
        }
    };

    const changeFiltro = (text) => {
        setFiltro(text);
    };

    useEffect(() => {
        pesquisar();
    }, []);

    useEffect(() => () => {}, []);

    return (
        <>
            <ModalLote
                show={modalLote} 
                close={setModalLote} 
                content={() => (
                    <>
                        <Text style={{
                            color: '#fff',
                            marginBottom: 5,
                            fontSize: 22,
                            fontWeight: '400'
                        }}>Rastreamento Lote</Text>
                        <Text style={{
                            color: '#fff',
                            marginBottom: 20,
                            fontSize: 22,
                            fontWeight: '400'
                        }}>[Athens]</Text>
                        {
                            (dadosModal.table?dadosModal.table:[]).length ?
                            <>
                                {
                                    dadosModal.table.map((row, index) => (
                                        <React.Fragment key={index}>
                                            <View style={{...styles.table, flexDirection: 'column', marginBottom: 15, width: '100%', flex: 1, paddingTop: 0 }}>
                                                <View style={{ flexDirection: 'row', width: '100%', marginTop: 10 }}>
                                                    <View style={{ width: '50%' }}>
                                                        <Text style={{ fontWeight: 'bold', fontSize: 16 }}>Sublote:</Text>
                                                        <Text style={{ fontSize: 16 }}>{row.numerO_SUBLOTE}</Text>
                                                    </View>
                                                    <View style={{ width: '50%' }}>
                                                        <Text style={{ fontWeight: 'bold', fontSize: 16 }}>Tipo Bilhete:</Text>
                                                        <Text style={{ fontSize: 16 }}>{row.tipO_BILHETE}</Text>
                                                    </View>
                                                </View>
                                                <View style={{ flexDirection: 'row', width: '100%', marginTop: 10 }}>
                                                    <View style={{ width: '50%' }}>
                                                        <Text style={{ fontWeight: 'bold', fontSize: 16 }}>Qtd. Sublote: </Text>
                                                        <Text style={{ fontSize: 16 }}>{row.quantidadE_SUBLOTE}</Text>
                                                    </View>
                                                    <View style={{ width: '50%' }}>
                                                        <Text style={{ fontWeight: 'bold', fontSize: 16 }}>Consumido:</Text>
                                                        <Text style={{ fontSize: 16 }}>{row.esgotado?'Sim' : 'Não'}</Text>
                                                    </View>
                                                </View>
                                                <Text style={{ fontSize: 16, marginBottom: 5, marginTop: 10 }}>
                                                    <Text style={{ fontWeight: 'bold' }}>Transporte</Text>{' '}- Transportadora
                                                </Text>
                                                <View style={{ flexDirection: 'row', width: '100%', paddingLeft: 10 }}>
                                                    <View style={{ width: '100%' }}>
                                                        <Text style={{ fontSize: 16 }}>
                                                            <FeatherIcon.ArrowRightCircle 
                                                                color="#000"
                                                                width={14}
                                                                height={14}
                                                                style={{ 
                                                                    alignSelf: 'center', 
                                                                    marginRight: 5, 
                                                                    flexDirection: 'row'
                                                                }}
                                                            /> {row.datA_SAIDA_SPTRANS?`${row.datA_SAIDA_SPTRANS} - ${row.horA_SAIDA_SPTRANS.substring(0, 5)}`:'...'}
                                                            {' '}- <Text style={{fontStyle:'italic'}}>Saída SP Trans</Text>
                                                        </Text>
                                                        <Text style={{ fontSize: 16, marginTop: 5 }}>
                                                            <FeatherIcon.ArrowRightCircle 
                                                                color="#000"
                                                                width={14}
                                                                height={14}
                                                                style={{ 
                                                                    alignSelf: 'center', 
                                                                    marginRight: 5, 
                                                                    flexDirection: 'row'
                                                                }}
                                                            /> {row.datA_CHEGADA_CUSTODIA?`${row.datA_CHEGADA_CUSTODIA} - ${row.horA_CHEGADA_CUSTODIA.substring(0, 5)}`:'...'}
                                                            {' '}- <Text style={{fontStyle:'italic'}}>Chegada Custódia</Text>
                                                        </Text>
                                                        <Text style={{ fontSize: 16, marginTop: 5 }}>
                                                            <FeatherIcon.ArrowRightCircle 
                                                                color="#000"
                                                                width={14}
                                                                height={14}
                                                                style={{ 
                                                                    alignSelf: 'center', 
                                                                    marginRight: 5, 
                                                                    flexDirection: 'row'
                                                                }}
                                                            /> {row.datA_SAIDA_CUSTODIA?`${row.datA_SAIDA_CUSTODIA} - ${row.horA_SAIDA_CUSTODIA.substring(0, 5)}`:'...'}
                                                            {' '}- <Text style={{fontStyle:'italic'}}>Saída Custódia</Text>
                                                        </Text>
                                                        <Text style={{ fontSize: 16, marginTop: 5 }}>
                                                            <FeatherIcon.ArrowRightCircle 
                                                                color="#000"
                                                                width={14}
                                                                height={14}
                                                                style={{ 
                                                                    alignSelf: 'center', 
                                                                    marginRight: 5, 
                                                                    flexDirection: 'row'
                                                                }}
                                                            /> {row.datA_CHEGADA_POSTO?`${row.datA_CHEGADA_POSTO} - ${row.horA_CHEGADA_POSTO.substring(0, 5)}`:'...'}
                                                            {' '}- <Text style={{fontStyle:'italic'}}>Chegada Posto</Text>
                                                        </Text>
                                                    </View>
                                                </View>
                                                <Text style={{ fontWeight: 'bold', fontSize: 16, marginTop: 10}}>Entrega ao Operador: </Text>
                                                <View style={{ flexDirection: 'row', width: '100%' }}>
                                                    <View style={{ width: '100%', paddingLeft: 10 }}>
                                                        <Text style={{ fontSize: 16 }}>
                                                            <FeatherIcon.ArrowRightCircle 
                                                                color="#000"
                                                                width={14}
                                                                height={14}
                                                                style={{ 
                                                                    alignSelf: 'center', 
                                                                    marginRight: 5 
                                                                }}
                                                            />
                                                           Operador: {row.operador?row.operador:'...'}
                                                        </Text>
                                                    </View>
                                                </View>
                                                <View style={{ flexDirection: 'row', width: '100%', marginTop: 10 }}>
                                                    <View style={{ width: '50%' }}>
                                                        <Text style={{ fontWeight: 'bold', fontSize: 16 }}>Data Vinculação:</Text>
                                                        <Text style={{ fontSize: 16 }}>{row.datA_VINCULACAO?`${row.datA_VINCULACAO} às ${row.horA_VINCULACAO.substring(0, 5)}`:'...'}</Text>
                                                    </View>
                                                    <View style={{ width: '50%' }}>
                                                        <Text style={{ fontWeight: 'bold', fontSize: 16 }}>Posto:</Text>
                                                        <Text style={{ fontSize: 16 }}>{row.unidade}</Text>
                                                    </View>
                                                </View>
                                            </View>
                                        </React.Fragment>
                                    ))
                                }
                            </> :  
                            <View style={{...styles.warningAlert, marginBottom: 30 }}>
                                <Text style={styles.alertText}>
                                    NENHUM DADO FOI LOCALIZADO
                                </Text>
                            </View>
                        }
                    </>
                )}
            />
            <Spinner
                visible={spinner}
                textContent={spinnerText}
                textStyle={{ color: '#fff'}}
                color="#fff"
                overlayColor="rgba(0, 0, 0, 0.85)"
                hidden={false}
                animation="fade"
            />
            <Form 
                {...props}
                title="Consultar Lote"
                menu={true}
                inputs={[{
                    style: { marginBottom: 10 },
                    accessoryLeft: searchIcon,
                    placeholder: "BUSCA POR NÚMERO DO LOTE",
                    textStyle: { textAlign: 'center' },
                    autoCorrect: false,
                    defautValue: filtro,
                    autoCapitalize: "none",
                    autoCorrect: false,
                    returnKeyType: 'done',
                    keyboardType: 'number-pad',
                    autoCapitalize: 'none',
                    onChangeText: changeFiltro,
                    onSubmitEditing: pesquisar
                }]}
                content={() => (<>
                    <Button
                        size="small" 
                        style={{ 
                            marginBottom: 10, width: 100, alignSelf: 'flex-end',
                        }}
                        onPress={() => { pesquisar(); Keyboard.dismiss(); }}
                    >Pesquisar</Button>
                    {
                        dados !== null ?
                        <>
                            {
                                dados.length ?
                                <>
                                    <ScrollView 
                                        bounces={false} 
                                        showsVerticalScrollIndicator={false} 
                                        showsHorizontalScrollIndicator={false} 
                                        horizontal={true}
                                        vertical={true}
                                        style={{
                                            width: '100%',
                                            marginBottom: 5
                                        }}
                                    >
                                        <Table borderStyle={styles.tableBorderStyle} style={{
                                            width: '100%'
                                        }}>
                                            <Row 
                                                data={[
                                                    'Lote',
                                                    'Qt.',
                                                    'Tipo',
                                                    'Esgotado',
                                                    'Visualizar'
                                                ]} 
                                                textStyle={styles.tableHeaderText}
                                                style={styles.tableHeader}
                                                widthArr={widthArray}
                                            />
                                            {
                                                dados[pagina].map((row, index) => (
                                                    <TouchableOpacity key={index} onPress={() => abrirLote(row.numerO_LOTE)}>
                                                        <Row
                                                            data={[
                                                                row.numerO_LOTE,
                                                                row.quantidadE_LOTE,
                                                                row.tipO_BILHETE,
                                                                row.esgotado ? 'Sim' : 'Não',
                                                                <FeatherIcon.Search 
                                                                    color="#4E86FB"
                                                                    width={18}
                                                                    height={18}
                                                                    style={{ alignSelf: 'center' }}
                                                                />
                                                            ]}
                                                            style={[styles.row, index%2 && {backgroundColor: '#EEF5FE'}]}
                                                            textStyle={{padding: 10, textAlign: 'center'}}
                                                            widthArr={widthArray}
                                                        />
                                                    </TouchableOpacity>
                                                ))
                                            }
                                        </Table>
                                    </ScrollView>
                                    <View style={styles.pagination}>
                                        {
                                            dados.map((row, index) => (
                                                <TouchableOpacity 
                                                    key={index}
                                                    style={{...styles.paginationDot, backgroundColor: pagina == index ? '#E7EAFF' : 'white'}}
                                                    onPress={() => setPagina(index)}
                                                >
                                                    <Text style={{fontSize: 18}}>{index + 1}</Text>
                                                </TouchableOpacity>
                                            ))
                                        }
                                    </View>
                                </> : <>
                                <View style={{...styles.warningAlert, marginBottom: 30 }}>
                                        <Text style={styles.alertText}>
                                            NENHUM DADO FOI LOCALIZADO
                                        </Text>
                                    </View>
                                </>
                            }
                        </> : <></>
                    }
                </>)}
            />
        </>
    );
};