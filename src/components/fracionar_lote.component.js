import React, { useEffect, useState } from 'react';
import Spinner from 'react-native-loading-spinner-overlay';
import { 
    TouchableOpacity, TouchableWithoutFeedback, Keyboard, View, ScrollView, Image
} from 'react-native';
import * as FeatherIcon from "react-native-feather";
import { Text } from '@ui-kitten/components';
import * as Linking from 'expo-linking';

import { Form } from './subcomponents/Form';

import Alert from '../utils/Alert';
import Store from '../utils/Store';
import styles from '../utils/CustomStyles';

import api from '../service/ApiHandler';

export const FracionarLoteScreen = (props) => {  
    const [spinner, setSpinner] = useState(true);
    const [spinnerText, setSpinnerText] = useState('Aguarde...');

    const [tipoBilhete, setTipoBilhete] = useState('');
    const [tipoBilheteLabel, setTipoBilheteLabel] = useState('...');
    const [tiposBilhetes, setTiposBilhetes] = useState([]);
    const [lotesDisponiveis, setLotesDisponiveis] = useState([]);
    const [lote, setLote] = useState('');
    const [loteLabel, setLoteLabel] = useState('...');
    const [numeroBoleta, setNumeroBoleta] = useState('');
    const [numeroSublotes, setNumeroSublotes] = useState(0);
    const [numeroCartoes, setNumeroCartoes] = useState(0);
	const [cartoes, setCartoes] = useState([]);
    const [iniciado, setIniciado] = useState(false);
    const [sublotes, setSubLotes] = useState([]);
    const [filtrar, setFiltrar] = useState(true);
    const [totalCartoes, setTotalCartoes] = useState(0);
    const [letraSeguinte, setLetraSeguinte] = useState('A');

    const [expandTipo, setExpandTipo] = useState(false);
    const [expandLote, setExpandLote] = useState(false);

    const changeLote = async (item) => {
        setNumeroCartoes(Number(item.quantidade));
        setLoteLabel(item.label);
        setLote(item.value);
        setExpandLote(false);
        await Store.set('numeroLote', String(item.value));
        await Store.set('loteLabel', String(item.label));
        await Store.set('numeroCartoes', String(item.quantidade));
    };

    const abrirLeitor = () => {
        Keyboard.dismiss();
        props.navigation.navigate('BarCode');
        let interval = setInterval(async () => {
            var leitura_boleta = await Store.get('leitura-boleta');
            if(leitura_boleta){
                clearInterval(interval);
                changeNumeroBoleta(leitura_boleta);
            }
        }, 500);
    };

    const obterLetraSeguinte = async () => {
        var total_sublotes = await Store.get('totalSublotes');
        if(total_sublotes) total_sublotes = Number(total_sublotes);
        else total_sublotes = 0;
        var letra = ((total_sublotes + 1) + 9).toString(36).toUpperCase();
        setLetraSeguinte(letra);
        return letra;
    };

    const iniciarLeitura = async () => {
        setSpinnerText('Iniciando leitura');
        setSpinner(true);
        var letra = await obterLetraSeguinte();
        try {
            Linking.openURL(`devels://nfc/leitura_lote/${String(lote)}-${letra}/${String(numeroCartoes / numeroSublotes)}`);
            setSpinner(false);
        } catch(e){
            Alert.simple({ 
                subtitle: 'Erro ao iniciar o processo de leitura',
                accept: () => {
                    setSpinner(false);
                }
            });
        }
    };


    const barcodeIcon = () => (
        <TouchableWithoutFeedback onPress={abrirLeitor}>
            <FeatherIcon.Camera 
                color="#4494F2" 
                style={{
                    marginRight: 5
                }} 
            />
        </TouchableWithoutFeedback>
    );

    const changeNumeroBoleta = async (text) => {
        setNumeroBoleta(text);
    };

	const changeBilhete = async (item) => {
        setSpinnerText('Aguarde...');
        setSpinner(true);
        setTipoBilheteLabel('...');
        setTipoBilhete('');

        await Store.remove('numeroLote');
        await Store.remove('loteLabel');

        const idDispositivo = await Store.get('idDispositivo');
        const response2 = await api.lotesDisponiveis({ 
            idDispositivo, idTipoBilhete: item.value
        });
        if(response2){
            if(response2.length){
                function ordemLotes(a, b) {
                    if ( a.ordem < b.ordem ){
                        return -1;
                    }
                    if ( a.ordem > b.ordem ){
                        return 1;
                    }
                    return 0;
                }

                setTipoBilheteLabel(item.label);
                setTipoBilhete(item.value);
                setNumeroSublotes(Number(item.quantidade));
                setExpandTipo(false);
                await Store.set('tipoBilheteLabel', item.label);
                await Store.set('tipoBilhete', String(item.value));
                await Store.set('numeroSublotes', String(item.quantidade));

                var _lotesDisponiveis = [];
                for(var row of response2){
                    _lotesDisponiveis.push({
                        label: row.numeroLote?String(row.numeroLote):'', 
                        value: row.numeroLote,
                        quantidade: row.quantidade
                    });
                }
                _lotesDisponiveis = _lotesDisponiveis.sort(ordemLotes);
                setLotesDisponiveis(_lotesDisponiveis);
                setSpinner(false);
                setLote('');
                setLoteLabel('...');
            } else {
                Alert.simple({ 
                    title: item.label,
                    subtitle: 'Nenhum lote disponível',
                    accept: () => {
                        setSpinner(false);
                    }
                });
            }
        } else {
            Alert.simple({ 
                subtitle: 'Erro de conexão',
                accept: () => {
                    setSpinner(false);
                }
            });
        }
    };

    const inserirSubLotes = async () => {
        setSpinner('Enviando lote...');
        setSpinner(true);
        const idDispositivo = await Store.get('idDispositivo');
        const sucessos = 0;
        var i = 0;
        for(const s of sublotes){
            var response = await api.inserirSubLote({
                idDispositivo,
                numeroSubLote: `${s.lote}-${s.letra}`,
                boleta: s.numeroBoleta,
                bilhetes: s.cartoes,
                idTipoBilhete: tipoBilhete,
            });
            if(response){
                await Store.remove(`sublote${s.numeroBoleta}`);
                await Store.remove(`cartoes${s.numeroBoleta}`);
                await Store.remove(`sublote${i}`);
                sucessos++;
            }
            i += 1;
        }
        if(sucessos > 0){
            Alert.simple({ 
                title: item.label,
                subtitle: 'Lote enviado com sucesso!',
                accept: async () => {
                    setSpinnerText("Aguarde...");
                    await Store.remove('numeroLote');
                    await Store.remove('totalSublotes');
                    await Store.remove('totalCartoes');
                    await Store.remove('loteLabel');
                    await Store.remove('tipoBilhete');
                    await Store.remove('tipoBilheteLabel');
                    await Store.remove('cartoes');
                    await Store.remove('sublotes');
                    setSpinner(false);
                    props.navigation.reset({ index: 0, routes: [{ name: 'Menu' }]});
                }
            });
        } else {
            Alert.simple({ 
                subtitle: 'Não foi possível enviar os dados',
                accept: () => {
                    setSpinner(false);
                }
            });
        }
    
        setSpinner(false);
    };

    const cancel = async () => {
        setSpinnerText("Cancelando...");
        setSpinner(true);
        await Store.remove('numeroLote');
        await Store.remove('totalSublotes');
        await Store.remove('totalCartoes');
        await Store.remove('loteLabel');
        await Store.remove('tipoBilhete');
        await Store.remove('tipoBilheteLabel');
        await Store.remove('cartoes');
        await Store.remove('sublotes');
        var i = 0;
        for(const s of sublotes){
            await Store.remove(`sublote${s.numeroBoleta}`);
            await Store.remove(`cartoes${s.numeroBoleta}`);
            await Store.remove(`sublote${s.i}`);
            i += 1;
        }
        setSpinner(false);
    };

    const fracionarSublote = async () => {
        if(!numeroBoleta) Alert.simple({ subtitle: 'Infome o número da boleta' });
        else if(numeroBoleta.length < 7) Alert.simple({ subtitle: 'Número de boleta inválido' });
        else {
            setSpinnerText("Fracionando...");
            setSpinner(true);
            var _sublote = {
                tipoBilhete,
                tipoBilheteLabel,
                lote,
                loteLabel,
                numeroBoleta,
            };
            // Sublote
            var existe_sublote = await Store.get('sublote' + String(numeroBoleta));
            if(existe_sublote){
                Alert.simple({ 
                    subtitle: 'Boleta já adicionada anteriormente',
                    accept: async () => setSpinner(false)
                })
                return false;
            }
            var total_sublotes = await Store.get('totalSublotes');
            if(total_sublotes) total_sublotes = Number(total_sublotes);
            else total_sublotes = 0;
            if(!existe_sublote){
                total_sublotes = Number(total_sublotes) + 1;
                _sublote.letra = (total_sublotes + 9).toString(36).toUpperCase();
                await Store.set('sublote' + String(total_sublotes), JSON.stringify(_sublote));
                await Store.set('sublote' + String(numeroBoleta), 'existe');
            }
            await Store.set('totalSublotes', String(total_sublotes));
            // Cartões
            var existe_cartoes = await Store.get('cartoes' + String(numeroBoleta));
            var total_cartoes = await Store.get('totalCartoes');
            if(total_cartoes) total_cartoes = Number(total_cartoes);
            else total_cartoes = 0;
            if(!existe_cartoes){
                total_cartoes = Number(total_cartoes) + cartoes.length;
                await Store.set('cartoes' + String(numeroBoleta), JSON.stringify(cartoes));
            }
            await Store.set('totalCartoes', String(total_cartoes));
            await Store.remove('cartoes');
            setCartoes([]);
            setNumeroBoleta('');
            await lerSublotes();
            setFiltrar(false);
            props.navigation.replace('FracionarLote', { no_nfc: true });
        }
    };
    
    const lerSublotes = async () => {
        var total_sublotes = await Store.get('totalSublotes');
        if(total_sublotes) total_sublotes = Number(total_sublotes);
        var _sublotes = [];
        for(var i = 1; i <= total_sublotes; i++){
            var objeto = `sublote${i}`;
            var sublote = await Store.get(objeto);
            if(sublote){
                sublote = JSON.parse(sublote);
                var __cartoes = await Store.get(`cartoes${sublote.numeroBoleta}`);
                if(__cartoes){
                    __cartoes = JSON.parse(__cartoes);
                    sublote.cartoes = __cartoes;
                    _sublotes.push(sublote);
                }
            }
        }
        setSubLotes(_sublotes);
    };

    useEffect(() => {
        var validar_nfc = true;
        try {
            if(props.route.params.cartoes) validar_nfc = false;
        } catch(e){ }
        try {
            if(props.route.params.no_nfc) validar_nfc = false;
        } catch(e){ }

        setSpinner(true);
        setSpinnerText('Aguarde...');
        (async function () {
            if(validar_nfc){
                try {
                    await Linking.openURL(`devels://nfc/verificar_versao/1.0.1`);
                } catch(e){
                    Alert.confirm({
                        title: 'A utilidade de leitura de cartões ainda não foi instalada', 
                        subtitle: 'Deseja instalar o complemento agora?',
                        accept: async () => {
                            await Linking.openURL(`http://projects.samuelcarrara.com/devels/devels-nfc-1.0.1.apk`);
                            setSpinner(false);
                            props.navigation.reset({ index: 0, routes: [{ name: 'Menu' }]});
                        }, 
                        cancel: async () => {
                            setSpinner(false);
                            props.navigation.reset({ index: 0, routes: [{ name: 'Menu' }]});
                        }
                    });
                    return false;
                }
            }

            await obterLetraSeguinte();
            var _lote = await Store.get('numeroLote');
            var _cartoes = await Store.get('cartoes');
            if(_lote || _cartoes){
                setLote(_lote);
                var _tipoBilhete = await Store.get('tipoBilhete');
                setTipoBilhete(_tipoBilhete);
                var _tipoBilheteLabel = await Store.get('tipoBilheteLabel');
                setTipoBilheteLabel(_tipoBilheteLabel);
                var _numeroCartoes = await Store.get('numeroCartoes');
                setNumeroCartoes(Number(_numeroCartoes));
                var _numeroSublotes = await Store.get('numeroSublotes');
                setNumeroSublotes(Number(_numeroSublotes));
                var _loteLabel = await Store.get('loteLabel');
                setLoteLabel(_loteLabel);
                setIniciado(true);
                var _totalCartoes = await Store.get('totalCartoes');
                if(_totalCartoes) _totalCartoes = Number(_totalCartoes);
                else _totalCartoes = 0;
                setTotalCartoes(_totalCartoes);
                if(filtrar){
                    try {
                        if(_cartoes) _cartoes = JSON.parse(_cartoes);
                        else _cartoes = [];
                        var _cartoes2 = props.route.params.cartoes;
                        for(const c of _cartoes2){
                            var index = _cartoes.indexOf(c);
                            if(index < 0) _cartoes.push(c);
                        }
                        setCartoes(_cartoes);
                        await Store.set('cartoes', JSON.stringify(_cartoes));
                        await lerSublotes();
                        if(!numeroBoleta && _cartoes.length && _totalCartoes < _numeroCartoes){
                            abrirLeitor();
                            setSpinner(false);
                            return false;
                        }
                    } catch(e){ }
                }
                await lerSublotes();
                
                setSpinner(false);

                return false;
            }

            setTipoBilhete('');
            setLote('');
            setLoteLabel('...');
            setTipoBilheteLabel('...');
            setTiposBilhetes([]);
            setIniciado(false);
            
            await Store.remove('numeroLote');
            await Store.remove('loteLabel');
            await Store.remove('tipoBilhete');
            await Store.remove('tipoBilheteLabel');
            await Store.remove('cartoes');

            const idDispositivo = await Store.get('idDispositivo');
            const response1 = await api.tiposBilhetes({ idDispositivo });
            if(response1){
                function ordemBilhetes(a, b) {
                    if ( a.ordem < b.ordem ){
                        return -1;
                    }
                    if ( a.ordem > b.ordem ){
                        return 1;
                    }
                    return 0;
                }

                if(response1.length){
                    var _tiposBilhetes = [];
                    for(var row of response1){
                        _tiposBilhetes.push({
                            label: row.descricao?row.descricao:'', 
                            value: row.tipoBilheteId, 
                            quantidade: row.qtdeSubLotes
                        });
                    }
                    _tiposBilhetes = _tiposBilhetes.sort(ordemBilhetes);
                    setTiposBilhetes(_tiposBilhetes);
                }
            }
            setSpinner(false);
        })();
    }, []);

    useEffect(() => () => {}, []);

    return (
        <>
            <Spinner
                visible={spinner}
                textContent={spinnerText}
                textStyle={{ color: '#fff'}}
                color="#fff"
                overlayColor="rgba(0, 0, 0, 0.95)"
                hidden={false}
                animation="fade"
            />
            <Form 
                {...props}
                back={false}
                title={(sublotes.length||iniciado) && totalCartoes < numeroCartoes?"":"Fracionar Lote"}
                menu={false}
                cancel={cancel}
                headerButton={() => (<>
                    {
                        sublotes.length && iniciado ?
                        <>
                            {
                                totalCartoes < numeroCartoes ?
                                <Text style={{
                                    textAlign: 'center',
                                    marginBottom: 10
                                }}>
                                    Você escaneou {'\n'}
                                    <Text style={{
                                        fontWeight: 'bold',
                                        fontSize: 24
                                    }}>
                                        {totalCartoes} de {numeroCartoes} cartões
                                    </Text>
                                </Text> : <></>
                }
                            {
                                sublotes.map((row, index) => (
                                    <View
                                        key={index}
                                        style={{
                                            paddingHorizontal: 10,
                                            paddingVertical: 10,
                                            backgroundColor: '#121212',
                                            borderRadius: 6,
                                            marginBottom: 10,
                                            flexDirection: 'row'
                                        }}
                                    >
                                        <FeatherIcon.Archive 
                                            color="#ffff"
                                            width={14} 
                                            height={14}
                                            style={{ 
                                                marginRight: 5, marginTop: 4
                                            }}
                                        />
                                        <Text style={{ color: '#fff' }}>
                                            {row.lote}-{row.letra} / {row.numeroBoleta}
                                        </Text>
                                        <Text style={{
                                            color: '#fff',
                                            position: 'absolute',
                                            right: 30,
                                            top: 9
                                        }}>{row.cartoes.length}</Text>
                                        <FeatherIcon.CreditCard 
                                            color="#ffff"
                                            width={14} 
                                            height={14}
                                            style={{ 
                                                top: 13,
                                                position: 'absolute',
                                                right: 10
                                            }}
                                        />
                                    </View>
                                ))
                            }
                        </> : <></>
                    }
                    {
                        iniciado && totalCartoes < numeroCartoes ?
                        <>
                            <Text style={{
                                textTransform: 'uppercase',
                                marginBottom: 10,
                                color: 'darkgray'
                            }}>
                                Tipo de Bilhete
                            </Text>
                            <View 
                                style={{
                                    ...styles.customCombo,
                                    borderBottomLeftRadius: 6,
                                    borderBottomRightRadius: 6,
                                    marginBottom: 10,
                                }}
                            >
                                <FeatherIcon.Archive 
                                    color="#4494F2"
                                    width={16} 
                                    height={16}
                                    style={{ marginRight: 5 }}
                                />
                                <Text
                                    style={{
                                        position: 'absolute',
                                        left: 40,
                                        top: 8
                                    }}
                                >{tipoBilheteLabel}</Text>
                            </View>
                            <Text style={{
                                textTransform: 'uppercase',
                                marginBottom: 10,
                                color: 'darkgray'
                            }}>
                                Lote
                            </Text>
                            <View 
                                style={{
                                    ...styles.customCombo,
                                    borderBottomLeftRadius: 6,
                                    borderBottomRightRadius: 6,
                                    marginBottom: 10,
                                }}
                            >
                                <FeatherIcon.Archive 
                                    color="#4494F2"
                                    width={16} 
                                    height={16}
                                    style={{ marginRight: 5 }}
                                />
                                <Text
                                    style={{
                                        position: 'absolute',
                                        left: 40,
                                        top: 8
                                    }}
                                >{loteLabel}</Text>
                            </View>
                        </> : <></>
                    }
                    {
                        !iniciado ?
                        <>
                            <Text style={{
                                textTransform: 'uppercase',
                                marginBottom: 10,
                                color: 'darkgray'
                            }}>
                                Tipo de Bilhete
                            </Text>
                            <TouchableOpacity 
                                style={{
                                    ...styles.customCombo,
                                    borderBottomLeftRadius: expandTipo ? 0 : 6,
                                    borderBottomRightRadius: expandTipo ? 0 : 6,
                                    marginBottom: expandTipo ? 0 : 10,
                                }}
                                onPress={() => setExpandTipo(!expandTipo)}
                            >
                                <FeatherIcon.Archive 
                                    color="#4494F2"
                                    width={16} 
                                    height={16}
                                    style={{ marginRight: 5 }}
                                />
                                <Text
                                    style={{
                                        position: 'absolute',
                                        left: 40,
                                        top: 10
                                    }}
                                >{tipoBilheteLabel}</Text>
                                {
                                    expandTipo ?
                                    <FeatherIcon.ChevronUp 
                                        width={20} 
                                        height={20} 
                                        color={'#000'} 
                                        style={{ position: 'absolute', right: 5, top: 8 }}
                                    />
                                    :
                                    <FeatherIcon.ChevronDown 
                                        width={20} 
                                        height={20} 
                                        color={'#000'} 
                                        style={{ position: 'absolute', right: 5, top: 8 }}
                                    />
                                }
                            </TouchableOpacity> 
                        </> : <></>
                    }
                    {
                        expandTipo && !iniciado ?
                        <ScrollView 
                            bounces={false}
                            style={{ 
                                marginBottom: 15
                            }}
                        >
                            {
                                tiposBilhetes.map((row, index) => (
                                    <TouchableOpacity 
                                        key={index} 
                                        style={styles.customComboOption}
                                        onPress={() => changeBilhete(row)}
                                    >
                                        <Text>{row.label}</Text>
                                    </TouchableOpacity>
                                ))
                            }
                        </ScrollView> : <></>
                    }
                    {
                        tipoBilhete && !iniciado ?
                        <>
                            <Text style={{
                                textTransform: 'uppercase',
                                marginBottom: 10,
                                color: 'darkgray',
                                marginTop: 10
                            }}>
                                Lote
                            </Text>
                            <TouchableOpacity 
                                style={{
                                    ...styles.customCombo,
                                    borderBottomLeftRadius: expandLote ? 0 : 6,
                                    borderBottomRightRadius: expandLote ? 0 : 6,
                                    marginBottom: expandLote ? 0 : 10,
                                }}
                                onPress={() => setExpandLote(!expandLote)}
                            >
                                <FeatherIcon.Archive 
                                    color="#4494F2"
                                    width={16} 
                                    height={16}
                                    style={{ marginRight: 5 }}
                                />
                                <Text
                                    style={{
                                        position: 'absolute',
                                        left: 40,
                                        top: 10
                                    }}
                                >{loteLabel}</Text>
                                {
                                    expandLote ?
                                    <FeatherIcon.ChevronUp 
                                        width={20} 
                                        height={20} 
                                        color={'#000'} 
                                        style={{ position: 'absolute', right: 5, top: 8 }}
                                    />
                                    :
                                    <FeatherIcon.ChevronDown 
                                        width={20} 
                                        height={20} 
                                        color={'#000'} 
                                        style={{ position: 'absolute', right: 5, top: 8 }}
                                    />
                                }
                            </TouchableOpacity>
                            {
                                expandLote && !iniciado ?
                                <ScrollView 
                                    bounces={false}
                                    style={{ 
                                        marginBottom: 15
                                    }}
                                >
                                    {
                                        lotesDisponiveis.map((row, index) => (
                                            <TouchableOpacity 
                                                key={index} 
                                                style={styles.customComboOption}
                                                onPress={() => changeLote(row)}
                                            >
                                                <Text>{row.label}</Text>
                                            </TouchableOpacity>
                                        ))
                                    }
                                </ScrollView> : <></>
                            }
                        </> : <></>
                    }
                    {
						tipoBilhete && lote && !cartoes.length && totalCartoes < numeroCartoes ?
						<TouchableOpacity 
                            onPress={iniciarLeitura}
                            style={{
                                marginTop: 10,
                                paddingVertical: 40,
                                paddingHorizontal: 20,
                                borderColor: '#EFF1FD',
                                backgroundColor: '#EFF1FD',
                                borderWidth: 1.5,
                                borderRadius: 6,
                                alignItems: 'flex-start'
                            }}
                        >
                            <Image
                                source={require("../../assets/images/nfc.png")}
                                resizeMode="contain"
                                style={{ 
                                    width: 40, 
                                    height: 40,
                                    position: 'absolute',
                                    right: 15,
                                    top: 20
                                }}
                            />
                            <Text style={{ 
                                color: '#000', 
                                fontSize: 22,
                                position: 'absolute',
                                top: 11,
                                left: 15
                            }}>
                                Iniciar Leitura
                                {'\n'}
                                <Text style={{fontSize: 22, fontWeight: 'bold'}}>
                                    {lote}-{letraSeguinte}
                                </Text>
                            </Text>
                        </TouchableOpacity> : <></>
					}
                    {
                        tipoBilhete && lote && cartoes.length && totalCartoes < numeroCartoes ?
                        <>
                            <Text style={{
                                textTransform: 'uppercase',
                                marginBottom: 10,
                                color: 'darkgray'
                            }}>
                                Leitura de Cartões
                            </Text>
                            <View 
                                style={{
                                    ...styles.customCombo,
                                    borderBottomLeftRadius: 6,
                                    borderBottomRightRadius: 6,
                                    marginBottom: 10,
                                }}
                            >
                                <FeatherIcon.CreditCard 
                                    color="#4494F2"
                                    width={16} 
                                    height={16}
                                    style={{ marginRight: 5 }}
                                />
                                <Text
                                    style={{
                                        position: 'absolute',
                                        left: 40,
                                        top: 8
                                    }}
                                >{cartoes.length} cartões foram lidos</Text>
                            </View>
                        </> : <></>
                    }
                    {
                        tipoBilhete && lote && cartoes.length && totalCartoes < numeroCartoes ?
                        <Text style={{
                            textTransform: 'uppercase',
                            marginBottom: 10,
                            color: 'darkgray',
                            marginTop: 10
                        }}>
                            Número da Boleta
                        </Text> : <></>
                    }
                </>)}
                inputs={tipoBilhete && lote && cartoes.length && totalCartoes < numeroCartoes ? [{
                    style: { 
                        marginBottom: 10, 
                        backgroundColor: 'white',
                    },
                    containerStyle: {
                        borderColor: '#EFF1FD',
                        borderWidth: 2
                    },
                    placeholder: "Número da Boleta",
                    textStyle: { textAlign: 'left' },
                    autoCorrect: false,
                    value: numeroBoleta,
                    defaultValue: numeroBoleta,
                    autoCapitalize: "none",
                    autoCorrect: false,
                    returnKeyType: 'done',
                    autoCapitalize: 'none',
                    onChangeText: changeNumeroBoleta,
                    keyboardType: 'number-pad',
                    accessoryRight: barcodeIcon
                }] : []}
                content={() => (<>
                    {
                        tipoBilhete && lote && cartoes.length && totalCartoes < numeroCartoes ?
                        <>
                            <TouchableOpacity
                                style={{ 
                                    padding: 10,
                                    width: '100%',
                                    textAlign: 'center',
                                    backgroundColor: '#4494F2',
                                    borderRadius: 6,
                                    marginTop: 10 
                                }}
                                onPress={fracionarSublote}
                            >
                                <Text
                                    style={{
                                        textAlign: 'center',
                                        color: 'white',
                                        textTransform: 'uppercase'
                                    }}
                                >Fracionar Lote</Text>
                            </TouchableOpacity> 
                        </> : <></>
                    }
                    {
                        totalCartoes >= numeroCartoes ?
                        <TouchableOpacity
                            style={{ 
                                paddingHorizontal: 10,
                                paddingVertical: 15,
                                width: '100%',
                                textAlign: 'center',
                                backgroundColor: '#4494F2',
                                borderRadius: 6,
                                marginTop: 10 
                            }}
                            onPress={inserirSubLotes}
                        >
                            <Text
                                style={{
                                    textAlign: 'center',
                                    color: 'white',
                                    textTransform: 'uppercase'
                                }}
                            >Enviar Lote</Text>
                        </TouchableOpacity>  : <></>
                    }
                </>)}
            />
        </>
    );
};