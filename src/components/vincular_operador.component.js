import React, { useEffect, useState } from 'react';
import { 
    View, Text, Dimensions, Image, TouchableOpacity, TouchableWithoutFeedback, Platform
} from 'react-native';
import { BarCodeScanner } from 'expo-barcode-scanner';
import Spinner from 'react-native-loading-spinner-overlay';
import { Button } from '@ui-kitten/components';
import * as Linking from 'expo-linking';
import { Camera } from 'expo-camera';

import { Form } from './subcomponents/Form';
import api from '../service/ApiHandler';
import styles from '../utils/CustomStyles';
import Store from '../utils/Store';
import Alert from '../utils/Alert';

export const VincularOperadorScreen = (props) => {  
    const [spinner, setSpinner] = useState(true);
    const [spinnerText, setSpinnerText] = useState('Aguarde...');  
    const [sublote, setSublote] = useState('');
    const [hasPermission, setHasPermission] = useState(null);
    const [scanned, setScanned] = useState(false);
    const [zoom, setZoom] = useState(0);
    const [flash, setFlash] = useState(null);
    const [cartao, setCartao] = useState('');
    const [inputs, setInputs] = useState([]);

    const handleBarCode = async (resultado) => {
        if(scanned) return false;
        if(resultado){
            setSublote(String(resultado.data));
            setScanned(true);
            setUpInputs(String(resultado.data));
        }
    };

    const lerBilhete = async () => {
        await Linking.openURL(`devels://nfc/leitura_cartao/?${sublote}`);
        setUpInputs(sublote);
    };

    const changeFlash = () => {
        if(flash == Camera.Constants.FlashMode.off) 
            setFlash(Camera.Constants.FlashMode.on);
            else
            setFlash(Camera.Constants.FlashMode.off);
    };

    const changeZoom = (mode) => {
        var _zoom = zoom * 100;
        if(mode == 'out'){
            if(_zoom === 0) return _zoom = 0;
            else _zoom = (_zoom - 10) / 100;
        }
        if(mode == 'in'){
            if(_zoom === 100) _zoom = 1;
            else _zoom = (_zoom + 10) / 100;
        }
        setZoom(_zoom);
    };

    const vincular = async () => {
        setSpinner(true);
        setSpinnerText('Vinculando...');
        const idDispositivo = await Store.get('idDispositivo');
        const response = await api.vincularOperador({
            idDispositivo, 
            numeroCracha: String(cartao), 
            idBoleta: String(sublote)
        });
        if(response){
            Alert.simple({ 
                subtitle: 'Operador vinculado com sucesso',
                accept: () => {
                    setSpinner(false);
                    props.navigation.reset({ index: 0, routes: [{ name: 'Menu' }]});
                }
            });
        } else {
            Alert.simple({ 
                subtitle: 'Ocorreu um erro',
                accept: () => {
                    setSpinner(false);
                }
            });
        }
    };

    useEffect(() => {
        (async () => {
            var validar_nfc = true;
            try {
                if(props.route.params.operador && props.route.params.boleta) validar_nfc = false;
                else validar_nfc = true;
            } catch (e){ validar_nfc = true; }
            if(Platform.OS !== 'ios' && validar_nfc){
                try {
                    await Linking.openURL(`devels://nfc/verificar_versao/1.0.1?http://projects.samuelcarrara.com/devels/devels-nfc-1.0.1.apk`);
                } catch(e){
                    Alert.confirm({
                        title: 'A utilidade de leitura de cartões ainda não foi instalada', 
                        subtitle: 'Deseja instalar o complemento agora?',
                        accept: async () => {
                            await Linking.openURL(`http://projects.samuelcarrara.com/devels/devels-nfc-1.0.1.apk`);
                            setSpinner(false);
                            props.navigation.reset({ index: 0, routes: [{ name: 'Menu' }]});
                        }, 
                        cancel: async () => {
                            setSpinner(false);
                            props.navigation.reset({ index: 0, routes: [{ name: 'Menu' }]});
                        }
                    });
                    return false;
                }
            }
            setSpinner(true);
            setSpinnerText('Aguarde...')
            changeFlash(Camera.Constants.FlashMode.off)
            setZoom(0);
            const { status } = await Camera.requestPermissionsAsync();
            setHasPermission(status === 'granted');
            setSpinner(false);
            setScanned(false);
            if(!validar_nfc){
                var o = props.route.params.operador;
                var b = props.route.params.boleta;
                setSublote(b);
                setCartao(o);
                setUpInputs(b, o);
                setScanned(true);
            }
        })();
    }, []);

    const setUpInputs = (l = '', c = '') => {
        var _inputs = [];
        if(l) 
            _inputs.push({
                accessoryLeft: () => 
                (
                    <TouchableWithoutFeedback>
                        <View style={{ minWidth: 110, backgroundColor: '#293756', paddingHorizontal: 10, paddingTop: 6, paddingBottom: 7, marginLeft: -9, borderTopLeftRadius: 4, borderBottomLeftRadius: 4, marginTop: -6, marginBottom: -6 }}>
                            <Text style={{ fontWeight: 'bold', color: '#fff', textAlign: 'center' }}>Lote</Text>
                        </View>
                    </TouchableWithoutFeedback>
                ),
                style: { marginBottom: 10, alignSelf: 'center' },
                textStyle: { textAlign: 'center', color: '#000', fontSize: 16 },
                value: l,
                disabled: true,
            });
        if(c)
            _inputs.push({
                accessoryLeft: () => 
                (
                    <TouchableWithoutFeedback>
                        <View style={{ minWidth: 110, backgroundColor: '#293756', paddingHorizontal: 10, paddingTop: 6, paddingBottom: 7, marginLeft: -9, borderTopLeftRadius: 4, borderBottomLeftRadius: 4, marginTop: -6, marginBottom: -6 }}>
                            <Text style={{ fontWeight: 'bold', color: '#fff', textAlign: 'center' }}>Operador</Text>
                        </View>
                    </TouchableWithoutFeedback>
                ),
                style: { marginBottom: 10, alignSelf: 'center' },
                textStyle: { textAlign: 'center', color: '#000', fontSize: 16 },
                value: c,
                disabled: true,
            });
        setInputs(_inputs);
    };

    useEffect(() => () => {}, []);

    return (
        <>
            <Spinner
                visible={spinner}
                textContent={spinnerText}
                textStyle={{ color: '#fff'}}
                color="#fff"
                overlayColor="rgba(0, 0, 0, 0.85)"
                hidden={false}
                animation="fade"
            />
            <Form 
                {...props}
                title="Vincular Lote ao Operador"
                menu={false}
                bigContent={hasPermission && !scanned}
                inputs={inputs}
                content={() => (<>
                    {
                        hasPermission ?
                        <>
                            {
                                scanned ?
                                <>
                                    {
                                        sublote && !cartao ?
                                        <TouchableOpacity 
                                            onPress={lerBilhete}
                                            style={{
                                                marginTop: 10,
                                                paddingVertical: 25,
                                                paddingHorizontal: 20,
                                                borderColor: '#EFF1FD',
                                                backgroundColor: '#EFF1FD',
                                                borderWidth: 1.5,
                                                borderRadius: 6,
                                                alignItems: 'flex-start',
                                                flexDirection: 'row'
                                            }}
                                        >
                                            <Image
                                                source={require("../../assets/images/nfc.png")}
                                                resizeMode="contain"
                                                style={{ 
                                                    width: 30, 
                                                    height: 30,
                                                    position: 'absolute',
                                                    right: 15,
                                                    top: 10
                                                }}
                                            />
                                            <Text style={{ 
                                                color: '#000', 
                                                fontSize: 16,
                                                position: 'absolute',
                                                top: 13,
                                                left: 15
                                            }}>
                                                Ler Cartão
                                            </Text>
                                        </TouchableOpacity> : <></>
                                    }
                                    {
                                        cartao && sublote ?
                                        <Button 
                                            style={{
                                                marginTop: 10,
                                                width: 100,
                                                alignSelf: 'flex-end'
                                            }}
                                            status="primary"
                                            size="tiny"
                                            onPress={vincular}
                                        >Vincular</Button> : <></>
                                    }
                                </> 
                                :
                                <Camera
                                    zoom={zoom}
                                    flashMode={flash}
                                    onBarCodeScanned={handleBarCode}
                                    barCodeScannerSettings={{
                                        barCodeTypes: [BarCodeScanner.Constants.BarCodeType.code128]
                                    }}
                                    style={{
                                        ...styles.formScrollContainer, 
                                        ...styles.formScroll,
                                        backgroundColor: 'black',
                                        borderTopLeftRadius: 20,
                                        padding: 0,
                                        height: Dimensions.get("window").height - 120
                                    }}
                                >
                                    <Image
                                        source={require("../../assets/images/qrcode2.png")}
                                        style={{
                                            width: 200,
                                            height: Dimensions.get('window').height - 190,
                                            position: 'absolute',
                                            alignSelf: 'center'
                                        }}
                                        resizeMode="contain"
                                    />
                                    {
                                        zoom ?
                                        <Text style={{
                                            position: 'absolute',
                                            bottom: 80,
                                            alignSelf: 'center',
                                            color: '#fff',
                                            fontSize: 14
                                        }}>{Number(String((zoom)).substr(-1)) + 1} x Zoom</Text> : <></>
                                    }
                                    <TouchableOpacity
                                        onPress={changeFlash}
                                        style={{
                                            alignSelf: 'center',
                                            position: 'absolute',
                                            zIndex: 10,
                                            bottom: 20
                                        }}
                                    >
                                        {
                                            flash == Camera.Constants.FlashMode.off ?
                                            <Image
                                                source={require('../../assets/images/flash-off.png')}
                                                resizeMode="contain"
                                                style={{ 
                                                    width: 64, 
                                                    height: 30,
                                                }}
                                            />
                                            :
                                            <Image
                                                source={require('../../assets/images/flash-on.png')}
                                                resizeMode="contain"
                                                style={{ 
                                                    width: 64, 
                                                    height: 30,
                                                }}
                                            />
                                        }
                                    </TouchableOpacity>
                                    <TouchableOpacity
                                        onPress={() => {changeZoom('out')}}
                                        style={{
                                            alignSelf: 'flex-start',
                                            position: 'absolute',
                                            bottom: 20,
                                            left: 20,
                                            zIndex: 10,
                                        }}
                                    >
                                        <Image
                                            source={require('../../assets/images/minus-button.png')}
                                            resizeMode="contain"
                                            style={{ 
                                                width: 64, 
                                                height: 30,
                                            }}
                                        />
                                    </TouchableOpacity>
                                    <TouchableOpacity
                                        onPress={() => {changeZoom('in')}}
                                        style={{
                                            alignSelf: 'flex-end',
                                            position: 'absolute',
                                            bottom: 20,
                                            right: 20,
                                            zIndex: 10,
                                        }}
                                    >
                                        <Image
                                            source={require('../../assets/images/plus-button.png')}
                                            resizeMode="contain"
                                            style={{ 
                                                width: 64, 
                                                height: 30,
                                            }}
                                        />
                                    </TouchableOpacity>
                                    <View style={{
                                        position: 'absolute',
                                        width: Dimensions.get("window").width,
                                        zIndex: 10,
                                        alignItems: 'center',
                                        alignContent: 'center',
                                        justifyContent: 'center',
                                        top: 30,
                                    }}>
                                        <Text style={{
                                            fontSize: 18,
                                            color: '#fff',
                                            textShadowColor: 'rgba(0, 0, 0, 0.75)',
                                            textShadowOffset: {width: -1, height: 1},
                                            textShadowRadius: 0.1
                                        }}>Vincular Lote ao Operador</Text>
                                    </View>
                                </Camera>
                            }
                        </>
                        :
                        <View style={{
                            backgroundColor: '#e74c3c',
                            padding: 15,
                            borderRadius: 10,
                            width: '100%',
                            shadowColor: "#000",
                            shadowOffset: {
                                width: 0,
                                height: 2,
                            },
                            shadowOpacity: 0.23,
                            shadowRadius: 2.62,
                            elevation: 4,
                        }}>
                            <Text style={{
                                textAlign: 'center',
                                color: 'white',
                                fontSize: 20,
                                textTransform: 'uppercase'
                            }}>
                                Sem Permissão para {'\n'} utilizar a câmera
                            </Text>
                        </View>
                    }
                </>)}
            />
        </>
    );
};