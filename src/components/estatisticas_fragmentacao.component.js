import React, { useEffect, useState } from 'react';
import { 
    ScrollView, View, Text, TouchableOpacity
} from 'react-native';
import Spinner from 'react-native-loading-spinner-overlay';
import * as FeatherIcon from "react-native-feather";
import { Table, Row } from 'react-native-table-component';

import api from '../service/ApiHandler';

import styles from '../utils/CustomStyles';
import Store from '../utils/Store';
import Formater from '../utils/Formater';

import { Form } from './subcomponents/Form';
import { Modal as ModalAPF } from './subcomponents/Modal';

export const EstatisticasFragmentacaoScreen = (props) => {    
    const [spinner, setSpinner] = useState(true);
    const [spinnerText, setSpinnerText] = useState('Obtendo dados...');
    const [contagem, setContagem] = useState('...');
    const [tabela, setTabela] = useState([]);
    const [atualizacao, setAtualizacao] = useState('...');
    const [modalAPF, setModalAPF] = useState(false);
    const [dadosModal, setDadosModal] = useState({});
    
    const widthArray = [100, 120, 130, 120, 100];

    const header = [
        'Cód.',
        'Abertura.',
        'Encerramento',
        'Bilhetes',
        'Visualizar'
    ];

    const abrirAPF = async (row) => {
        setSpinner(true);
        setSpinnerText('Aguarde...');
        const idDispositivo = await Store.get('idDispositivo');
        const response = await api.consulta({
            chave: 'CtdConsultar_ApfSelecao',
            filtro: row.cod,
            idDispositivo
        });
        if(!response || !response.length){
            setDadosModal({ table: [], fragmentacaoId: row.cod });
            setSpinner(false);
            setModalAPF(true);
        } else {
            setDadosModal({ table: response, fragmentacaoId: row.cod });
            setSpinner(false);
            setModalAPF(true);
        }
    };

    useEffect(() => {
        async function obterDados(){
            setSpinnerText('Obtendo dados...');
            setSpinner(true);
            const idDispositivo = await Store.get('idDispositivo');
            const response1 = await api.consulta({
                chave: 'CtdStats_frag',
                idDispositivo
            });
            if(response1) {
                try {
                    setContagem(response1[0].contagemAPFs);
                } catch(e){}
            } else setContagem('...');

            const response2 = await api.consulta({
                chave: 'CtdStats_frag2',
                idDispositivo
            });
            if(response2) {
                setTabela(response2);
            } else setTabela([]);

            setSpinner(false);
            setAtualizacao(Formater.formatarDataHora(new Date()));
        }
        obterDados();
    }, []);

    useEffect(() => () => {}, []);

    return (
        <>
            <Spinner
                visible={spinner}
                textContent={spinnerText}
                textStyle={{ color: '#fff'}}
                color="#fff"
                overlayColor="rgba(0, 0, 0, 0.85)"
                hidden={false}
                animation="fade"
            />
            <ModalAPF
                show={modalAPF} 
                close={setModalAPF} 
                content={() => (
                    <>
                        <View style={{...styles.table, flexDirection: 'column', marginBottom: 15}}>
                            <Text style={{ fontWeight: 'bold', fontSize: 16 }}>ID DA FRAGMENTAÇÃO</Text>
                            <Text style={{ fontSize: 16 }}>{dadosModal.fragmentacaoId}</Text>
                        </View>
                        {
                            (dadosModal.table?dadosModal.table:'').length ?
                            <ScrollView 
                                bounces={false} 
                                showsVerticalScrollIndicator={false} 
                                showsHorizontalScrollIndicator={false} 
                                horizontal={true}
                                vertical={true}
                                style={{
                                    width: '100%',
                                    marginBottom: 5,
                                    borderRadius: 8
                                }}
                            >
                                <Table borderStyle={styles.tableBorderStyle} style={{ 
                                    width: '100%', backgroundColor: 'white' 
                                }}>
                                    <Row 
                                        data={[
                                            'Data',
                                            'Tipo Bilhete',
                                            'Nº Bilhete',
                                            'Usuário',
                                            'Operador'
                                        ]} 
                                        textStyle={styles.tableHeaderText}
                                        style={styles.tableHeader}
                                        widthArr={[90, 100, 100, 150, 150]}
                                    />
                                    {
                                        dadosModal.table.map((row, index) => (
                                            <Row
                                                key={index}
                                                data={[
                                                    row.datA_CANCELAMENTO?row.datA_CANCELAMENTO:'...',
                                                    row.tipO_BILHETE?row.tipO_BILHETE:'...',
                                                    row.n_BILHETE?row.n_BILHETE:'...',
                                                    row.nomE_USUARIO?row.nomE_USUARIO:'...',
                                                    row.operador?row.operador:'...',
                                                ]}
                                                style={[styles.row, index%2 && {backgroundColor: '#EEF5FE'}]}
                                                textStyle={{padding: 10, textAlign: 'center'}}
                                                widthArr={[90, 100, 100, 150, 150]}
                                            />
                                        ))
                                    }
                                </Table>
                            </ScrollView> 
                            : 
                            <View style={{...styles.warningAlert, marginBottom: 30 }}>
                                <Text style={styles.alertText}>
                                    NENHUM DADO FOI LOCALIZADO
                                </Text>
                            </View>
                        }
                    </>
                )}
            />
            <Form 
                {...props}
                title="Estatísticas da Fragmentação"
                menu={true}
                content={() => (<>
                    <View 
                        style={{ 
                            width: '100%',
                            flexDirection: 'row',
                            marginBottom: 10,
                            alignItems: 'center',
                            justifyContent: 'center',
                        }}
                    >
                        <View 
                            style={{
                                ...styles.dashCard,
                                backgroundColor: '#219AFF',
                                width: '100%'
                            }}
                        >
                            <FeatherIcon.Monitor style={styles.dashIcon} />
                            <Text style={styles.dashTitle}>
                                QTDD. APF
                            </Text>
                            <Text style={styles.dashSubtitle}>
                                {contagem}
                            </Text>
                        </View>
                    </View>
                    {
                        tabela.length ?
                        <>
                            <Text style={{ 
                                fontSize: 24, 
                                marginBottom: 15, 
                                marginTop: 10,
                                fontWeight: '600'
                            }}>ÚLTIMAS APFs</Text>
                            <ScrollView 
                                bounces={false} 
                                showsVerticalScrollIndicator={false} 
                                showsHorizontalScrollIndicator={false} 
                                horizontal={true}
                                vertical={true}
                                style={{
                                    width: '100%',
                                    marginBottom: 10
                                }}
                            >
                                <Table borderStyle={styles.tableBorderStyle} style={{
                                    width: '100%'
                                }}>
                                    <Row 
                                        data={header} 
                                        textStyle={styles.tableHeaderText}
                                        style={styles.tableHeader}
                                        widthArr={widthArray}
                                    />
                                    {
                                        tabela.map((row, index) => (
                                            <TouchableOpacity key={index} onPress={() => abrirAPF(row)}>
                                                <Row
                                                    data={[
                                                        row.cod?row.cod:'-', 
                                                        row.abertura?row.abertura:'-',
                                                        row.encerramento?row.encerramento:'-', 
                                                        row.bilhetes,
                                                        <FeatherIcon.Search 
                                                            color="#4E86FB"
                                                            width={18}
                                                            height={18}
                                                            style={{ alignSelf: 'center' }}
                                                        />
                                                    ]}
                                                    style={[styles.row, index%2 && {backgroundColor: '#EEF5FE'}]}
                                                    textStyle={{padding: 10, textAlign: 'center'}}
                                                    widthArr={widthArray}
                                                />
                                            </TouchableOpacity>
                                        ))
                                    }
                                </Table>
                            </ScrollView> 
                            <TouchableOpacity 
                                onPress={() => props.navigation.navigate('ConsultarAPF')}
                                style={styles.link}
                            >
                                <Text style={styles.linkText}>
                                    Ver Todas
                                </Text>
                            </TouchableOpacity>
                        </>
                        : <></>
                    }
                    <View style={{ 
                        width: '100%',
                        flexDirection: 'column',
                        alignItems: 'center',
                        justifyContent: 'center',
                        marginBottom: 10,
                    }}>
                        <View style={styles.simpleCard}>
                            <Text style={styles.simpleCardTitle}>
                                Última Atualização
                            </Text>
                            <Text style={styles.simpleCardSubtitle}>
                                {atualizacao}
                            </Text>
                        </View>
                    </View>
                </>)}
            />
        </>
    );
};