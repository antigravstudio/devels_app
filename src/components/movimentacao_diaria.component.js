import React, { useEffect, useState } from 'react';
import { 
    TouchableWithoutFeedback, ScrollView, TouchableOpacity, 
    Text, View, Keyboard, Dimensions
} from 'react-native';
import { Button } from '@ui-kitten/components';
import Spinner from 'react-native-loading-spinner-overlay';
import * as FeatherIcon from "react-native-feather";
import { Table, Row } from 'react-native-table-component';

import styles from '../utils/CustomStyles';
import Formater from '../utils/Formater';
import Store from '../utils/Store';

import api from '../service/ApiHandler';

import { Form } from './subcomponents/Form';
import { Modal as ModalMovimentacao } from './subcomponents/Modal';
import { Modal as ModalBilhetes } from './subcomponents/Modal';

export const MovimentacaoDiariaScreen = (props) => {    
    const [spinner, setSpinner] = useState(false);
    const [spinnerText, setSpinnerText] = useState('Aguarde...');
    const [modalMovimentacao, setModalMovimentacao] = useState(false);
    const [modalBilhetes, setModalBilhetes] = useState(false);
    const [dados, setDados] = useState(null);
    const [pagina, setPagina] = useState(0);
    const [filtro, setFiltro] = useState('');
    const [dadosModalMovimentacao, setDadosModalMovimentacao] = useState({});
    const [dadosModalBilhetes, setDadosModalBilhetes] = useState({});

    const searchIcon = () => (<TouchableWithoutFeedback><FeatherIcon.Search width={24} height={24} color="#4E86FB" /></TouchableWithoutFeedback>);

    const widthArray = [100, 140, 100, 180, 90, 100];

    const abrirMovimentacao = async (row) => {
        setSpinner(true);
        setSpinnerText('Aguarde...');
        const tipo = await Store.get('perfil');
        const idDispositivo = await Store.get('idDispositivo');
        var chave = 'EncMov_diariaSelecao1';
        switch(tipo){
            case 'Custódia':
            case 'CUSTODIA_CANCELAMENTO':
            case 'CUSTODIA_SUPORTE':
            case 'CUSTODIA_ABASTECIMENTO':
                chave = 'CtdMov_diariaSelecao1';
                break;
        }
        const response = await api.consulta({
            chave,
            filtro: row.operador,
            idDispositivo
        });
        setDadosModalMovimentacao({
            lote: row.lote,
            operador: row.operador,
            saldo: row.saldo,
            table: response
        });
        setSpinner(false);
        setModalMovimentacao(true);
    };

    const abrirBilhete = async (row) => {
        setSpinner(true);
        setSpinnerText('Aguarde...');
        const tipo = await Store.get('perfil');
        const idDispositivo = await Store.get('idDispositivo');
        var chave = 'EncMov_diariaSelecao2';
        switch(tipo){
            case 'Custódia':
            case 'CUSTODIA_CANCELAMENTO':
            case 'CUSTODIA_SUPORTE':
            case 'CUSTODIA_ABASTECIMENTO':
                chave = 'CtdMov_diariaSelecao2';
                break;
        }
        const response = await api.consulta({
            chave,
            filtro: row.lote,
            idDispositivo
        });
        setDadosModalBilhetes({
            lote: row.lote,
            operador: row.operador,
            saldo: row.saldo,
            table: response
        });
        setSpinner(false);
        setModalBilhetes(true);
    };

    const pesquisar = async () => {
        setSpinner(true);
        setSpinnerText('Consultando...');
        const idDispositivo = await Store.get('idDispositivo');
        const tipo = await Store.get('perfil');
        var chave_busca = 'EncMov_diariaBusca';
        var chave_incial = 'EncMov_diariaInicial';
        switch(tipo){
            case 'Custódia':
            case 'CUSTODIA_CANCELAMENTO':
            case 'CUSTODIA_SUPORTE':
            case 'CUSTODIA_ABASTECIMENTO':
                chave_busca = 'CtdMov_diariaBusca';
                chave_incial = 'CtdMov_diariaInicial';
                break;
        }
        const response = await api.consulta({
            chave: filtro ? chave_busca : chave_incial,
            filtro: filtro.toUpperCase(),
            idDispositivo
        });
        if(response) {
            setPagina(0);
            try {
                var paginas = [];
                var i_pagina = 0;
                var i = 0;
                for(var row of response){
                    row.dataCriacao = Formater.formatarData(row.dataCriacao);
                    if(paginas[i_pagina] === undefined) paginas[i_pagina] = [];
                    paginas[i_pagina].push(row);
                    i++;
                    if(i == 5){
                        i = 0;
                        i_pagina++;
                    }
                }
                setDados(paginas);
            } catch(e){
                setDados([]);
            }
            setSpinner(false);
        } else {
            setDados([]);
            setSpinner(false);
        }
    };

    const changeFiltro = (text) => {
        setFiltro(text);
    };

    useEffect(() => {
        pesquisar();
    }, []);

    useEffect(() => () => {}, []);

    return (
        <>
            <ModalMovimentacao
                show={modalMovimentacao} 
                close={setModalMovimentacao} 
                content={() => (
                    <>
                        <View style={{...styles.table, flexDirection: 'column', marginBottom: 15}}>
                            <Text style={{ fontWeight: 'bold', fontSize: 16 }}>OPERADOR</Text>
                            <Text style={{ fontSize: 16 }}>{dadosModalMovimentacao.operador}</Text>
                        </View>
                        <View style={{ flexDirection: 'row', marginBottom: 15 }}>
                            <View style={{
                                ...styles.table, flexDirection: 'column', width: '48%', marginRight: '2%', marginBottom: 0
                            }}>
                                <Text style={{ fontWeight: 'bold', fontSize: 16 }}>Nº LOTE</Text>
                                <Text style={{ fontSize: 16 }}>{dadosModalMovimentacao.lote}</Text>
                            </View>
                            <View style={{
                                ...styles.table, flexDirection: 'column', width: '48%', marginLeft: '2%', marginBottom: 0 
                            }}>
                                <Text style={{ fontWeight: 'bold', fontSize: 16 }}>SALDO</Text>
                                <Text style={{ fontSize: 16 }}>{dadosModalMovimentacao.saldo}</Text>
                            </View>
                        </View>
                        {
                            (dadosModalMovimentacao.table?dadosModalMovimentacao.table:[]).length ?
                            <ScrollView 
                                bounces={false} 
                                showsVerticalScrollIndicator={false} 
                                showsHorizontalScrollIndicator={false} 
                                horizontal={true}
                                vertical={true}
                                style={{
                                    width: '100%',
                                    marginBottom: 5,
                                    borderRadius: 8
                                }}
                            >
                                <Table borderStyle={styles.tableBorderStyle} style={{ 
                                    width: '100%', backgroundColor: 'white' 
                                }}>
                                    <Row 
                                        data={[
                                            'Lote',
                                            'Saldo',
                                            'Operador',
                                            'Visualizar',
                                        ]} 
                                        textStyle={styles.tableHeaderText}
                                        style={styles.tableHeader}
                                        widthArr={[(Dimensions.get('window').width / 4), (Dimensions.get('window').width / 4), (Dimensions.get('window').width / 4), 100]}
                                    />
                                    {
                                        dadosModalMovimentacao.table.map((row, index) => (
                                            <TouchableOpacity 
                                                key={index}
                                                onPress={() => abrirBilhete(row)}
                                            >
                                                <Row
                                                    
                                                    data={[
                                                        row.lote,
                                                        row.saldo,
                                                        row.operador?row.operador:'...',
                                                        <FeatherIcon.Search 
                                                            color="#4E86FB"
                                                            width={18}
                                                            height={18}
                                                            style={{ alignSelf: 'center' }}
                                                        />
                                                    ]}
                                                    style={[styles.row, index%2 && {backgroundColor: '#EEF5FE'}]}
                                                    textStyle={{padding: 10, textAlign: 'center'}}
                                                    widthArr={[(Dimensions.get('window').width / 4), (Dimensions.get('window').width / 4), (Dimensions.get('window').width / 4), (Dimensions.get('window').width / 4)]}
                                                />
                                            </TouchableOpacity>
                                        ))
                                    }
                                </Table>
                            </ScrollView> : <></>
                        }
                    </>
                )}
            />
            <ModalBilhetes
                show={modalBilhetes} 
                close={setModalBilhetes} 
                content={() => (
                    <>
                        <View style={{...styles.table, flexDirection: 'column', marginBottom: 15}}>
                            <Text style={{ fontWeight: 'bold', fontSize: 16 }}>OPERADOR</Text>
                            <Text style={{ fontSize: 16 }}>{dadosModalBilhetes.operador}</Text>
                        </View>
                        <View style={{ flexDirection: 'row', marginBottom: 15 }}>
                            <View style={{
                                ...styles.table, flexDirection: 'column', width: '48%', marginRight: '2%', marginBottom: 0
                            }}>
                                <Text style={{ fontWeight: 'bold', fontSize: 16 }}>Nº LOTE</Text>
                                <Text style={{ fontSize: 16 }}>{dadosModalBilhetes.lote}</Text>
                            </View>
                            <View style={{
                                ...styles.table, flexDirection: 'column', width: '48%', marginLeft: '2%', marginBottom: 0 
                            }}>
                                <Text style={{ fontWeight: 'bold', fontSize: 16 }}>SALDO</Text>
                                <Text style={{ fontSize: 16 }}>{dadosModalBilhetes.saldo}</Text>
                            </View>
                        </View>
                        {
                            (dadosModalBilhetes.table?dadosModalBilhetes.table:[]).length ?
                            <ScrollView 
                                bounces={false} 
                                showsVerticalScrollIndicator={false} 
                                showsHorizontalScrollIndicator={false} 
                                horizontal={true}
                                vertical={true}
                                style={{
                                    width: '100%',
                                    marginBottom: 5,
                                    borderRadius: 8
                                }}
                            >
                                <Table borderStyle={styles.tableBorderStyle} style={{ 
                                    width: '100%', backgroundColor: 'white' 
                                }}>
                                    <Row 
                                        data={[
                                            'CPF',
                                            'Data',
                                            'Nascimento',
                                            'Nº Bilhete',
                                            'Usuário',
                                        ]} 
                                        textStyle={styles.tableHeaderText}
                                        style={styles.tableHeader}
                                        widthArr={[140, 100, 120, 100, 350]}
                                    />
                                    {
                                        dadosModalBilhetes.table.map((row, index) => (
                                            <Row
                                                key={index}
                                                data={[
                                                    row.cpf?Formater.formatarDocumento(row.cpf):'...',
                                                    row.data?row.data:'...',
                                                    row['nasC.']?Formater.formatarData(row['nasC.']):'...',
                                                    row['nº BILHETE']?row['nº BILHETE']:'...',
                                                    row['usuário']?row['usuário']:'...',
                                                ]}
                                                style={[styles.row, index%2 && {backgroundColor: '#EEF5FE'}]}
                                                textStyle={{padding: 10, textAlign: 'center'}}
                                                widthArr={[140, 100, 120, 100, 350]}
                                            />
                                        ))
                                    }
                                </Table>
                            </ScrollView> : <></>
                        }
                    </>
                )}
            />
            <Spinner
                visible={spinner}
                textContent={spinnerText}
                textStyle={{ color: '#fff'}}
                color="#fff"
                overlayColor="rgba(0, 0, 0, 0.85)"
                hidden={false}
                animation="fade"
            />
            <Form 
                {...props}
                title="Movimentação Diária"
                menu={true}
                inputs={[{
                    style: { marginBottom: 10 },
                    accessoryLeft: searchIcon,
                    placeholder: "BUSCA POR OPERADOR",
                    textStyle: { textAlign: 'center' },
                    autoCorrect: false,
                    value: filtro,
                    autoCapitalize: "none",
                    autoCorrect: false,
                    returnKeyType: 'done',
                    autoCapitalize: 'none',
                    onChangeText: changeFiltro,
                    onSubmitEditing: pesquisar
                }]}
                content={() => (<>
                    <Button
                        size="small" 
                        style={{ 
                            marginBottom: 10, width: 100, alignSelf: 'flex-end',
                        }}
                        onPress={() => { pesquisar(); Keyboard.dismiss(); }}
                    >Pesquisar</Button>
                    {
                        dados !== null ?
                        <>
                            {
                                dados.length ?
                                <>
                                    <ScrollView 
                                        bounces={false} 
                                        showsVerticalScrollIndicator={false} 
                                        showsHorizontalScrollIndicator={false} 
                                        horizontal={true}
                                        vertical={true}
                                        style={{
                                            width: '100%',
                                            marginBottom: 5
                                        }}
                                    >
                                        <Table borderStyle={styles.tableBorderStyle} style={{
                                            width: '100%'
                                        }}>
                                            <Row 
                                                data={[
                                                    'Data',
                                                    'Tipo do Bilhete',
                                                    'Lote',
                                                    'Operador',
                                                    'Saldo',
                                                    'Visualizar'
                                                ]} 
                                                textStyle={styles.tableHeaderText}
                                                style={styles.tableHeader}
                                                widthArr={widthArray}
                                            />
                                            {
                                                dados[pagina].map((row, index) => (
                                                    <TouchableOpacity key={index} onPress={() => abrirBilhete(row)}>
                                                        <Row
                                                            data={[
                                                                row.dataCriacao, 
                                                                row.tipo,
                                                                row.lote,
                                                                row.operador,
                                                                row.saldo,
                                                                <FeatherIcon.Search 
                                                                    color="#4E86FB"
                                                                    width={18}
                                                                    height={18}
                                                                    style={{ alignSelf: 'center' }}
                                                                />
                                                            ]}
                                                            style={[styles.row, index%2 && {backgroundColor: '#EEF5FE'}]}
                                                            textStyle={{padding: 10, textAlign: 'center'}}
                                                            widthArr={widthArray}
                                                        />
                                                    </TouchableOpacity>
                                                ))
                                            }
                                        </Table>
                                    </ScrollView>
                                    <View style={styles.pagination}>
                                        {
                                            dados.map((row, index) => (
                                                <TouchableOpacity 
                                                    key={index}
                                                    style={{...styles.paginationDot, backgroundColor: pagina == index ? '#E7EAFF' : 'white'}}
                                                    onPress={() => setPagina(index)}
                                                >
                                                    <Text style={{fontSize: 18}}>{index + 1}</Text>
                                                </TouchableOpacity>
                                            ))
                                        }
                                    </View>
                                </> : <>
                                <View style={{...styles.warningAlert, marginBottom: 30 }}>
                                        <Text style={styles.alertText}>
                                            NENHUM DADO FOI LOCALIZADO
                                        </Text>
                                    </View>
                                </>
                            }
                        </> : <></>
                    }
                </>)}
            />
        </>
    );
};