import React, { useEffect, useState } from 'react';
import { 
    TouchableWithoutFeedback, ScrollView, TouchableOpacity, Text, View, Keyboard, Dimensions
} from 'react-native';
import Spinner from 'react-native-loading-spinner-overlay';
import * as FeatherIcon from "react-native-feather";
import { Table, Row } from 'react-native-table-component';
import { Button } from '@ui-kitten/components';

import styles from '../utils/CustomStyles';
import Formater from '../utils/Formater';
import Store from '../utils/Store';

import api from '../service/ApiHandler';

import { Form } from './subcomponents/Form';
import { Modal as ModalBilhete } from './subcomponents/Modal';

export const EstoqueBilhetesScreen = (props) => {    
    const [spinner, setSpinner] = useState(false);
    const [spinnerText, setSpinnerText] = useState('Aguarde...');
    const [modalBilhete, setModalBilhete] = useState(false);
    const [dados, setDados] = useState(null);
    const [pagina, setPagina] = useState(0);
    const [filtro, setFiltro] = useState('');
    const [tipo, setTipo] = useState('');

    const searchIcon = () => (<TouchableWithoutFeedback><FeatherIcon.Search width={24} height={24} color="#4E86FB" /></TouchableWithoutFeedback>);

    const [widthArray, setWidthArray] = useState([150, 150, 80, 100]);

    const abrirBilhete = () => {
        if(tipo != 'Custódia'){
            setModalBilhete(true);
        }
    };

    const pesquisar = async () => {
        setSpinner(true);
        setSpinnerText('Consultando...');
        const idDispositivo = await Store.get('idDispositivo');
        const response = await api.consulta({
            chave: filtro ? 'CtdEstoque_bilhetesBusca' : 'CtdEstoque_bilhetesInicial',
            filtro,
            idDispositivo
        });
        if(response) {
            setPagina(0);
            try {
                var paginas = [];
                var i_pagina = 0;
                var i = 0;
                for(var row of response){
                    row.dataCriacao = Formater.formatarData(row.dataCriacao);
                    if(paginas[i_pagina] === undefined) paginas[i_pagina] = [];
                    paginas[i_pagina].push(row);
                    i++;
                    if(i == 5){
                        i = 0;
                        i_pagina++;
                    }
                }
                setDados(paginas);
            } catch(e){
                setDados([]);
            }
            setSpinner(false);
        } else {
            setDados([]);
            setSpinner(false);
        }
    };

    const changeFiltro = (text) => {
        setFiltro(text);
    };

    useEffect(() => {
        (async () => {
            const _tipo = await Store.get('perfil');
            setTipo(_tipo);
            switch(_tipo){
                case 'Custódia':
                case 'CUSTODIA_CANCELAMENTO':
                case 'CUSTODIA_SUPORTE':
                case 'CUSTODIA_ABASTECIMENTO':
                    setWidthArray([150, 150, 80, 100]);
                    break;
                default:
                    setWidthArray([(Dimensions.get('window').width / 3) - 5, (Dimensions.get('window').width / 3) - 5, (Dimensions.get('window').width / 3) - 20]);
                    break;
            }
        })();
        
        pesquisar();
    }, []);

    useEffect(() => () => {}, []);

    return (
        <>
            <ModalBilhete
                show={modalBilhete} 
                close={setModalBilhete} 
                content={() => (
                    <>
                        <Text style={{
                            color: 'white',
                            textAlign: 'center',
                            paddingBottom: 20,
                            fontSize: 23
                        }}>DETALHE LOTE</Text>
                        <View style={styles.table}>
                            <View style={styles.tableIcon}>
                                <FeatherIcon.File color="#283757" width={24} height={24} />
                            </View>
                            <View style={{ paddingLeft: 10, flexDirection: 'row' }}>
                                <Text style={styles.tableLabel}>
                                    LOTE {'\n'}
                                    <Text style={styles.tableSublabel}>177293-B</Text>
                                    {'\n'}{'\n'}
                                    RESP. {'\n'}
                                    <Text style={styles.tableSublabel}>Importação</Text>
                                </Text>
                                <Text style={styles.tableLabel}>
                                    TIPO {'\n'}
                                    <Text style={styles.tableSublabel}>Prata - 4K</Text>
                                    {'\n'}{'\n'}
                                    SALDO {'\n'}
                                    <Text style={styles.tableSublabel}>20</Text>
                                </Text>
                                <Text style={styles.tableLabel}>
                                    DATA {'\n'}
                                    <Text style={styles.tableSublabel}>01/11/2020</Text>
                                    {'\n'}{'\n'}
                                    ATENDENTE {'\n'}
                                    <Text style={styles.tableSublabel}>Aline Alves Silva</Text>
                                </Text>
                            </View>
                        </View>
                    </>
                )}
            />
            <Spinner
                visible={spinner}
                textContent={spinnerText}
                textStyle={{ color: '#fff'}}
                color="#fff"
                overlayColor="rgba(0, 0, 0, 0.85)"
                hidden={false}
                animation="fade"
            />
            <Form 
                {...props}
                title="Estoque de Bilhetes"
                menu={true}
                inputs={[{
                    style: { marginBottom: 10 },
                    accessoryLeft: searchIcon,
                    placeholder: "BUSCA POR NOME DA UNIDADE",
                    textStyle: { textAlign: 'center' },
                    autoCorrect: false,
                    defautValue: filtro,
                    autoCapitalize: "none",
                    autoCorrect: false,
                    returnKeyType: 'done',
                    autoCapitalize: 'none',
                    onChangeText: changeFiltro,
                    onSubmitEditing: pesquisar
                }]}
                content={() => (<>
                    <Button
                        size="small" 
                        style={{ 
                            marginBottom: 10, width: 100, alignSelf: 'flex-end',
                        }}
                        onPress={() => { pesquisar(); Keyboard.dismiss(); }}
                    >Pesquisar</Button>
                    {
                        dados !== null ?
                        <>
                            {
                                dados.length ?
                                <>
                                    <ScrollView 
                                        bounces={false} 
                                        showsVerticalScrollIndicator={false} 
                                        showsHorizontalScrollIndicator={false} 
                                        horizontal={true}
                                        vertical={true}
                                        style={{
                                            width: '100%',
                                            marginBottom: 5
                                        }}
                                    >
                                        <Table borderStyle={styles.tableBorderStyle} style={{
                                            width: '100%'
                                        }}>
                                            <Row 
                                                data={[
                                                    'Unidade',
                                                    'Tipo',
                                                    'Saldo',
                                                    tipo != 'Custódia' ? 'Visualizar' : '',
                                                ]} 
                                                textStyle={styles.tableHeaderText}
                                                style={styles.tableHeader}
                                                widthArr={widthArray}
                                            />
                                            {
                                                dados[pagina].map((row, index) => (
                                                    <>
                                                        <Row
                                                            data={[
                                                                row.posto, 
                                                                row.tipo,
                                                                row.saldo,
                                                                tipo != 'Custódia' ?
                                                                <FeatherIcon.Search 
                                                                    color="#4E86FB"
                                                                    width={18}
                                                                    height={18}
                                                                    style={{ alignSelf: 'center' }}
                                                                /> : <></>
                                                            ]}
                                                            style={[styles.row, index%2 && {backgroundColor: '#EEF5FE'}]}
                                                            textStyle={{padding: 10, textAlign: 'center'}}
                                                            widthArr={widthArray}
                                                        />
                                                        {/*<TouchableOpacity key={index} onPress={() => abrirBilhete()}>
                                                            <Row
                                                                data={[
                                                                    row.posto, 
                                                                    row.tipo,
                                                                    row.saldo,
                                                                    tipo != 'Custódia' ?
                                                                    <FeatherIcon.Search 
                                                                        color="#4E86FB"
                                                                        width={18}
                                                                        height={18}
                                                                        style={{ alignSelf: 'center' }}
                                                                    /> : <></>
                                                                ]}
                                                                style={[styles.row, index%2 && {backgroundColor: '#EEF5FE'}]}
                                                                textStyle={{padding: 10, textAlign: 'center'}}
                                                                widthArr={widthArray}
                                                            />
                                                        </TouchableOpacity>*/}
                                                    </>
                                                ))
                                            }
                                        </Table>
                                    </ScrollView>
                                    <View style={styles.pagination}>
                                        {
                                            dados.map((row, index) => (
                                                <TouchableOpacity 
                                                    key={index}
                                                    style={{...styles.paginationDot, backgroundColor: pagina == index ? '#E7EAFF' : 'white'}}
                                                    onPress={() => setPagina(index)}
                                                >
                                                    <Text style={{fontSize: 18}}>{index + 1}</Text>
                                                </TouchableOpacity>
                                            ))
                                        }
                                    </View>
                                </> : <>
                                <View style={{...styles.warningAlert, marginBottom: 30 }}>
                                        <Text style={styles.alertText}>
                                            NENHUM DADO FOI LOCALIZADO
                                        </Text>
                                    </View>
                                </>
                            }
                        </> : <></>
                    }
                </>)}
            />
        </>
    );
};