import React, { useEffect, useState } from 'react';
import { 
    View, Text, Dimensions, Image, TouchableOpacity
} from 'react-native';
import { BarCodeScanner } from 'expo-barcode-scanner';
import { Camera } from 'expo-camera';
import * as FeatherIcon from "react-native-feather";

import { Form } from './subcomponents/Form';

import styles from '../utils/CustomStyles';
import Store from '../utils/Store';


export const BarCodeScreen = (props) => {    
    const [hasPermission, setHasPermission] = useState(null);
    const [scanned, setScanned] = useState(false);
    const [zoom, setZoom] = useState(0);
    const [flash, setFlash] = useState(null);

    const handleBarCode = async (resultado) => {
        if(scanned) return false;
        if(resultado){
            setScanned(true);
            await Store.set('leitura-boleta', String(resultado.data));
            props.navigation.goBack();
            setScanned(false);
        }
    };

    const changeFlash = () => {
        if(flash == Camera.Constants.FlashMode.off) 
            setFlash(Camera.Constants.FlashMode.on);
            else
            setFlash(Camera.Constants.FlashMode.off);
    };

    const changeZoom = (mode) => {
        var _zoom = zoom * 100;
        if(mode == 'out'){
            if(_zoom === 0) return _zoom = 0;
            else _zoom = (_zoom - 10) / 100;
        }
        if(mode == 'in'){
            if(_zoom === 100) _zoom = 1;
            else _zoom = (_zoom + 10) / 100;
        }
        setZoom(_zoom);
    };
    

    useEffect(() => {
        (async () => {
            changeFlash(Camera.Constants.FlashMode.off)
            setZoom(0);
            await Store.remove('leitura-boleta');
            const { status } = await Camera.requestPermissionsAsync();
            setHasPermission(status === 'granted');
            setScanned(false);
        })();
    }, []);

    useEffect(() => () => {}, []);

    return (
        <>
            <Form 
                {...props}
                title="Ler Código de Barras"
                menu={false}
                bigContent={hasPermission}
                content={() => (<>
                    {
                        hasPermission ?
                        <Camera
                            zoom={zoom}
                            flashMode={flash}
                            onBarCodeScanned={handleBarCode}
                            barCodeScannerSettings={{
                                barCodeTypes: [BarCodeScanner.Constants.BarCodeType.code128]
                            }}
                            style={{
                                ...styles.formScrollContainer, 
                                ...styles.formScroll,
                                backgroundColor: 'black',
                                borderTopLeftRadius: 20,
                                padding: 0,
                                height: Dimensions.get("window").height - 120
                            }}
                        >
                            <Image
                                source={require("../../assets/images/qrcode2.png")}
                                style={{
                                    width: 200,
                                    height: Dimensions.get('window').height - 190,
                                    position: 'absolute',
                                    alignSelf: 'center'
                                }}
                                resizeMode="contain"
                            />
                            {
                                zoom ?
                                <Text style={{
                                    position: 'absolute',
                                    bottom: 80,
                                    alignSelf: 'center',
                                    color: '#fff',
                                    fontSize: 14
                                }}>{Number(String((zoom)).substr(-1)) + 1} x Zoom</Text> : <></>
                            }
                            <TouchableOpacity
                                onPress={changeFlash}
                                style={{
                                    alignSelf: 'center',
                                    position: 'absolute',
                                    zIndex: 10,
                                    bottom: 20
                                }}
                            >
                                {
                                    flash == Camera.Constants.FlashMode.off ?
                                    <Image
                                        source={require('../../assets/images/flash-off.png')}
                                        resizeMode="contain"
                                        style={{ 
                                            width: 64, 
                                            height: 30,
                                        }}
                                    />
                                    :
                                    <Image
                                        source={require('../../assets/images/flash-on.png')}
                                        resizeMode="contain"
                                        style={{ 
                                            width: 64, 
                                            height: 30,
                                        }}
                                    />
                                }
                            </TouchableOpacity>
                            <TouchableOpacity
                                onPress={() => {changeZoom('out')}}
                                style={{
                                    alignSelf: 'flex-start',
                                    position: 'absolute',
                                    bottom: 20,
                                    left: 20,
                                    zIndex: 10,
                                }}
                            >
                                <Image
                                    source={require('../../assets/images/minus-button.png')}
                                    resizeMode="contain"
                                    style={{ 
                                        width: 64, 
                                        height: 30,
                                    }}
                                />
                            </TouchableOpacity>
                            <TouchableOpacity
                                onPress={() => {changeZoom('in')}}
                                style={{
                                    alignSelf: 'flex-end',
                                    position: 'absolute',
                                    bottom: 20,
                                    right: 20,
                                    zIndex: 10,
                                }}
                            >
                                <Image
                                    source={require('../../assets/images/plus-button.png')}
                                    resizeMode="contain"
                                    style={{ 
                                        width: 64, 
                                        height: 30,
                                    }}
                                />
                            </TouchableOpacity>
                        </Camera>
                        :
                        <View style={{
                            backgroundColor: '#e74c3c',
                            padding: 15,
                            borderRadius: 10,
                            width: '100%',
                            shadowColor: "#000",
                            shadowOffset: {
                                width: 0,
                                height: 2,
                            },
                            shadowOpacity: 0.23,
                            shadowRadius: 2.62,
                            elevation: 4,
                        }}>
                            <Text style={{
                                textAlign: 'center',
                                color: 'white',
                                fontSize: 20,
                                textTransform: 'uppercase'
                            }}>
                                Sem Permissão para {'\n'} utilizar a câmera
                            </Text>
                        </View>
                    }
                </>)}
            />
        </>
    );
};