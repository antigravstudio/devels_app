import React, { useEffect, useState } from 'react';
import { 
    View, Text, TouchableOpacity
} from 'react-native';
import Spinner from 'react-native-loading-spinner-overlay';
import * as FeatherIcon from "react-native-feather";

import styles from '../utils/CustomStyles';
import Store from '../utils/Store';

import api from '../service/ApiHandler';

import { Form } from './subcomponents/Form';

export const HistoricoLeiturasScreen = (props) => {    
    const [spinner, setSpinner] = useState(true);
    const [spinnerText, setSpinnerText] = useState('Aguarde...');
    const [dados, setDados] = useState(null);
    const [pagina, setPagina] = useState(0);

    useEffect(() => {
        setSpinnerText('Aguarde...');
        setSpinner(true);
        (async function () {
            const tipo = await Store.get('perfil');
            var chave = 'Historico_leituras';
            switch(tipo) {
                case 'Encarregado':
                case 'ENCARREGADO':
                case 'ZELADORIA':
                case 'AGENTE':
                    chave = 'Enc_Historico_leituras'; 
                break;
                case 'Custódia':
                case 'CUSTODIA_CANCELAMENTO':
                case 'CUSTODIA_SUPORTE':
                case 'CUSTODIA_ABASTECIMENTO':
                    chave = 'Ctd_Historico_leituras'; 
                break;
                case 'Transportadora': 
                    chave = 'Tpd_Historico_leituras'; 
                break;
                case 'SPTrans':
                case 'SPTRANS':
                    chave = 'Spt_Historico_Leituras'; 
                break;
            }
            const idDispositivo = await Store.get('idDispositivo');
            const response = await api.consulta({
                chave,
                idDispositivo
            });
            if(response) {
                setPagina(0);
                try {
                    var paginas = [];
                    var i_pagina = 0;
                    var i = 0;
                    for(var row of response){
                        //row.dataCriacao = Formater.formatarData(row.dataCriacao);
                        if(paginas[i_pagina] === undefined) paginas[i_pagina] = [];
                        paginas[i_pagina].push(row);
                        i++;
                        if(i == 5){
                            i = 0;
                            i_pagina++;
                        }
                    }
                    setDados(paginas);
                } catch(e){
                    setDados([]);
                }
                setSpinner(false);
            } else {
                setDados([]);
                setSpinner(false);
            }
        })();
    }, []);

    useEffect(() => () => {}, []);

    return (
        <>
            <Spinner
                visible={spinner}
                textContent={spinnerText}
                textStyle={{ color: '#fff'}}
                color="#fff"
                overlayColor="rgba(0, 0, 0, 0.85)"
                hidden={false}
                animation="fade"
            />
            <Form 
                {...props}
                title="Histórico de Leituras"
                menu={true}
                content={() => (<>
                    {
                        dados != null ?
                        <>
                            {
                                dados.length ?
                                <>
                                    {
                                        dados[pagina].map((row, index) => (
                                            <View style={{...styles.table, marginBottom: 10}} key={index}>
                                                <View style={styles.tableIcon}>
                                                    <FeatherIcon.File color="#283757" width={24} height={24} />
                                                </View>
                                                <View style={{ paddingLeft: 10, flexDirection: 'row' }}>
                                                    <Text style={styles.tableLabel}>
                                                        DATA {'\n'}
                                                        <Text style={styles.tableSublabel}>{row.datA_LEITURA}</Text>
                                                        {'\n'}{'\n'}
                                                        MOVIMENTO {'\n'}
                                                        <Text style={styles.tableSublabel}>{row.movimento}</Text>
                                                    </Text>
                                                    <Text style={styles.tableLabel}>
                                                        ETAPA {'\n'}
                                                        <Text style={styles.tableSublabel}>{row.etapa}</Text>
                                                        {'\n'}{'\n'}
                                                        ID {'\n'}
                                                        <Text style={styles.tableSublabel}>{row.entidadE_ID?row.entidadE_ID:'...'}</Text>
                                                    </Text>
                                                </View>
                                            </View>
                                        ))
                                    }
                                    <View style={styles.pagination}>
                                        {
                                            dados.map((row, index) => (
                                                <TouchableOpacity 
                                                    key={index}
                                                    style={{...styles.paginationDot, backgroundColor: pagina == index ? '#E7EAFF' : 'white'}}
                                                    onPress={() => setPagina(index)}
                                                >
                                                    <Text style={{fontSize: 18}}>{index + 1}</Text>
                                                </TouchableOpacity>
                                            ))
                                        }
                                    </View>
                                </> 
                                :
                                <View style={{...styles.warningAlert, marginBottom: 30 }}>
                                    <Text style={styles.alertText}>
                                        NENHUM DADO FOI LOCALIZADO
                                    </Text>
                                </View> 
                            }
                        </> : 
                        <></>
                    }
                </>)}
            />
        </>
    );
};