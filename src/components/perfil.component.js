import React, { useEffect, useState } from 'react';
import { Text, TouchableOpacity } from 'react-native';
import Spinner from 'react-native-loading-spinner-overlay';
import * as FeatherIcon from "react-native-feather";
import * as AuthSession from 'expo-auth-session';
import * as Permissions from 'expo-permissions';
import * as WebBrowser from 'expo-web-browser';
import * as Location from 'expo-location';
import * as Updates from 'expo-updates';
import * as Device from 'expo-device';

import Axios from 'axios';

import Alert from '../utils/Alert';
import Store from '../utils/Store';

import styles from '../utils/CustomStyles';
import { Form } from './subcomponents/Form';

import api from '../service/ApiHandler';

WebBrowser.maybeCompleteAuthSession();
const useProxy = true;
const redirectUri = AuthSession.makeRedirectUri({ useProxy });

const slug = require('../../app.json').expo.slug;

export const PerfilScreen = (props) => {  
    const [spinner, setSpinner] = useState(false);
    const [spinnerText, setSpinnerText] = useState('Aguarde...');

    var url = 'https://id.lyli.com.br/auth/realms/Lyli';

    const discovery = AuthSession.useAutoDiscovery(url);

    const acessarPerfil = async (tipo) => {
        props.navigation.replace('Login', { perfil: tipo });
    };

    const [request, result, promptAsync] = AuthSession.useAuthRequest(
        {
            redirectUri,
            clientId: 'Athens',
            clientSecret: 'QRNDottBNZkpUdT8l9WuG92sN5TKTBxm',
            scopes: ["openid", "role", "profile", "email", "id_unit"],
            usePKCE: false
        },
        discovery
    );

    const login = async () => {
        try {
            const p = await promptAsync({ useProxy });
            if(p.type == 'success'){
                setSpinner(true);
                var data = new FormData();
                data.append('grant_type', 'authorization_code');
                data.append('client_id', 'clientApp');
                data.append('client_secret', '2FBFB527-7BDB-46F6-83B1-84CA94D96294');
                data.append('code', p.params.code);
                data.append('redirect_uri', redirectUri);
                data.append('scope', "openid role profile email id_unit");
                data.append('use_pkce', "false");
                const u = await Axios({
                    url: discovery.tokenEndpoint,
                    method: 'POST',
                    headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                    data
                });
                const a = await AuthSession.fetchUserInfoAsync({ accessToken: u.data.access_token }, discovery);
                if(a){
                    var location = '';
                    try {
                        let { status } = await Location.requestPermissionsAsync();
                        if (status !== 'granted') {
                            location = {
                                coords: {
                                    latitude: '',
                                    longitude: ''
                                }
                            };
                        } else {
                            setSpinnerText('Obtendo localização...');
                            location = await Location.getCurrentPositionAsync({});
                        }
                    } catch(e){
                        console.log(e);
                        location = {
                            coords: {
                                latitude: '',
                                longitude: ''
                            }
                        };
                    }
                    try {
                        const { status, permissions } = await Permissions.askAsync(Permissions.LOCATION);
                        if (status !== 'granted') {
                            location = await Location.getCurrentPositionAsync({ enableHighAccuracy: true });
                        }
                    } catch(e){}
                    setSpinnerText('Aguarde...');
                    const idDispositivo = await api.primeiroLogin({
                        nomeDispositivo: Device.deviceName,
                        fabricante: Device.manufacturer,
                        plataforma: Device.osName,
                        versao: Device.osVersion,
                        modelo: Device.modelName,
                        celular: null,
                        login: String(a.email),
                        senha: '',
                        perfil: String(a.role),
                        ip: null,
                        lat: location.coords.latitude,
                        lon: location.coords.longitude,
                        unidadeId: String(a.id_unit)
                    });
                    if(idDispositivo == 'nao_autorizado'){
                        Alert.simple({ 
                            subtitle: 'Dados de Acesso invalidos',
                            accept: () => {
                                setSpinner(false);
                            }
                        });
                    } else if(idDispositivo){
                        await Store.set('idDispositivo', idDispositivo);
                        await Store.set('perfil', String(a.role));
                        await Updates.reloadAsync();
                    }  else {
                        Alert.simple({ 
                            subtitle: 'Erro de conexão',
                            accept: () => {
                                setSpinner(false);
                            }
                        });
                    }
                } else {
                    Alert.simple({ 
                        subtitle: 'Erro de conexão',
                        accept: () => {
                            setSpinner(false);
                        }
                    });
                }
            }
        } catch (e){
            Alert.simple({ 
                subtitle: 'Erro de conexão',
                accept: () => {
                    setSpinner(false);
                }
            });
            console.log(e);
        }
    };

    useEffect(() => () => {
        setSpinner(false);
    }, []);

    return (
        <>
            <Spinner
                visible={spinner}
                textContent={spinnerText}
                textStyle={{ color: '#fff'}}
                color="#fff"
                overlayColor="rgba(0, 0, 0, 0.85)"
                hidden={false}
                animation="fade"
            />
            <Form 
                {...props}
                title="Qual perfil deseja acessar?"
                simple={true}
                content={() => (<>
                    <TouchableOpacity 
                        onPress={() => acessarPerfil('Transportadora')}
                        style={styles.link}
                    >
                        <FeatherIcon.User 
                            color="#283757" 
                            height={22} width={22} 
                            style={styles.linkIcon}
                        />
                        <Text style={styles.linkText}>
                            Trasportadora
                        </Text>
                    </TouchableOpacity>
                    <TouchableOpacity 
                        onPress={() => acessarPerfil('Outros')}
                        style={styles.link}
                    >
                        <FeatherIcon.User 
                            color="#283757" 
                            height={22} width={22} 
                            style={styles.linkIcon}
                        />
                        <Text style={styles.linkText}>
                            Outros
                        </Text>
                    </TouchableOpacity>
                </>)}
            />
        </>
    );
};