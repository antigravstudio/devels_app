import React, { useEffect, useState } from 'react';
import { 
    View, Text, Dimensions, Image, TouchableOpacity
} from 'react-native';
import Spinner from 'react-native-loading-spinner-overlay';
import { BarCodeScanner } from 'expo-barcode-scanner';
import * as Permissions from 'expo-permissions';
import * as Location from 'expo-location';
import * as Network from 'expo-network';
import { Camera } from 'expo-camera';

import { Form } from './subcomponents/Form';
import api from '../service/ApiHandler';
import Store from '../utils/Store';
import Alert from '../utils/Alert';

import styles from '../utils/CustomStyles';

export const QRCodeScreen = (props) => {    
    const [spinner, setSpinner] = useState(true);
    const [spinnerText, setSpinnerText] = useState('Aguarde...');
    const [hasPermission, setHasPermission] = useState(null);
    const [scanned, setScanned] = useState(false);
    const [zoom, setZoom] = useState(0);
    const [flash, setFlash] = useState(null);

    const changeFlash = () => {
        if(flash == Camera.Constants.FlashMode.off) 
            setFlash(Camera.Constants.FlashMode.on);
            else
            setFlash(Camera.Constants.FlashMode.off);
    };

    const changeZoom = (mode) => {
        var _zoom = zoom * 100;
        if(mode == 'out'){
            if(_zoom === 0) return _zoom = 0;
            else _zoom = (_zoom - 10) / 100;
        }
        if(mode == 'in'){
            if(_zoom === 100) _zoom = 1;
            else _zoom = (_zoom + 10) / 100;
        }
        setZoom(_zoom);
    };

    const handleQRCode = async (resultado) => {
        if(scanned) return false;
        setScanned(true);
        setSpinnerText('Validando dados...');
        setSpinner(true);
        var location = '';
        try {
            let { status } = await Location.requestPermissionsAsync();
            if (status !== 'granted') {
                location = {
                    coords: {
                        latitude: '',
                        longitude: ''
                    }
                };
            } else {
                location = await Location.getCurrentPositionAsync({});
            }
        } catch(e){
            console.log(e);
            location = {
                coords: {
                    latitude: '',
                    longitude: ''
                }
            };
        }
        try {
            const { status, permissions } = await Permissions.askAsync(Permissions.LOCATION);
            if (status !== 'granted') {
                location = await Location.getCurrentPositionAsync({ enableHighAccuracy: true });
            }
        } catch(e){
            console.log(e);
        }
        console.log(location);
        const ip = await Network.getIpAddressAsync();
        const idDispositivo = await Store.get('idDispositivo');
        const response = await api.leituraQRCode({ 
            idDispositivo, ip, 
            textoLido: resultado.data,
            lat: location.coords.latitude,
            lon: location.coords.longitude
        })
        console.log(response);
        if(response.status == 200){
            Alert.simple({ 
                subtitle: response.data,
                accept: () => {
                    setSpinner(false);
                    setScanned(false);
                    props.navigation.reset({ index: 0, routes: [{ name: 'Menu' }] });
                }
            });
        } else {
            Alert.simple({ 
                subtitle: response.data ? response.data : 'Ocorreu um erro',
                accept: () => {
                    setSpinner(false);
                    setScanned(false);
                }
            });
        }
    };

    useEffect(() => {
        (async () => {
            changeFlash(Camera.Constants.FlashMode.off)
            setZoom(0);
            const { status } = await BarCodeScanner.requestPermissionsAsync();
            setHasPermission(status === 'granted');
            setSpinner(false);
            setScanned(false);
        })();
    }, []);

    useEffect(() => () => {}, []);

    return (
        <>
            <Spinner
                visible={spinner}
                textContent={spinnerText}
                textStyle={{ color: '#fff'}}
                color="#fff"
                overlayColor="rgba(0, 0, 0, 0.85)"
                hidden={false}
                animation="fade"
            />
            <Form 
                {...props}
                title="Leitura de QR Code"
                menu={false}
                bigContent={hasPermission}
                content={() => (<>
                    {
                        hasPermission ?
                        <Camera
                            zoom={zoom}
                            flashMode={flash}
                            onBarCodeScanned={handleQRCode}
                            barCodeScannerSettings={{
                                barCodeTypes: [BarCodeScanner.Constants.BarCodeType.qr]
                            }}
                            style={{
                                ...styles.formScrollContainer, 
                                ...styles.formScroll,
                                backgroundColor: 'black',
                                borderTopLeftRadius: 20,
                                padding: 0,
                                height: Dimensions.get("window").height - 80
                            }}
                        >
                            <Image
                                source={require("../../assets/images/qrcode.png")}
                                style={{
                                    width: '100%',
                                    height: Dimensions.get('window').height - 190
                                }}
                                resizeMode="contain"
                            />
                            {
                                zoom ?
                                <Text style={{
                                    position: 'absolute',
                                    bottom: 140,
                                    alignSelf: 'center',
                                    color: '#fff',
                                    fontSize: 16
                                }}>{Number(String((zoom)).substr(-1)) + 1} x Zoom</Text> : <></>
                            }
                            <TouchableOpacity
                                onPress={changeFlash}
                                style={{
                                    alignSelf: 'center',
                                    position: 'absolute',
                                    zIndex: 10,
                                    bottom: 80
                                }}
                            >
                                {
                                    flash == Camera.Constants.FlashMode.off ?
                                    <Image
                                        source={require('../../assets/images/flash-off.png')}
                                        resizeMode="contain"
                                        style={{ 
                                            width: 64, 
                                            height: 30,
                                        }}
                                    />
                                    :
                                    <Image
                                        source={require('../../assets/images/flash-on.png')}
                                        resizeMode="contain"
                                        style={{ 
                                            width: 64, 
                                            height: 30,
                                        }}
                                    />
                                }
                            </TouchableOpacity>
                            <TouchableOpacity
                                onPress={() => {changeZoom('out')}}
                                style={{
                                    alignSelf: 'flex-start',
                                    position: 'absolute',
                                    bottom: 80,
                                    left: 20,
                                    zIndex: 50,
                                }}
                            >
                                <Image
                                    source={require('../../assets/images/minus-button.png')}
                                    resizeMode="contain"
                                    style={{ 
                                        width: 64, 
                                        height: 30,
                                    }}
                                />
                            </TouchableOpacity>
                            <TouchableOpacity
                                onPress={() => {changeZoom('in')}}
                                style={{
                                    alignSelf: 'flex-end',
                                    position: 'absolute',
                                    bottom: 80,
                                    right: 20,
                                    zIndex: 50,
                                }}
                            >
                                <Image
                                    source={require('../../assets/images/plus-button.png')}
                                    resizeMode="contain"
                                    style={{ 
                                        width: 64, 
                                        height: 30,
                                    }}
                                />
                            </TouchableOpacity>
                            <View style={{
                                position: 'absolute',
                                width: Dimensions.get("window").width,
                                zIndex: 10,
                                alignItems: 'center',
                                alignContent: 'center',
                                justifyContent: 'center',
                                top: 30,
                            }}>
                                <Text style={{
                                    fontSize: 18,
                                    color: '#fff',
                                    textShadowColor: 'rgba(0, 0, 0, 0.75)',
                                    textShadowOffset: {width: -1, height: 1},
                                    textShadowRadius: 0.1
                                }}>Leitura de QR Code</Text>
                            </View>
                        </Camera>
                        :
                        <View style={{
                            backgroundColor: '#e74c3c',
                            padding: 15,
                            borderRadius: 10,
                            width: '100%',
                            shadowColor: "#000",
                            shadowOffset: {
                                width: 0,
                                height: 2,
                            },
                            shadowOpacity: 0.23,
                            shadowRadius: 2.62,
                            elevation: 4,
                        }}>
                            <Text style={{
                                textAlign: 'center',
                                color: 'white',
                                fontSize: 20,
                                textTransform: 'uppercase'
                            }}>
                                Sem Permissão para {'\n'} utilizar a câmera
                            </Text>
                        </View>
                    }
                </>)}
            />
        </>
    );
};