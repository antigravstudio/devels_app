import React, { useEffect, useState } from 'react';
import { 
    TouchableWithoutFeedback, ScrollView, TouchableOpacity, Text, View, Keyboard
} from 'react-native';
import { Button } from '@ui-kitten/components';
import Spinner from 'react-native-loading-spinner-overlay';
import * as FeatherIcon from "react-native-feather";
import { Table, Row } from 'react-native-table-component';

import styles from '../utils/CustomStyles';
import Formater from '../utils/Formater';
import Store from '../utils/Store';

import api from '../service/ApiHandler';

import { Form } from './subcomponents/Form';
import { Modal as ModalAPF } from './subcomponents/Modal';

export const ConsultarAPFScreen = (props) => {    
    const [spinner, setSpinner] = useState(false);
    const [spinnerText, setSpinnerText] = useState('Aguarde...');
    const [modalAPF, setModalAPF] = useState(false);
    const [dados, setDados] = useState(null);
    const [pagina, setPagina] = useState(0);
    const [filtro, setFiltro] = useState('');
    const [dadosModal, setDadosModal] = useState({});

    const searchIcon = () => (<TouchableWithoutFeedback><FeatherIcon.Search width={24} height={24} color="#4E86FB" /></TouchableWithoutFeedback>);

    const widthArray = [90, 120, 130, 100, 100];

    const abrirAPF = async (row) => {
        setSpinner(true);
        setSpinnerText('Aguarde...');
        const idDispositivo = await Store.get('idDispositivo');
        const response = await api.consulta({
            chave: 'CtdConsultar_ApfSelecao',
            filtro: row.cod,
            idDispositivo
        });
        if(!response || !response.length){
            setDadosModal({ table: [], fragmentacaoId: row.cod });
            setModalAPF(true);
            setSpinner(false);
        } else {
            setDadosModal({ table: response, fragmentacaoId: row.cod });
            setModalAPF(true);
            setSpinner(false);
        }
    };

    const pesquisar = async () => {
        setSpinner(true);
        setSpinnerText('Consultando...');
        const idDispositivo = await Store.get('idDispositivo');
        const response = await api.consulta({
            chave: filtro ? 'CtdConsultar_ApfBusca' : 'CtdConsultar_ApfInicial',
            filtro: filtro.toUpperCase(),
            idDispositivo
        });
        if(response) {
            setPagina(0);
            try {
                var paginas = [];
                var i_pagina = 0;
                var i = 0;
                for(var row of response){
                    row.dataCriacao = Formater.formatarData(row.dataCriacao);
                    if(paginas[i_pagina] === undefined) paginas[i_pagina] = [];
                    paginas[i_pagina].push(row);
                    i++;
                    if(i == 5){
                        i = 0;
                        i_pagina++;
                    }
                }
                setDados(paginas);
            } catch(e){
                setDados([]);
            }
            setSpinner(false);
        } else {
            setDados([]);
            setSpinner(false);
        }
    };

    const changeFiltro = (text) => {
        setFiltro(text);
    };

    useEffect(() => {
        pesquisar();
    }, []);

    useEffect(() => () => {}, []);

    return (
        <>
            <ModalAPF
                show={modalAPF} 
                close={setModalAPF} 
                content={() => (
                    <>
                        <View style={{...styles.table, flexDirection: 'column', marginBottom: 15}}>
                            <Text style={{ fontWeight: 'bold', fontSize: 16 }}>ID DA FRAGMENTAÇÃO</Text>
                            <Text style={{ fontSize: 16 }}>{dadosModal.fragmentacaoId}</Text>
                        </View>
                        {
                            (dadosModal.table?dadosModal.table:[]).length ?
                            <ScrollView 
                                bounces={false} 
                                showsVerticalScrollIndicator={false} 
                                showsHorizontalScrollIndicator={false} 
                                horizontal={true}
                                vertical={true}
                                style={{
                                    width: '100%',
                                    marginBottom: 5
                                }}
                            >
                                <Table borderStyle={styles.tableBorderStyle} style={{ 
                                    width: '100%', backgroundColor: 'white' 
                                }}>
                                    <Row 
                                        data={[
                                            'Data',
                                            'Tipo Bilhete',
                                            'Nº Bilhete',
                                            'Usuário',
                                            'Operador',
                                        ]} 
                                        textStyle={styles.tableHeaderText}
                                        style={styles.tableHeader}
                                        widthArr={[90, 200, 100, 200, 200]}
                                    />
                                    {
                                        dadosModal.table.map((row, index) => (
                                            <Row
                                                key={index}
                                                data={[
                                                    row.datA_CANCELAMENTO?row.datA_CANCELAMENTO:'...',
                                                    row.tipO_BILHETE?row.tipO_BILHETE:'...',
                                                    row.n_BILHETE?row.n_BILHETE:'...',
                                                    row.nomE_USUARIO?row.nomE_USUARIO:'...',
                                                    row.operador?row.operador:'...',
                                                ]}
                                                style={[styles.row, index%2 && {backgroundColor: '#EEF5FE'}]}
                                                textStyle={{padding: 10, textAlign: 'center'}}
                                                widthArr={[90, 200, 100, 200, 200]}
                                            />
                                        ))
                                    }
                                </Table>
                            </ScrollView> 
                            : 
                            <View style={{...styles.warningAlert, marginBottom: 30 }}>
                                <Text style={styles.alertText}>
                                    NENHUM DADO FOI LOCALIZADO
                                </Text>
                            </View>
                        }
                    </>
                )}
            />
            <Spinner
                visible={spinner}
                textContent={spinnerText}
                textStyle={{ color: '#fff'}}
                color="#fff"
                overlayColor="rgba(0, 0, 0, 0.85)"
                hidden={false}
                animation="fade"
            />
            <Form 
                {...props}
                title="APF"
                menu={true}
                inputs={[{
                    style: { marginBottom: 10 },
                    accessoryLeft: searchIcon,
                    placeholder: "BUSCA POR NÚMERO DA APF",
                    textStyle: { textAlign: 'center' },
                    autoCorrect: false,
                    defautValue: filtro,
                    autoCapitalize: "none",
                    autoCorrect: false,
                    returnKeyType: 'done',
                    autoCapitalize: 'none',
                    onChangeText: changeFiltro,
                    keyboardType: 'number-pad',
                    onSubmitEditing: pesquisar
                }]}
                content={() => (<>
                    <Button
                        size="small" 
                        style={{ 
                            marginBottom: 10, width: 100, alignSelf: 'flex-end',
                        }}
                        onPress={() => { pesquisar(); Keyboard.dismiss(); }}
                    >Pesquisar</Button>
                    {
                        dados !== null ?
                        <>
                            {
                                dados.length ?
                                <>
                                    <ScrollView 
                                        bounces={false} 
                                        showsVerticalScrollIndicator={false} 
                                        showsHorizontalScrollIndicator={false} 
                                        horizontal={true}
                                        vertical={true}
                                        style={{
                                            width: '100%',
                                            marginBottom: 5
                                        }}
                                    >
                                        <Table borderStyle={styles.tableBorderStyle} style={{
                                            width: '100%'
                                        }}>
                                             <Row 
                                                data={[
                                                    'Cód',
                                                    'Abertura.',
                                                    'Encerramento',
                                                    'Bilhetes',
                                                    'Visualizar'
                                                ]} 
                                                textStyle={styles.tableHeaderText}
                                                style={styles.tableHeader}
                                                widthArr={widthArray}
                                            />
                                            {
                                                dados[pagina].map((row, index) => (
                                                    <TouchableOpacity 
                                                        key={index} 
                                                        onPress={() => abrirAPF(row)}
                                                    >
                                                        <Row
                                                            data={[
                                                                row.cod,
                                                                row.abertura?row.abertura:'-',
                                                                row.encerramento?row.encerramento:'-',
                                                                row.bilhetes,
                                                                <FeatherIcon.Search 
                                                                    color="#4E86FB"
                                                                    width={18}
                                                                    height={18}
                                                                    style={{ alignSelf: 'center' }}
                                                                />
                                                            ]}
                                                            style={[styles.row, index%2 && {backgroundColor: '#EEF5FE'}]}
                                                            textStyle={{padding: 10, textAlign: 'center'}}
                                                            widthArr={widthArray}
                                                        />
                                                    </TouchableOpacity>
                                                ))
                                            }
                                        </Table>
                                    </ScrollView>
                                    <View style={styles.pagination}>
                                        {
                                            dados.map((row, index) => (
                                                <TouchableOpacity 
                                                    key={index}
                                                    style={{...styles.paginationDot, backgroundColor: pagina == index ? '#E7EAFF' : 'white'}}
                                                    onPress={() => setPagina(index)}
                                                >
                                                    <Text style={{fontSize: 18}}>{index + 1}</Text>
                                                </TouchableOpacity>
                                            ))
                                        }
                                    </View>
                                </> : <>
                                <View style={{...styles.warningAlert, marginBottom: 30 }}>
                                        <Text style={styles.alertText}>
                                            NENHUM DADO FOI LOCALIZADO
                                        </Text>
                                    </View>
                                </>
                            }
                        </> : <></>
                    }
                </>)}
            />
        </>
    );
};