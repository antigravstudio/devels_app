import React, { useState, useEffect } from 'react';
import { 
    View, SafeAreaView, Platform, ScrollView, Text, TouchableOpacity, Keyboard
} from 'react-native';
import { Input } from '@ui-kitten/components';
import { StatusBar } from 'expo-status-bar';
import styles from '../../utils/CustomStyles';

import { Header } from './Header';
import { HeaderSimple } from './HeaderSimple';

export const Form = ({ 
    navigation, subheader, back, logout, content, title, 
    menu, dark, simple, headerButton, inputs, bigContent,
    cancel
}) => {
    const Content = content;
    const HeaderButton = headerButton;
    const voltarMenu = () => navigation.reset({ index: 0, routes: [{ name: 'Menu' }] });

    const [opened, setOpened] = useState(false);

    useEffect(() => {
        Keyboard.addListener('keyboardDidShow', () => { setOpened(true) });
        Keyboard.addListener('keyboardDidHide', () => { setOpened(false) });
    }, []);

    useEffect(() => () => {}, []);

    return (
        <>
            <StatusBar style="light" backgroundColor="#293756" />
            <SafeAreaView backgroundColor="#293756" />
            <View
                behavior={Platform.OS == 'ios' ? 'padding' : 'height'} 
                style={{ 
                    flex: 1, 
                    height: '100%',
                    flexDirection: 'column',
                    backgroundColor: '#293756',
                }}
            >
                <View style={{ 
                    flex: 1, 
                    height: '100%', 
                    flexDirection: 'column'
                }}>
                    {
                        simple ?
                        <HeaderSimple 
                            navigation={navigation} 
                            subheader={subheader}
                            back={back} 
                            logout={logout}
                            noLogo={bigContent}
                            cancel={cancel}
                        />
                        :
                        <Header 
                            navigation={navigation} 
                            subheader={subheader}
                            back={back} 
                            logout={logout}
                            noLogo={bigContent}
                            cancel={cancel}
                        />
                    }
                    {
                        bigContent ?
                            <Content />
                        :
                            <ScrollView 
                                style={dark ? styles.formDark : styles.formScroll}
                                containerStyle={{...styles.formScrollContainer}}
                                showsVerticalScrollIndicator={false} 
                                showsHorizontalScrollIndicator={false}
                                keyboardShouldPersistTaps="always"
                                bounce={true}
                            >
                                {
                                    title ?
                                    <Text style={styles.viewTitle}>{title}</Text>
                                    : <></>
                                }
                                { headerButton ? <HeaderButton /> : <></>}
                                {inputs? <>
                                    {
                                        inputs.map((_pros, index) => (
                                            <Input 
                                                key={index}
                                                {..._pros}
                                                size="small"
                                            />
                                        ))
                                    }
                                </> :<></>}
                                <Content />
                                {
                                    menu ?
                                    <TouchableOpacity 
                                        onPress={voltarMenu}
                                        style={styles.link}
                                    >
                                        <Text style={styles.linkText}>
                                            Voltar ao Menú
                                        </Text>
                                    </TouchableOpacity>
                                    : <></>
                                }
                                <View 
                                    style={{ 
                                        height: opened ? 300 : 100, 
                                        width: '100%' 
                                    }}
                                ></View>
                            </ScrollView>
                    }
                </View>
            </View>
        </>
    );
};