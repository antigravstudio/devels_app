import React, { useEffect } from 'react';
import { View, Image } from 'react-native';

export const HeaderSimple = () => {
    useEffect(() => () => {}, []);
    return (<>
        <View style={{
            width: '100%', 
            flexDirection: 'column', 
            paddingHorizontal: 10,
            paddingTop: 20,
            alignItems: 'center'
        }}>
            <Image 
                source={require("../../../assets/images/athens_white_simple.png")} 
                resizeMode="contain"
                style={{width: 150, height: 55, marginBottom: 10}}
                showsVerticalScrollIndicator={false} 
                showsHorizontalScrollIndicator={false}
            />
            <Image 
                source={require("../../../assets/images/app_gbu.png")} 
                resizeMode="contain"
                style={{ width: 100, height: 25, marginBottom: 10}}
                showsVerticalScrollIndicator={false} 
                showsHorizontalScrollIndicator={false}
            />
        </View>
    </>);
};