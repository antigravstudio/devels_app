import React from 'react';
import { 
    View, Text, ScrollView, TouchableOpacity, Image
} from 'react-native';

export const Modal = ({ show, content, close, closable}) => {
    const Content = content ? content : () => (<></>);
    const closeCallback = close ? close : () => {};
    closable = closable !== undefined ? closable : true;
    
    return (
        <>
            {
                show ?
                <View 
                    style={{
                        position: 'absolute',
                        zIndex: 1000,
                        top: 0,
                        left: 0,
                        backgroundColor: 'rgba(0, 0, 0, 0.8)',
                        flex: 1,
                        width: '100%',
                        height: '100%',
                        paddingTop: 40,
                        paddingHorizontal: 20,
                        justifyContent: 'center',
                        alignContent: 'center',
                        alignItems: 'center',
                    }}
                >
                    <ScrollView 
                        style={{
                            height: '100%',
                            width: '100%',
                            flex: 1,
                            flexDirection: 'column',
                            borderBottomLeftRadius: 10,
                            borderBottomRightRadius: 10
                        }}
                        contentContainerStyle={{
                            flexGrow: 1,
                            justifyContent: 'center',
                            alignContent: 'center',
                            alignItems: 'center',
                        }}
                        bounces={false}
                    >
                        {
                            closable ?
                            <View style={{
                                alignContent: 'flex-end', 
                                width: '100%', 
                                flexDirection: 'column',
                                marginBottom: 20,
                            }}>
                                <TouchableOpacity style={{
                                    backgroundColor: 'white',
                                    borderRadius: 40,
                                    padding: 10,
                                    alignSelf: 'flex-end'
                                }} onPress={() => closeCallback(false)}>
                                    <Image
                                        source={require("../../../assets/images/close.png")}
                                        resizeMode="contain"
                                        style={{ width: 20, height: 20 }}
                                    />
                                </TouchableOpacity>
                            </View> : <></>
                        }
                        <Content />
                        <View style={{width: '100%', height: 100}}></View>
                    </ScrollView>
                </View> : <></>
            }
        </>
    )
};