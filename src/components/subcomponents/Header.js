import React, { useState, useEffect } from 'react';
import { View, Image, TouchableOpacity, SafeAreaView } from 'react-native';
import { Text } from '@ui-kitten/components';
import * as FeatherIcon from "react-native-feather";
import Store from '../../utils/Store';
import Alert from '../../utils/Alert';

export const Header = ({ navigation, subheader, back, logout, noLogo, cancel }) => {
    const showSubheader = subheader == undefined || subheader == true ? true : false;
    const showLogout = logout == undefined ? false : logout;
    const canGoBack = back == undefined ? navigation.canGoBack() : back;

    const [tipoPerfil, setTipoPerfil] = useState('');

    const sair = () => {
        Alert.confirm({
            title: 'Você tem certeza?', 
            subtitle: 'Seu login será removido deste dispositivo',
            accept: async () => {
                await Store.remove('perfil');
                await Store.remove('idDispositivo');
                navigation.navigate('Splash');
            }
        });
    };

    const cancelar = () => {
        Alert.confirm({
            title: 'Você tem certeza?', 
            subtitle: 'Não será possível reverter',
            accept: async () => {
                await cancel();
                navigation.reset({ index: 0, routes: [{ name: 'Menu' }]});
            }
        });
    };

    useEffect(() => {
        async function loadTipoPerfil(){
            const tipo = await Store.get('perfil');
            if(perfil.includes('Custódia')
                || perfil.includes('CUSTODIA_CANCELAMENTO')
                || perfil.includes('CUSTODIA_SUPORTE')
                || perfil.includes('CUSTODIA_ABASTECIMENTO')
            ){
                setTipoPerfil('Custódia'); 
            }
            if(perfil.includes('Encarregado') 
                || perfil.includes('ENCARREGADO')
                || perfil.includes('ZELADORIA')
                || perfil.includes('AGENTE')
            ){
                setTipoPerfil('Encarregado'); 
            }
            if(perfil.includes('Transportadora') ){
                setTipoPerfil('Transportadora'); 
            }
            if(perfil.includes('Administrador') 
                || perfil.includes('DIRETORIA')
                || perfil.includes('ADM_CCO')
                || perfil.includes('LYLI')
            ){
                setTipoPerfil('Administração'); 
            }            
            if(perfil.includes('SPTrans') 
                || perfil.includes('SPTRANS')
            ){
                setTipoPerfil('SP Trans'); 
            }                          
            if(perfil.includes('ZELADORIA') ){
                setTipoPerfil('Zeladoria'); 
            }
        }
        loadTipoPerfil();
    }, []);

    return (<>
        {
            !noLogo ?
            <View style={{
                width: '100%', 
                flexDirection: 'row', 
                paddingHorizontal: 10,
                paddingTop: 30
            }}>
                <View style={{ width: '50%', alignItems: 'flex-start' }}>
                    <Image 
                        source={require("../../../assets/images/athens_white_simple.png")} 
                        resizeMode="contain"
                        style={{width: 80, height: 50}}
                        showsVerticalScrollIndicator={false} 
                        showsHorizontalScrollIndicator={false}
                    />
                </View>
                <View style={{ width: '50%', alignItems: 'flex-end', paddingTop: 16 }}>
                    <Image 
                        source={require("../../../assets/images/app_gbu.png")} 
                        resizeMode="contain"
                        style={{ width: 140, height: 20}}
                        showsVerticalScrollIndicator={false} 
                        showsHorizontalScrollIndicator={false}
                    />
                </View>
            </View> : <></>
        }
        {
            showSubheader ?
            <View style={{
                width: '100%', 
                flexDirection: 'row', 
                paddingHorizontal: 20
            }}>
                <View style={{ width: '30%', alignItems: 'flex-start', paddingTop: noLogo ? 40 : 20 }}>
                    {
                        showLogout ?
                        <>
                            <TouchableOpacity 
                                onPress={sair}
                            >
                                <Text style={{fontSize: 18, color: '#e74c3c'}}>
                                    <FeatherIcon.LogOut 
                                        color="#e74c3c"
                                        width={18}
                                        height={18}
                                    />
                                    {' '}
                                    Sair
                                </Text>
                            </TouchableOpacity>
                        </>
                        :
                        <>
                            {
                                canGoBack ?
                                <TouchableOpacity 
                                    onPress={() => navigation.goBack()}
                                >
                                    <Text style={{fontSize: 18, color: 'white', marginLeft: -5}}>
                                        <FeatherIcon.ChevronLeft 
                                            color="white"
                                            width={18}
                                            height={18}
                                        />
                                        {' '}
                                        Voltar
                                    </Text>
                                </TouchableOpacity>
                                : 
                                <>
                                    {
                                        cancel ?
                                        <TouchableOpacity 
                                            onPress={cancelar}
                                        >
                                            <Text style={{fontSize: 18, color: '#e74c3c', marginTop: 2}}>
                                                Cancelar
                                            </Text>
                                        </TouchableOpacity>
                                        : 
                                        <></>
                                    }
                                
                                </>
                            }
                        </>
                }
                </View>
                <View style={{ width: '70%', alignItems: 'flex-end', paddingTop: noLogo ? 40 : 20}}>
                    <Text style={{
                        fontSize: 18,
                        fontWeight: '600',
                        color: 'white',
                        textAlign: 'right'
                    }}>
                        {tipoPerfil}
                        {' '}
                        <FeatherIcon.User 
                            color="white"
                            width={18}
                            height={18}
                        />
                    </Text>
                </View>
            </View>
            :
            <></>
        }
    </>);
};