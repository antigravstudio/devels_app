import React, { useEffect, useState } from 'react';
import { 
    TouchableOpacity, Text, View, Image
} from 'react-native';
import Spinner from 'react-native-loading-spinner-overlay';
import * as FeatherIcon from "react-native-feather";
import * as FileSystem from 'expo-file-system';
import * as Sharing from 'expo-sharing';
import * as Linking from 'expo-linking';
import base64 from 'react-native-base64';

import styles from '../utils/CustomStyles';
import Alert from '../utils/Alert';
import Store from '../utils/Store';

import api from '../service/ApiHandler';

import { Form } from './subcomponents/Form';

export const DocumentosScreen = (props) => {    
    const [spinner, setSpinner] = useState(false);
    const [spinnerText, setSpinnerText] = useState('Aguarde...');
    const [arquivos, setArquivos] = useState([]);
    const [pasta, setPasta] = useState([]);

    const listarArquivos = async (pasta) => {
        setSpinnerText('Obtendo arquivos...');
        setSpinner(true);
        const idDispositivo = await Store.get('idDispositivo');
        const tipo = await Store.get('perfil');
        if(pasta.slice(pasta.length -1) == '/'){
            pasta = pasta.slice(0, -1); 
        }
        await api.listarDocumentos(`idDispositivo=${idDispositivo}&pasta=${pasta}`, async (response) => {
            if(response){
                if(response.length){
                    setArquivos(response);
                } else {
                    setArquivos([]);
                }
            } else {
                setArquivos([]);
            }
            setSpinner(false);
        });
    };

    const escolher = async(row) => {
        if(row.Nome.slice(row.Nome.length -1) == '/'){
            var _pasta = row.Nome.split("/").filter(n => n);
            setPasta(_pasta);
            await listarArquivos(row.Nome);
        } else {
            var file_name = row.Nome.split("/");
            file_name = file_name[file_name.length - 1];
            Alert.confirm({
                title: 'Atenção', 
                subtitle: `Deseja baixar o aquivo ${file_name}?`,
                accept: async () => {
                    setSpinnerText('Baixando arquivo...');
                    setSpinner(true);
                    const idDispositivo = await Store.get('idDispositivo');
                    await api.downloadDocumento(`idDispositivo=${idDispositivo}&arquivo=${row.Nome}`, async (config, ) => {
                        if(config){
                            var dados = base64.encode(JSON.stringify(config));
                            var url = "http://projects.samuelcarrara.com/devels/download.php?hash=" + dados;
                            await Linking.openURL(url);
                            //console.log(dados);
                            setSpinner(false);
                            return false;
                            await Sharing.shareAsync(uri);
                            setSpinner(false);
                            try {
                                FileSystem.deleteAsync(uri)
                                    .then((deleted) => {
                                        console.log(deleted);
                                    })
                                    .catch((error) => {
                                        console.log(error)
                                    });
                            } catch(e){
                                console.log(e);
                            }
                        } else {
                            Alert.simple({ 
                                subtitle: 'Não foi possível baixar o arquivo',
                                accept: () => {
                                    setSpinner(false);
                                }
                            });
                        }
                    });
                }
            });
        }
    };

    const voltar = async (index) => {
        if(index == -1){
            setPasta([]);
            await listarArquivos('');
        }
        if((index + 1) == pasta.length){
            return false;
        }
        var rota = [];
        pasta.forEach((element, i) => {
            if(i > index) return;
            rota.push(element);
        });
        rota = rota.join("/");
        var _pasta = rota.split("/").filter(n => n);
        setPasta(_pasta);
        await listarArquivos(rota);
        
    };

    const normalizarPasta = (text) => {
        text = text.split("/").filter(n => n);
        text = text[text.length - 1]
        return text;
    };

    useEffect(() => {
        (async () => {
            await listarArquivos('');
            setPasta([]);
        })();
    }, []);

    useEffect(() => () => {}, []);

    return (
        <>
            <Spinner
                visible={spinner}
                textContent={spinnerText}
                textStyle={{ color: '#fff'}}
                color="#fff"
                overlayColor="rgba(0, 0, 0, 0.85)"
                hidden={false}
                animation="fade"
            />
            <Form 
                {...props}
                title="Documentos"
                menu={true}
                content={() => (<>
                    {
                        pasta.length ?
                        <>
                            <View style={{
                                flexDirection: 'column',
                            }}>
                                <TouchableOpacity style={{
                                    flexDirection: 'row',
                                    paddingTop: 10,
                                    paddingBottom: 10,
                                    alignContent: 'center',
                                    alignItems: 'center',
                                    marginTop: 0
                                }} onPress={() => voltar(-1)}>
                                    <Image 
                                        source={require('../../assets/icons/empty-folder.png')} 
                                        style={{ 
                                            width: 15, 
                                            height: 15
                                        }}
                                    />
                                    <Text style={{
                                        fontSize: 14,
                                        fontWeight: '800',
                                        marginLeft: 10
                                    }}>Pasta Raiz</Text>
                                </TouchableOpacity>
                                {
                                    pasta.map((row, index) => (
                                        <TouchableOpacity style={{
                                            flexDirection: 'row',
                                            paddingTop: 10,
                                            paddingBottom: 10,
                                            alignContent: 'center',
                                            alignItems: 'center',
                                            marginTop: 0,
                                            marginBottom: 5
                                        }} key={index} onPress={() => voltar(index)}>
                                            <FeatherIcon.ChevronsRight 
                                                color="#c5c5c5"
                                                width={18}
                                                height={18}
                                                style={{
                                                    marginLeft: (index * 15) + 15
                                                }}
                                            />
                                            <Image 
                                                source={require('../../assets/icons/empty-folder.png')} 
                                                style={{ 
                                                    width: 15, 
                                                    height: 15, 
                                                    marginLeft: 10
                                                }}
                                            />
                                            <Text style={{
                                                fontSize: 14,
                                                fontWeight: '800',
                                                marginLeft: 10
                                            }}>{row}</Text>
                                        </TouchableOpacity>
                                    ))
                                }
                            </View>
                        </> : <></>
                    }
                    {
                        arquivos.length && !spinner ? 
                        <>
                            {
                                arquivos.map((row, index) => (
                                    <TouchableOpacity key={index} style={{
                                        width: '100%',
                                        height: 60,
                                        backgroundColor: '#f5f5f5',
                                        flex: 1,
                                        flexDirection: 'row',
                                        alignContent: 'center',
                                        alignItems: 'center',
                                        marginBottom: 10,
                                        borderRadius: 4
                                    }} onPress={() => escolher(row)}>

                                        <View style={{
                                            height: '100%',
                                            width: '20%',
                                            alignItems: 'center',
                                            justifyContent: 'center',
                                            alignContent: 'center'
                                        }}>
                                            {
                                                row.Nome.slice(row.Nome.length -1) == '/' ?
                                                <>
                                                    {
                                                        row.Tamanho == '0 Kb' ?
                                                        <Image 
                                                            source={require('../../assets/icons/empty-folder.png')} 
                                                            style={{ width: 30, height: 30 }}
                                                        />
                                                        :
                                                        <Image 
                                                            source={require('../../assets/icons/folder.png')} 
                                                            style={{ width: 30, height: 30 }}
                                                        />
                                                    }
                                                </>
                                                :
                                                <Image 
                                                    source={require('../../assets/icons/pdf.png')} 
                                                    style={{ width: 30, height: 30 }}
                                                />
                                            }
                                        </View>
                                        <View style={{
                                            height: '100%',
                                            width: '80%',
                                            alignItems: 'flex-end',
                                            justifyContent: 'center',
                                            alignContent: 'center',
                                            paddingRight: 10
                                        }}>
                                            <Text>{normalizarPasta(row.Nome)}</Text>
                                        </View>
                                    </TouchableOpacity>
                                ))
                            }
                        </>
                        :
                        <>
                            {
                                !spinner ?
                                <View style={{ paddingTop: 15 }}>
                                    <View style={{...styles.warningAlert }}>
                                        <Text style={styles.alertText}>
                                            PASTA VAZIA
                                        </Text>
                                    </View>
                                </View>
                                :
                                <View style={{
                                    padding: 20,
                                    alignItems: 'center',
                                    justifyContent: 'center',
                                    flex: 1,
                                    backgroundColor: '#f5f5f5',
                                    marginBottom: 15,
                                    borderRadius: 8
                                }}>
                                    <Text style={{
                                        fontSize: 16,
                                        fontWeight: 'bold'
                                    }}>
                                        Carregando...
                                    </Text>
                                </View>
                            }
                        </>
                    }
                </>)}
            />
        </>
    );
};