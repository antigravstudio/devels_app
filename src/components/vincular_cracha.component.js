import React, { useEffect, useState } from 'react';
import { 
    View, Image, TouchableOpacity, TouchableWithoutFeedback, Platform, ScrollView
} from 'react-native';
import Spinner from 'react-native-loading-spinner-overlay';
import { Text } from '@ui-kitten/components';
import * as Linking from 'expo-linking';

import * as FeatherIcon from "react-native-feather";

import { Form } from './subcomponents/Form';
import api from '../service/ApiHandler';
import Store from '../utils/Store';
import Alert from '../utils/Alert';
import styles from '../utils/CustomStyles';

export const VincularCrachaScreen = (props) => {  
    const [spinner, setSpinner] = useState(true);
    const [expandOperador, setExpandOperador] = useState(false);
    const [spinnerText, setSpinnerText] = useState('Aguarde...');  
    const [cartao, setCartao] = useState('');
    const [operadores, setOperadores] = useState([]);
    const [operador, setOperador] = useState(null);
    const [operadorLabel, setOperadorLabel] = useState('...');

    const lerBilhete = async () => {
        await Linking.openURL(`devels://nfc/leitura_operador/?${operador}`);
    };

    const changeOperador = (row) => {
        console.log(row.operadorId);
        setOperador(row.operadorId);
        setOperadorLabel(row.nome.toUpperCase());
        setExpandOperador(false);
    };

    const vincular = async () => {
        setSpinner(true);
        setSpinnerText('Vinculando...');
        const idDispositivo = await Store.get('idDispositivo');
        const response = await api.vincularCracha({
            idDispositivo, 
            idOperador: operador, 
            //idOperador: 186, 
            numeroCracha: String(cartao),
            //numeroCracha: String(7920903)
        });
        if(response){
            Alert.simple({ 
                title: 'Crácha vinculado com sucesso',
                subtitle: `Vinculado para ${operadorLabel}`,
                accept: () => {
                    setSpinner(false);
                    props.navigation.reset({ index: 0, routes: [{ name: 'Menu' }]});
                }
            });
        } else {
            Alert.simple({ 
                subtitle: 'Ocorreu um erro',
                accept: () => {
                    setSpinner(false);
                    props.navigation.reset({ index: 0, routes: [{ name: 'Menu' }]});
                }
            });
        }
    };

    useEffect(() => {
        (async () => {
            setSpinner(true);
            var _operadores = [];
            setOperador('');
            setOperadorLabel('...');
            setExpandOperador(false);
            try {
                const idDispositivo = await Store.get('idDispositivo');
                const response = await api.obtemOperadores({idDispositivo});
                if(response){
                    if(response.length){
                        setOperadores(response);
                        _operadores = response;
                    } else {
                        Alert.simple({ 
                            subtitle: 'Não foi possível obter os operadores',
                            accept: () => {
                                setSpinner(false);
                                props.navigation.reset({ index: 0, routes: [{ name: 'Menu' }]});
                            }
                        });
                    }
                } else {
                    Alert.simple({ 
                        subtitle: 'Não foi possível obter os operadores',
                        accept: () => {
                            setSpinner(false);
                            props.navigation.reset({ index: 0, routes: [{ name: 'Menu' }]});
                        }
                    });
                }
            } catch(e){
                console.log(e);
            }
            var validar_nfc = true;
            try {
                if(props.route.params.cartao && props.route.params.operador) validar_nfc = false;
                else validar_nfc = true;
            } catch (e){ validar_nfc = true; }
            if(Platform.OS !== 'ios' && validar_nfc){
                try {
                    await Linking.openURL(`devels://nfc/verificar_versao/1.0.1?http://projects.samuelcarrara.com/devels/devels-nfc-1.0.1.apk`);
                } catch(e){
                    Alert.confirm({
                        title: 'A utilidade de leitura de cartões ainda não foi instalada', 
                        subtitle: 'Deseja instalar o complemento agora?',
                        accept: async () => {
                            await Linking.openURL(`http://projects.samuelcarrara.com/devels/devels-nfc-1.0.1.apk`);
                            setSpinner(false);
                            props.navigation.reset({ index: 0, routes: [{ name: 'Menu' }]});
                        }, 
                        cancel: async () => {
                            setSpinner(false);
                            props.navigation.reset({ index: 0, routes: [{ name: 'Menu' }]});
                        }
                    });
                    return false;
                }
            }
            setSpinner(true);
            setSpinnerText('Aguarde...');
            setSpinner(false);
            if(!validar_nfc){
                try {
                    var c = props.route.params.cartao;
                    var o = props.route.params.operador; 
                    setCartao(c);
                    var _o = false;
                    for (const row of _operadores){
                        if(String(row.operadorId) == String(o)){
                            changeOperador(row);
                            _o = row;
                            break;
                        }
                    }
                    if(_o){
                        Alert.confirm({
                            title: 'Confirma a vinculação do crachá?', 
                            subtitle: `Vinculando para ${_o.nome.toUpperCase()}`,
                            accept: async () => {
                                await vincular();
                            }, 
                            cancel: async () => {
                                setSpinner(false);
                                props.navigation.reset({ index: 0, routes: [{ name: 'Menu' }]});
                            }
                        });
                    } else {
                        Alert.simple({ 
                            subtitle: 'Não foi validar a leitura',
                            accept: () => {
                                setSpinner(false);
                                props.navigation.reset({ index: 0, routes: [{ name: 'Menu' }]});
                            }
                        });
                    }
                } catch(e){
                    console.log(e);
                    Alert.simple({ 
                        subtitle: 'Não foi validar a leitura',
                        accept: () => {
                            setSpinner(false);
                            props.navigation.reset({ index: 0, routes: [{ name: 'Menu' }]});
                        }
                    });
                }
            }
        })();
    }, []);

    useEffect(() => () => {}, []);

    return (
        <>
            <Spinner
                visible={spinner}
                textContent={spinnerText}
                textStyle={{ color: '#fff'}}
                color="#fff"
                overlayColor="rgba(0, 0, 0, 0.85)"
                hidden={false}
                animation="fade"
            />
            <Form 
                {...props}
                title="Vincular Crachá do Operador"
                menu={false}
                content={() => (<>
                    <Text style={{
                        textTransform: 'uppercase',
                        marginBottom: 10,
                        color: 'darkgray'
                    }}>
                        Operador
                    </Text>
                    <TouchableOpacity 
                        style={{
                            ...styles.customCombo,
                            borderBottomLeftRadius: expandOperador ? 0 : 6,
                            borderBottomRightRadius: expandOperador ? 0 : 6,
                            marginBottom: expandOperador ? 0 : 10,
                        }}
                        onPress={() => setExpandOperador(!expandOperador)}
                    >
                        <FeatherIcon.User 
                            color="#4494F2"
                            width={16} 
                            height={16}
                            style={{ marginRight: 5 }}
                        />
                        <Text
                            style={{
                                position: 'absolute',
                                left: 40,
                                top: 10
                            }}
                        >{operadorLabel}</Text>
                        {
                            expandOperador ?
                            <FeatherIcon.ChevronUp 
                                width={20} 
                                height={20} 
                                color={'#000'} 
                                style={{ position: 'absolute', right: 5, top: 8 }}
                            />
                            :
                            <FeatherIcon.ChevronDown 
                                width={20} 
                                height={20} 
                                color={'#000'} 
                                style={{ position: 'absolute', right: 5, top: 8 }}
                            />
                        }
                    </TouchableOpacity> 
                    {
                        expandOperador ?
                        <ScrollView 
                            bounces={false}
                            style={{ 
                                marginBottom: 15
                            }}
                        >
                            {
                                operadores.map((row, index) => (
                                    <TouchableOpacity 
                                        key={index} 
                                        style={styles.customComboOption}
                                        onPress={() => changeOperador(row)}
                                    >
                                        <Text>{row.nome.toUpperCase()}</Text>
                                    </TouchableOpacity>
                                ))
                            }
                        </ScrollView> : <></>
                    }
                    {
                        operador ?
                        <TouchableOpacity 
                            onPress={lerBilhete}
                            style={{
                                marginTop: 10,
                                paddingVertical: 25,
                                paddingHorizontal: 20,
                                borderColor: '#EFF1FD',
                                backgroundColor: '#EFF1FD',
                                borderWidth: 1.5,
                                borderRadius: 6,
                                alignItems: 'flex-start',
                                flexDirection: 'row',
                                marginBottom: 15
                            }}
                        >
                            <Image
                                source={require("../../assets/images/nfc.png")}
                                resizeMode="contain"
                                style={{ 
                                    width: 30, 
                                    height: 30,
                                    position: 'absolute',
                                    right: 15,
                                    top: 10
                                }}
                            />
                            <Text style={{ 
                                color: '#000', 
                                fontSize: 16,
                                position: 'absolute',
                                top: 13,
                                left: 15,
                            }}>
                                Ler Cartão
                            </Text>
                        </TouchableOpacity> 
                        :
                        <></>
                    }
                </>)}
            />
        </>
    );
};