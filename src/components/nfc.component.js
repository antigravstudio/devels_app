import React, { useEffect } from 'react';
import { Image, View } from  'react-native';
import { StatusBar } from 'expo-status-bar';

export const NFCScreen = (props) => {
    useEffect(() => {
        props.navigation.replace('FracionarLote', props.route.params);
    }, []);
    useEffect(() => () => {}, []);

    return (
        <>
            <StatusBar style="light" backgroundColor="#293756" />
            <View style={{
                backgroundColor: '#293756',
                flex: 1,
                alignContent: 'center',
                alignItems: 'center',
                height: '100%'
            }}>
                <Image 
                    source={require("../../assets/images/athens_white_simple.png")} 
                    resizeMode="contain"
                    style={{
                        width: 200,
                        height: '100%',
                        alignSelf: 'center',
                        resizeMode: 'contain'
                    }}
                />
            </View>
        </>
    );
};