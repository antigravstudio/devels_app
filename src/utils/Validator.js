const validateEmail = (email) => {
    // eslint-disable-next-line
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
};

const validateTelefone = (t) => {
    t = t.replace(/[^\d]+/g,'');
    return t.length === 10;
};

const validateCelular = (t) => {
    t = t.replace(/[^\d]+/g,'');
    return t.length === 11;
};

export default {
    validateTelefone,
    validateCelular,
    validateEmail
}