import { StyleSheet } from 'react-native';

export default StyleSheet.create({
    image: {
        width: 130,
        height: 100,
        resizeMode: 'contain',
        flexDirection: 'column',
        alignItems: 'center'
    },
    form: {
        flex: 1,
        justifyContent: 'flex-start',
        alignContent: 'flex-start',
        alignItems: 'flex-start',
        justifyContent: 'flex-start',
        backgroundColor: 'white',
        paddingHorizontal: 30,
        marginTop: 20,
        borderTopLeftRadius: 30,
        borderTopRightRadius: 30,
        paddingTop: 50,
        alignSelf: 'stretch'
    },
    formScroll: {
        backgroundColor: 'white',
        paddingHorizontal: 30,
        marginTop: 20,
        borderTopLeftRadius: 30,
        borderTopRightRadius: 30,
        paddingTop: 40,
        alignSelf: 'stretch',
        flexDirection: 'column'
    },
    formScrollContainer: {
        justifyContent: 'flex-start',
        alignContent: 'flex-start',
        alignItems: 'flex-start',
        justifyContent: 'flex-start',
    },
    input: {
        marginBottom: 15,
        borderWidth: 0,
        width: '100%',
        marginTop: 10,
        borderRadius: 8
    },
    link: {
        width: '100%',
        backgroundColor: '#C5DEFB',
        borderWidth: 0,
        borderRadius: 8,
        paddingVertical: 15,
        flexDirection: 'row',
        alignItems: 'center',
        alignContent: 'center',
        justifyContent: 'center',
        marginBottom: 15
    },
    linkIcon: {
        position: 'absolute',
        left: 10
    },
    linkSubtitle: {
        position: 'absolute',
        left: 10
    },
    linkText: {
        color: '#283757', 
        fontSize: 14, 
        textTransform: 'uppercase'
    },
    imageLogin: {
        width: 130,
        height: 100,
        resizeMode: 'contain',
        flexDirection: 'column',
        alignItems: 'center'
    },
    formDark: {
        flex: 1,
        backgroundColor: '#171717',
        paddingHorizontal: 30,
        marginTop: 20,
        borderTopLeftRadius: 30,
        borderTopRightRadius: 30,
        paddingTop: 20,
    },
    formLogin: {
        flex: 1,
        justifyContent: 'flex-start',
        alignContent: 'flex-start',
        alignItems: 'flex-start',
        justifyContent: 'flex-start',
        backgroundColor: 'white',
        paddingHorizontal: 30,
        marginTop: 20,
        borderTopLeftRadius: 30,
        borderTopRightRadius: 30,
        paddingTop: 20,
    },
    formSMS: {
        flex: 1,
        justifyContent: 'flex-start',
        alignContent: 'flex-start',
        alignItems: 'flex-start',
        justifyContent: 'flex-start',
        backgroundColor: '#171717',
        paddingHorizontal: 30,
        marginTop: 20,
        borderTopLeftRadius: 30,
        borderTopRightRadius: 30,
        paddingTop: 20,
    },
    inputLogin: {
        marginBottom: 15,
        borderWidth: 0,
        width: '100%',
        marginTop: 10,
        borderRadius: 8
    },
    viewTitle: {
        textAlign: 'center',
        width: '100%',
        fontSize: 17,
        marginBottom: 20
    },
    table: {
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 1,
        },
        shadowOpacity: 0.20,
        shadowRadius: 1.41,
        elevation: 2,
        paddingHorizontal: 10,
        paddingVertical: 15,
        backgroundColor: 'white',
        borderRadius: 6,
        marginBottom: 30,
        width: '100%',
        flexDirection: 'row',
        borderWidth: 1,
        borderColor: '#E9ECFF'
    },
    tableLabel: {
        fontSize: 14,
        color: '#283757',
        fontWeight: '600',
        paddingTop: 2,
        paddingRight: '5%',
    },
    tableIcon: {
        backgroundColor: '#C4DDFA',
        width: 34,
        height: 34,
        alignItems: 'center',
        paddingVertical: 5,
        marginLeft: '5%',
        marginRight: '5%',
        borderRadius: 8
    },
    tableSublabel: {
        fontSize: 12,
        color: '#757575'
    },
    pagination: {
        width: '100%',
        paddingBottom: 10,
        flexDirection: 'row',
        justifyContent: 'center',
    },
    paginationDot: {
        paddingVertical: 5,
        paddingHorizontal: 10,
        borderRadius: 8,
        marginLeft: 5,
        marginRight: 5,
        borderColor: '#E7EAFF',
        borderWidth: 1
    },
    successAlert: {
        backgroundColor: '#A5CC00',
        padding: 20,
        borderRadius: 10,
        width: '100%',
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.23,
        shadowRadius: 2.62,
        elevation: 4,
        marginBottom: 30
    },
    warningAlert: {
        backgroundColor: '#FFC002',
        padding: 20,
        borderRadius: 10,
        width: '100%',
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.23,
        shadowRadius: 2.62,
        elevation: 4,
        marginBottom: 30
    },
    alertText: {
        textAlign: 'center',
        color: 'white',
        fontSize: 14,
        fontWeight: 'bold'
    },
    viewSubtitle: {
        fontSize: 16,
        textTransform: 'uppercase',
        fontWeight: '600',
        marginBottom: 20
    },
    tableBorderStyle: {
        borderWidth: 0, 
        borderColor: '#C1C0B9', 
        backgroundColor: '#fff'
    },
    tableHeader: {
        backgroundColor: '#C4DDFA'
    },
    tableHeaderText: {
        padding: 10, 
        textAlign: 'center', 
        color: '#283757',
        textTransform: 'uppercase'
    },
    dashCard: {
        padding: 20,
        borderRadius: 8,
        width: '46%',
    },
    dashIcon: {
        alignSelf: 'flex-end',
        color: 'white',
        marginBottom: 5
    },
    dashTitle: {
        color: 'white',
        textTransform: 'uppercase',
        fontSize: 14,
        fontWeight: '500',
        opacity: .8,
        marginBottom: 5,
        minHeight: 30,
        alignItems: 'center',
        alignContent: 'center',
    },
    dashSubtitle: { 
        fontSize: 26, 
        color: 'white', 
        fontWeight: '600'
    },
    simpleCard: {
        padding: 10,
        borderWidth: 1,
        borderRadius: 8,
        borderColor: '#C4DDFA'
    },
    simpleCardTitle: {
        fontWeight: '800',
        textTransform: 'uppercase',
        textAlign: 'center',
        marginBottom: 10
    },
    simpleCardSubtitle: {
        textAlign: 'center',
        fontSize: 16,
        fontWeight: '300'
    },
    customCombo: {
        width: '100%',
        paddingVertical: 10,
        paddingHorizontal: 10,
        flexDirection: 'row',
        backgroundColor: '#fff',
        borderWidth: 1.5,
        borderRadius: 6,
        borderColor: '#ECEDFB'
    },
    customComboOption: {
        width: '100%',
        paddingVertical: 10,
        paddingHorizontal: 10,
        backgroundColor: '#fff',
        borderBottomWidth: 1.5,
        borderLeftWidth: 1.5,
        borderRightWidth: 1.5,
        borderColor: '#ECEDFB'
    }
}); 