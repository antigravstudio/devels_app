import * as SecureStore from 'expo-secure-store';
import AsyncStorage from '@react-native-async-storage/async-storage';

const set = async (key, value) => {
    let store = false;
    try {
        store = await SecureStore.setItemAsync(key, value, {
            keychainAccessible: SecureStore.ALWAYS_THIS_DEVICE_ONLY
        });
    } catch(e){
        try {
            await AsyncStorage.setItem(key, value);
            store = true;
        } catch(e){
            store = false;
        }
    }
    return store;
};

const get = async (key) => {
    let value = false;
    try {
        value = await SecureStore.getItemAsync(key);
    } catch(e){
        try {
            value = await AsyncStorage.getItem(key);
        } catch(e){
            value = false;
        }
    }
    return value;
};

const remove = async (key) => {
    let d = false;
    try {
        d = await SecureStore.deleteItemAsync(key);
    } catch(e){
        try {
            await AsyncStorage.deleteItem(key);
            d = true;
        } catch(e){
            try {
                await AsyncStorage.set(setItem, '');
                d = true;
            } catch(e){
                d = false;
            }
        }
    }
    return d;
};

export default {
    set, get, remove
}