import { Alert } from 'react-native';

const confirm = (config) => {
    Alert.alert(
        config.title ? config.title : 'Atenção',
        config.subtitle,
        [
            {
                text: "Não",
                onPress: config.cancel ? config.cancel : () => {},
                style: "cancel"
            },
            {
                text: "Sim",
                onPress: config.accept ? config.accept : () => {}
            }
        ]
    )
};

const simple = (config) => {
    Alert.alert(
        config.title ? config.title : 'Atenção',
        config.subtitle,
        [
            {
                text: config.text ? config.text : "OK",
                onPress: config.accept ? config.accept : () => {}
            }
        ]
    )
};

const custom = (config) => {
    Alert.alert(
        config.title ? config.title : 'Atenção',
        config.subtitle,
        [
            {
                text: config.text1,
                onPress: config.callback1 ? config.callback1 : () => {}
            },
            {
                text: config.text2,
                onPress: config.callback2 ? config.callback2 : () => {}
            }
        ]
    )
};

export default {
    confirm,
    simple,
    custom
};