import React from 'react';
import { Image } from 'react-native';


export default (props) => {
    var name = props.name ? props.name : 'missing';
    var size = props.size ? props.size : 20;
    var source = require('../../assets/icons/missing.png');
    switch(name){
        case 'car-active': source = require('../../assets/icons/car-active.png'); break;
        case 'car-inactive': source = require('../../assets/icons/car-inactive.png'); break;
        case 'radio-btn-active': source = require('../../assets/icons/radio-btn-active.png'); break;
        case 'radio-btn-passive': source = require('../../assets/icons/radio-btn-passive.png'); break;
    }
    return (
        <Image 
            source={source}
            resizeMode="contain"
            style={{ width: size, height: size }}
        />
    );
}