import moment from 'moment';
import { cpf, cnpj } from 'cpf-cnpj-validator'; 

const lowerize = (t) => {
    return t.toLowerCase();
};

const formatEmail = (e) => {
    e = lowerize(e);
    // eslint-disable-next-line
    e = e.replace(/[^@a-zA-Z0-9_\-\.]/g, '');
    return e;
};

const formatTelefone = (t) => {
    var r = t.replace(/\D/g, "");
    r = r.replace(/^0/, "");

    if (r.length > 10) r = r.replace(/^(\d\d)(\d{5})(\d{4}).*/, "($1) $2-$3");
    else if (r.length > 5) r = r.replace(/^(\d\d)(\d{4})(\d{0,4}).*/, "($1) $2-$3");
    else if (r.length > 2) r = r.replace(/^(\d\d)(\d{0,5})/, "($1) $2");
    else r = r.replace(/^(\d*)/, "($1");

    return r;
}

const formatCelular = (t) => {
    var r = t.replace(/\D/g, "");
    r = r.replace(/^0/, "");

    if (r.length > 10) r = r.replace(/^(\d\d)(\d{5})(\d{4}).*/, "($1) $2-$3");
    else if (r.length > 5) r = r.replace(/^(\d\d)(\d{4})(\d{0,4}).*/, "($1) $2-$3");
    else if (r.length > 2) r = r.replace(/^(\d\d)(\d{0,5})/, "($1) $2");
    else r = r.replace(/^(\d*)/, "($1");

    return r;
};

const capitalizer = (text) => {
    if (!text) return '';
    text = text.toLowerCase();
    text = text.toLowerCase().replace(/\b./g, function (a) { return a.toUpperCase(); });
    text = text.replace(/ De /g, ' de ');
    text = text.replace(/ Do /g, ' do ');
    text = text.replace(/ Di /g, ' di ');
    text = text.replace(/ Da /g, ' da ');
    text = text.replace(/ E /g, ' e ');
    text = text.replace(/ Dos /g, ' dos ');
    text = text.replace(/ Das /g, ' das ');
    return text;
};

const formatarData = (d) => {
    return moment(d).format('DD/MM/YYYY');
};

const formatarDataHora = (d) => {
    return moment(d).format('DD/MM/YYYY à\\s HH:mm:ss');
};

const masker = (val, mask) => {
    val = val.split('');
    var maskared = '', k = 0;
    for (var i = 0; i <= mask.length - 1; i++) {
        if (mask[i] == '#') {
            if (typeof val[k] !== undefined)
                maskared += val[k++];
        } else {
            if (typeof mask[i] !== undefined)
                maskared += mask[i];
        }
    }
    return maskared;
}

const formatarPlaca = (p) => {
    p = p.toUpperCase();
    return masker(p, '###-####');
};

const serialize = function (obj) {
    var str = [];
    for (var p in obj)
        if (obj.hasOwnProperty(p)) {
            str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
        }
    return str.join("&");
}

const printToLetter = (number) => {
    var alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
    var result = ""
    var charIndex = number % alphabet.length
    var quotient = number / alphabet.length
    if (charIndex - 1 == -1) {
        charIndex = alphabet.length
        quotient--;
    }
    result = alphabet.charAt(charIndex - 1) + result;
    if (quotient >= 1) printToLetter(parseInt(quotient));
    else result = "";
};

const formatarDocumento = (doc) => {
    doc = String(doc)
    doc = doc.replace(/\D/g, '');
    if(doc.length > 11) return cnpj.format(doc);
    else return cpf.format(doc);
};

export default {
    lowerize,
    formatEmail,
    formatTelefone,
    formatCelular,
    capitalizer,
    formatarData,
    formatarPlaca,
    serialize,
    formatarDataHora,
    printToLetter,
    formatarDocumento
};