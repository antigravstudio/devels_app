import json, sys, os, time
f = open('app.json','r')
filedata = f.read()
f.close()

ambiente = "prod"
acao = "publicar"
try:
    if sys.argv[1] == 'prod':
        ambiente = 'prod'
    if sys.argv[1] == 'hom':
        ambiente = 'hom'
except:
    pass

try:
    if sys.argv[2] == 'local':
        acao = 'alterar'
except:
    pass

filedata = json.loads(filedata)
if ambiente == 'prod':
    filedata['expo']['name'] = 'Athens'
    filedata['expo']['slug'] = 'devels_app'
    filedata['expo']['ios']['bundleIdentifier'] = 'com.devels.app'
    filedata['expo']['android']['package'] = 'com.devels.app'
if ambiente == 'hom':
    filedata['expo']['name'] = 'Athens Beta'
    filedata['expo']['slug'] = 'devels_app_homologa'
    filedata['expo']['ios']['bundleIdentifier'] = 'com.develshomologa.app'
    filedata['expo']['android']['package'] = 'com.develshomologa.app'

filedata = json.dumps(filedata, indent=4)
f = open('app.json', 'w')
f.write(filedata)
f.close()

if acao == 'publicar':
    if ambiente == 'prod':  
        os.system("rm -rf .expo & rm -rf .expo-shared; npx expo-optimize; git commit -am 'Alterado ambiente para producao' & git push & expo publish")
        time.sleep(1)
        os.system('git push')
    if ambiente == 'hom':  
        os.system("rm -rf .expo & rm -rf .expo-shared; npx expo-optimize; git commit -am 'Alterado ambiente para homologacao' & git push & expo publish")
        time.sleep(1)
        os.system('git push')